import 'package:flutter/material.dart';

const backgroundColor = Color(0xfffffffe);
const kPrimaryColor = Color(0xFF24AF01);
const tHintColor = Color(0xFF828282);
const textColor = Color(0xffF3E9DD);
const hBackgroundColor = Color(0xff254543);
const fontPoppins = 'Poppins';
const textHintColor = Color(0x63FFFFFF);
const klPrimaryColor = Color.fromARGB(255, 251, 184, 107);
const topCardBackgroundColor = Color(0xffE8EDEB);
const bottomCardBackgroundColor = Color(0xffffffff);
const hHintColor = Color(0xFF333333);
const backgroundCartColor = Color(0xff004643);
const clientId =
    'Ae63-9GJKyYZxMzddclk-mKStqW60IHmI0ZdaeeBFFaAeDHEXif6km-0g3YVg942jiHOjo4abOoabld3';
const secretKey =
    'EJfQ07Ne48RFgcIBupAAb9Vje0I-MvUavxeVAySbKBIpONxJCSYWfRpimJ1oc5jnIYhr9KHbYsZwjAcO';
const returnURL = 'https://samplesite.com/return';
const cancelURL = 'https://samplesite.com/cancel';
