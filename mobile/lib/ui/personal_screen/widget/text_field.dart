import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tech_wiz4/constants.dart';

class AccountTextField extends StatelessWidget {
  final controller;
  final String label;
  final bool isObscureText;
  final bool valid;
  final String instructionText;

  const AccountTextField(
      {super.key,
      required this.controller,
      required this.label,
      required this.isObscureText,
      required this.valid,
      required this.instructionText});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(),
      child: SizedBox(
        width: 230.w,
        height: 60.h,
        // height: 150.h,
        child: TextField(
          controller: controller,
          obscureText: isObscureText,
          style: TextStyle(fontSize: 20.sp, color: Colors.black87),
          decoration: InputDecoration(
            hintText: label,
            filled: true,
            errorText: valid ? instructionText : null,
            hintStyle: TextStyle(
                fontSize: 5.sp, color: Colors.black, letterSpacing: 2.sp),
            fillColor: backgroundColor,
            // enabledBorder: OutlineInputBorder(
            //   borderRadius: BorderRadius.circular(2),
            //   borderSide: BorderSide(color: Colors.black87, width: 1.sp),
            // ),
            // focusedBorder: OutlineInputBorder(
            //   borderRadius: BorderRadius.circular(2),
            //   borderSide: BorderSide(color: Colors.black87, width: 1.sp),
            // ),
          ),
        ),
      ),
    );
  }
}
