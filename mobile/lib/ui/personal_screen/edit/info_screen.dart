import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_update.dart';

import '../../../constants.dart';
import '../../../core/global/global_data.dart';
import '../../../core/global/locator.dart';
import '../../../core/global/router.dart';
import '../widget/text_field.dart';

class InfomationScreen extends StatefulWidget {
  const InfomationScreen({super.key});

  @override
  State<InfomationScreen> createState() => _InfomationScreenState();
}

class _InfomationScreenState extends State<InfomationScreen> {
  late TextEditingController usernameController;
  late TextEditingController nameController;
  late TextEditingController addressController;
  late TextEditingController emailController;
  late TextEditingController phoneController;

  bool validUsername = false;
  bool validName = false;
  bool validEmail = false;
  bool validPhone = false;
  bool validAddress = false;

  @override
  void initState() {
    super.initState();
    usernameController = TextEditingController();
    nameController = TextEditingController();
    addressController = TextEditingController();
    emailController = TextEditingController();
    phoneController = TextEditingController();

    usernameController.text = locator<GlobalData>().currentUser!.username;
    nameController.text = locator<GlobalData>().currentUser!.name;
    addressController.text = locator<GlobalData>().currentUser!.address;
    emailController.text = locator<GlobalData>().currentUser!.email;
    phoneController.text = locator<GlobalData>().currentUser!.phone;
  }

  Future<bool> doUpdate() async {
    try {
      await locator<IFirebaseUpdateService>().updateUser(
        usernameController.text,
        nameController.text,
        addressController.text,
        emailController.text,
        phoneController.text,
      );
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text(
          "Update infomation",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            letterSpacing: 1.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 0.h,
                thickness: 6.w,
              ),
            ),
            SizedBox(
              height: 15.h,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.h,
                right: 10.w,
                left: 10.w,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Username",
                    style: TextStyle(
                      fontSize: 17.sp,
                      color: Colors.black54,
                    ),
                  ),
                  AccountTextField(
                      controller: usernameController,
                      label: usernameController.text,
                      valid: validUsername,
                      isObscureText: false,
                      instructionText: 'Invalid Username'),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 20.h,
                thickness: 1.w,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.h,
                right: 10.w,
                left: 10.w,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Name",
                    style: TextStyle(
                      fontSize: 17.sp,
                      color: Colors.black54,
                    ),
                  ),
                  AccountTextField(
                      controller: nameController,
                      label: nameController.text,
                      valid: validName,
                      isObscureText: false,
                      instructionText: 'Invalid Name'),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 20.h,
                thickness: 1.w,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.h,
                right: 10.w,
                left: 10.w,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Email",
                    style: TextStyle(
                      fontSize: 17.sp,
                      color: Colors.black54,
                    ),
                  ),
                  AccountTextField(
                      controller: emailController,
                      label: emailController.text,
                      valid: validEmail,
                      isObscureText: false,
                      instructionText: 'Invalid Email'),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 20.h,
                thickness: 1.w,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.h,
                right: 10.w,
                left: 10.w,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Phone",
                    style: TextStyle(
                      fontSize: 17.sp,
                      color: Colors.black54,
                    ),
                  ),
                  AccountTextField(
                      controller: phoneController,
                      label: phoneController.text,
                      valid: validPhone,
                      isObscureText: false,
                      instructionText: 'Invalid phone number (6-11 digits)'),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 20.h,
                thickness: 1.w,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.h,
                right: 10.w,
                left: 10.w,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Address",
                    style: TextStyle(
                      fontSize: 17.sp,
                      color: Colors.black54,
                    ),
                  ),
                  AccountTextField(
                      controller: addressController,
                      label: addressController.text,
                      valid: validAddress,
                      isObscureText: false,
                      instructionText: 'Invalid Address'),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 30.h, left: 10.w, right: 10.w),
              child: InkWell(
                onTap: () {
                  setState(() {
                    validUsername = !RegExp(r"^[a-zA-Z0-9]+$")
                        .hasMatch(usernameController.text);
                    if (locator<GlobalData>().currentUser!.name == '') {
                      if (nameController.text != '') {
                        validName = !RegExp(r"^[a-zA-Z\s]+$")
                            .hasMatch(nameController.text);
                      }
                    } else {
                      validName = !RegExp(r"^[a-zA-Z\s]+$")
                          .hasMatch(nameController.text);
                    }

                    validEmail = !RegExp(
                            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                        .hasMatch(emailController.text);
                    validPhone = !RegExp(r"^[0-9]{6,11}$")
                        .hasMatch(phoneController.text);

                    if (locator<GlobalData>().currentUser!.address == '') {
                      if (addressController.text != '') {
                        validAddress = !RegExp(r"^[A-Za-z0-9'\.\-\s\,]+$")
                            .hasMatch(addressController.text);
                      }
                    } else {
                      validAddress = !RegExp(r"^[A-Za-z0-9'\.\-\s\,]+$")
                          .hasMatch(addressController.text);
                    }
                  });

                  if (!validUsername &&
                      !validName &&
                      !validAddress &&
                      !validEmail &&
                      !validPhone) {
                    doUpdate().then((value) {
                      if (value) {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return Dialog(
                              elevation: 0,
                              backgroundColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                  side: BorderSide(
                                      color: const Color(0XFF344A43),
                                      width: 5.sp)),
                              child: SizedBox(
                                height: 350.h,
                                width: 400.w,
                                child: Column(children: [
                                  Padding(
                                    padding:
                                        EdgeInsets.only(left: 0.w, top: 20.h),
                                    child: Image.asset(
                                      'assets/images/icon_update.png',
                                      width: 180.w,
                                      height: 180.h,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(left: 0.w, top: 20.h),
                                    child: Text(
                                      "Update Success",
                                      style: TextStyle(
                                        color: const Color(0XFF344A43),
                                        fontSize: 20.sp,
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: 2.sp,
                                        fontFamily: fontPoppins,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 20.h),
                                    child: ElevatedButton(
                                      onPressed: () {
                                        Get.toNamed(MyRouter.account);
                                      },
                                      style: ElevatedButton.styleFrom(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal:
                                                30.0), // Điều chỉnh padding ngang để làm cho button dài hơn
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          side: const BorderSide(
                                              color: Colors.black, width: 2.0),
                                        ),
                                        backgroundColor: Colors.white,
                                      ),
                                      child: Container(
                                        width:
                                            150.0, // Điều chỉnh chiều dài của button
                                        child: Center(
                                          child: Text(
                                            'Close',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: fontPoppins,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ]),
                              ),
                            );
                          },
                        );
                      }
                    });
                  }
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 50.h,
                  width: 270.w,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.black,
                    border: Border.all(
                      color: topCardBackgroundColor,
                    ),
                  ),
                  child: Text(
                    "Update",
                    style: TextStyle(
                      fontSize: 16.sp,
                      color: bottomCardBackgroundColor,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2.sp,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
