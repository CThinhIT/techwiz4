import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../../constants.dart';
import '../../../core/global/global_data.dart';
import '../../../core/global/locator.dart';
import '../../../core/global/router.dart';
import '../../../firebase/interfaces/ifirebase_update.dart';
import '../widget/text_field.dart';

class PasswordScreen extends StatefulWidget {
  const PasswordScreen({super.key});

  @override
  State<PasswordScreen> createState() => _PasswordScreenState();
}

class _PasswordScreenState extends State<PasswordScreen> {
  late TextEditingController oldController;
  late TextEditingController newController;
  late TextEditingController confirmController;

  bool validOld = false;
  bool validNew = false;
  bool validConfirm = false;

  String instructOld = 'Password Wrong, please try again';
  String instructNew = 'as least 6 characters(uppercase,lowercase,digit)';
  String instructConfirm = 'Confirm Password is wrong';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    oldController = TextEditingController();
    newController = TextEditingController();
    confirmController = TextEditingController();
  }

  Future<bool> doUpdate() async {
    try {
      await locator<IFirebaseUpdateService>().updatePassword(confirmController.text);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text(
          "Update infomation",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            letterSpacing: 1.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 0.h,
                thickness: 6.w,
              ),
            ),
            SizedBox(
              height: 15.h,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.h,
                right: 10.w,
                left: 10.w,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Old Password",
                    style: TextStyle(
                      fontSize: 17.sp,
                      color: Colors.black54,
                    ),
                  ),
                  AccountTextField(
                      controller: oldController,
                      label: oldController.text,
                      valid: validOld,
                      isObscureText: true,
                      instructionText: 'Invalid Password'),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 20.h,
                thickness: 1.w,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.h,
                right: 10.w,
                left: 10.w,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "New Password",
                    style: TextStyle(
                      fontSize: 17.sp,
                      color: Colors.black54,
                    ),
                  ),
                  AccountTextField(
                      controller: newController,
                      label: newController.text,
                      valid: validNew,
                      isObscureText: true,
                      instructionText: 'Password must be'),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 20.h,
                thickness: 1.w,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.h,
                right: 10.w,
                left: 10.w,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Confirm",
                    style: TextStyle(
                      fontSize: 17.sp,
                      color: Colors.black54,
                    ),
                  ),
                  AccountTextField(
                      controller: confirmController,
                      label: confirmController.text,
                      valid: validConfirm,
                      isObscureText: true,
                      instructionText: 'Invalid Confirm'),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 30.h, left: 10.w, right: 10.w),
              child: InkWell(
                onTap: () {
                  setState(() {
                    oldController.text !=
                            locator<GlobalData>().currentUser!.password
                        ? validOld = true
                        : validOld = false;
                    validNew =
                        !RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$')
                            .hasMatch(newController.text);
                    confirmController.text != newController.text
                        ? validConfirm = true
                        : validConfirm = false;
                  });

                  if (!validOld && !validNew && !validConfirm) {
                    doUpdate().then((value) {
                      if (value) {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return Dialog(
                              elevation: 0,
                              backgroundColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                  side: BorderSide(
                                      color: const Color(0XFF344A43),
                                      width: 5.sp)),
                              child: SizedBox(
                                height: 350.h,
                                width: 400.w,
                                child: Column(children: [
                                  Padding(
                                    padding:
                                        EdgeInsets.only(left: 0.w, top: 20.h),
                                    child: Image.asset(
                                      'assets/images/icon_update.png',
                                      width: 180.w,
                                      height: 180.h,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(left: 0.w, top: 20.h),
                                    child: Text(
                                      "Update Success",
                                      style: TextStyle(
                                        color: const Color(0XFF344A43),
                                        fontSize: 20.sp,
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: 2.sp,
                                        fontFamily: fontPoppins,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 20.h),
                                    child: ElevatedButton(
                                      onPressed: () {
                                        Get.toNamed(MyRouter.account);
                                      },
                                      style: ElevatedButton.styleFrom(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal:
                                                30.0), // Điều chỉnh padding ngang để làm cho button dài hơn
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          side: const BorderSide(
                                              color: Colors.black, width: 2.0),
                                        ),
                                        backgroundColor: Colors.white,
                                      ),
                                      child: Container(
                                        width:
                                            150.0, // Điều chỉnh chiều dài của button
                                        child: Center(
                                          child: Text(
                                            'Close',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.bold,
                                              fontFamily: fontPoppins,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ]),
                              ),
                            );
                          },
                        );
                      }
                    });
                  }
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 50.h,
                  width: 270.w,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.black,
                    border: Border.all(
                      color: topCardBackgroundColor,
                    ),
                  ),
                  child: Text(
                    "Update",
                    style: TextStyle(
                      fontSize: 16.sp,
                      color: bottomCardBackgroundColor,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2.sp,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
