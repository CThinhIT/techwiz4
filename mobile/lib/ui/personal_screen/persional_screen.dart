import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../constants.dart';
import '../../core/global/locator.dart';
import '../../core/global/router.dart';
import '../../firebase/interfaces/ifirebase_auth.dart';

class PersionalScreen extends StatefulWidget {
  const PersionalScreen({super.key});

  @override
  State<PersionalScreen> createState() => _PersionalScreenState();
}

class _PersionalScreenState extends State<PersionalScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text(
          "Account Setting",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            letterSpacing: 1.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 10.h,
                thickness: 6.w,
              ),
            ),
            InkWell(
              onTap: () => {Get.toNamed(MyRouter.updateInfo)},
              child: Flex(
                direction: Axis.horizontal,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Wrap(
                    direction: Axis.horizontal,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      SizedBox(
                        width: 10.w,
                      ),
                      Text(
                        "Update infomation",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.sp,
                          letterSpacing: 1.sp,
                          fontFamily: fontPoppins,
                        ),
                      ),
                    ],
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.arrow_forward_ios_sharp,
                      color: hBackgroundColor,
                      size: 15.sp,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 10.h,
                thickness: 1.w,
              ),
            ),
            InkWell(
              onTap: () => {Get.toNamed(MyRouter.resetPassword)},
              child: Flex(
                direction: Axis.horizontal,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Wrap(
                    direction: Axis.horizontal,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      SizedBox(
                        width: 10.w,
                      ),
                      Text(
                        "Reset Password",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.sp,
                          letterSpacing: 1.sp,
                          fontFamily: fontPoppins,
                        ),
                      ),
                    ],
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.arrow_forward_ios_sharp,
                      color: hBackgroundColor,
                      size: 15.sp,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 10.h,
                thickness: 1.w,
              ),
            ),
            InkWell(
              onTap: () => {
                showDialog(
                  context: context,
                  builder: (context) {
                    return Dialog(
                      elevation: 0,
                      backgroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                          side: BorderSide(
                              color: const Color(0XFF344A43), width: 5.sp)),
                      child: SizedBox(
                        height: 150.h,
                        width: 400.w,
                        child: Column(children: [
                          Padding(
                            padding: EdgeInsets.only(left: 0.w, top: 20.h),
                            child: Text(
                              "Are You Sure",
                              style: TextStyle(
                                color: const Color(0XFF344A43),
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 2.sp,
                                fontFamily: fontPoppins,
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(top: 20.h),
                                child: ElevatedButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal:
                                            30.0), // Điều chỉnh padding ngang để làm cho button dài hơn
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      side: const BorderSide(
                                          color: Colors.black, width: 2.0),
                                    ),
                                    backgroundColor: Colors.white,
                                  ),
                                  child: Container(
                                    width:
                                        70.0, // Điều chỉnh chiều dài của button
                                    child: Center(
                                      child: Text(
                                        'Close',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: fontPoppins,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 20.h),
                                child: ElevatedButton(
                                  onPressed: () {
                                    locator<IFirebaseAuthService>()
                                        .deleteAccount();
                                  },
                                  style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal:
                                            30.0), // Điều chỉnh padding ngang để làm cho button dài hơn
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      side: const BorderSide(
                                          color: Colors.red, width: 2.0),
                                    ),
                                    backgroundColor: Colors.white,
                                  ),
                                  child: Container(
                                    width:
                                        70.0, // Điều chỉnh chiều dài của button
                                    child: Center(
                                      child: Text(
                                        'Delete',
                                        style: TextStyle(
                                          color: Colors.red,
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: fontPoppins,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          )
                        ]),
                      ),
                    );
                  },
                )
              },
              child: Flex(
                direction: Axis.horizontal,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Wrap(
                    direction: Axis.horizontal,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      SizedBox(
                        width: 10.w,
                      ),
                      Text(
                        "Delete Account",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.sp,
                          letterSpacing: 1.sp,
                          fontFamily: fontPoppins,
                        ),
                      ),
                    ],
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.arrow_forward_ios_sharp,
                      color: hBackgroundColor,
                      size: 15.sp,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
