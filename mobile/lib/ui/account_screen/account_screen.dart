import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/core/dto/model/order_dto.dart';
import 'package:tech_wiz4/core/dto/model/users_dto.dart';
import 'package:tech_wiz4/core/global/locator.dart';
import 'package:tech_wiz4/core/view_model/interfaces/iorder_view_model.dart';
import 'package:tech_wiz4/ui/common_widgets/custom_bottom_navigation.dart';

import '../../core/global/global_data.dart';
import '../../core/global/router.dart';
import '../../firebase/interfaces/ifirebase_auth.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({super.key});

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  final IFirebaseAuthService _authService = locator<IFirebaseAuthService>();
  List<OrderDto> orders = [];
  int confirmCount = 0;
  int pickupCount = 0;
  int deliveryCount = 0;
  UsersDto? currentUser;
  late IOrderViewModel vm;
  @override
  void initState() {
    currentUser = _authService.currentUser;
    vm = context.read<IOrderViewModel>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      // appBar: AppBar(
      //   elevation: 0,
      //   backgroundColor: Colors.transparent,
      //   actions: const [
      //     // CustomAppBar(),
      //   ],
      // ),
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                color: topCardBackgroundColor,
              ),
              child: Padding(
                padding: EdgeInsets.only(top: 70.h, bottom: 30.h, left: 20.w),
                child: Row(
                  children: [
                    ClipOval(
                      child: Image.asset(
                        'assets/images/profile_img.png',
                        width: 60.w,
                        height: 60.h,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10.w),
                      child: Text(
                        locator<GlobalData>().currentUser!.username,
                        style: TextStyle(
                            fontFamily: fontPoppins,
                            fontSize: 20.sp,
                            color: Colors.black,
                            letterSpacing: 1.sp),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              decoration: const BoxDecoration(),
              child: Padding(
                padding: EdgeInsets.only(top: 30.h),
                child: Column(
                  children: [
                    Flex(
                      direction: Axis.horizontal,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        InkWell(
                          onTap: () {
                            vm.getConfirm();
                            vm.getPickup();
                            vm.getDelivery();
                            Get.toNamed(MyRouter.tab, arguments: 0);
                          },
                          child: Stack(
                            children: [
                              Column(
                                children: [
                                  SizedBox(
                                    height: 10.h,
                                  ),
                                  Image.asset(
                                    'assets/images/icon_confirm.png',
                                    width: 30.w,
                                    height: 30.h,
                                    fit: BoxFit.cover,
                                  ),
                                  SizedBox(
                                    height: 10.h,
                                  ),
                                  Text(
                                    "Confirm",
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 13.sp,
                                    ),
                                  ),
                                ],
                              ),
                              confirmCount != 0
                                  ? Container(
                                      alignment: Alignment.center,
                                      margin: EdgeInsets.only(
                                        left: 22.w,
                                      ),
                                      height: 20.h,
                                      width: 20.h,
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(100.r),
                                      ),
                                      child: Text(
                                        '${confirmCount}',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.sp,
                                        ),
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            vm.getConfirm();
                            vm.getPickup();
                            vm.getDelivery();
                            Get.toNamed(MyRouter.tab, arguments: 1);
                          },
                          child: Stack(
                            children: [
                              Column(
                                children: [
                                  SizedBox(
                                    height: 10.h,
                                  ),
                                  Image.asset(
                                    'assets/images/icon_take.png',
                                    width: 30.w,
                                    height: 30.h,
                                    fit: BoxFit.cover,
                                  ),
                                  SizedBox(
                                    height: 10.h,
                                  ),
                                  Text(
                                    "Pickup",
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 13.sp,
                                    ),
                                  ),
                                ],
                              ),
                              pickupCount != 0
                                  ? Container(
                                      alignment: Alignment.center,
                                      margin: EdgeInsets.only(
                                        left: 25.w,
                                      ),
                                      height: 20.h,
                                      width: 20.h,
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(100.r),
                                      ),
                                      child: Text(
                                        '$pickupCount',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.sp,
                                        ),
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            vm.getConfirm();
                            vm.getPickup();
                            vm.getDelivery();
                            Get.toNamed(MyRouter.tab, arguments: 2);
                          },
                          child: Stack(
                            children: [
                              Column(
                                children: [
                                  SizedBox(
                                    height: 10.h,
                                  ),
                                  Image.asset(
                                    'assets/images/icon_delivery.png',
                                    width: 30.w,
                                    height: 30.h,
                                    fit: BoxFit.cover,
                                  ),
                                  SizedBox(
                                    height: 10.h,
                                  ),
                                  Text(
                                    "Delivery",
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontSize: 13.sp,
                                    ),
                                  ),
                                ],
                              ),
                              deliveryCount != 0
                                  ? Container(
                                      alignment: Alignment.center,
                                      margin: EdgeInsets.only(
                                        left: 22.w,
                                      ),
                                      height: 20.h,
                                      width: 20.h,
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius:
                                            BorderRadius.circular(100.r),
                                      ),
                                      child: Text(
                                        '$deliveryCount',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12.sp,
                                        ),
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 0.w, right: 0.w),
                      child: Divider(
                        color: topCardBackgroundColor,
                        height: 5.h,
                        thickness: 5.w,
                      ),
                    ),
                    InkWell(
                      onTap: () => {Get.toNamed(MyRouter.persional)},
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Wrap(
                            direction: Axis.horizontal,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              SizedBox(width: 10.w),
                              ClipOval(
                                child: Image.asset(
                                  'assets/images/icon_user.png',
                                  width: 25.w,
                                  height: 25.h,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(
                                width: 10.w,
                              ),
                              Text(
                                "Account setting",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.sp,
                                  letterSpacing: 1.sp,
                                  fontFamily: fontPoppins,
                                ),
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: hBackgroundColor,
                              size: 15.sp,
                            ),
                          ),
                        ],
                      ),
                    ), //Account
                    Container(
                      margin: EdgeInsets.only(left: 0.w, right: 0.w),
                      child: Divider(
                        color: topCardBackgroundColor,
                        height: 0.h,
                        thickness: 1.w,
                      ),
                    ),
                    InkWell(
                      onTap: () => {
                        vm.getHistoryOrder(),
                        Get.toNamed(
                          MyRouter.history,
                        )
                      },
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Wrap(
                            direction: Axis.horizontal,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              SizedBox(width: 10.w),
                              ClipOval(
                                child: Image.asset(
                                  'assets/images/icon_invoice.png',
                                  width: 25.w,
                                  height: 25.h,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(width: 10.w),
                              Text(
                                "History",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.sp,
                                  letterSpacing: 1.sp,
                                  fontFamily: fontPoppins,
                                ),
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: hBackgroundColor,
                              size: 15.sp,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 0.w, right: 0.w),
                      child: Divider(
                        color: topCardBackgroundColor,
                        height: 0.h,
                        thickness: 1.w,
                      ),
                    ),
                    InkWell(
                      onTap: () => {},
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Wrap(
                            direction: Axis.horizontal,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              SizedBox(width: 10.w),
                              ClipOval(
                                child: Image.asset(
                                  'assets/images/icon_heart.png',
                                  width: 25.w,
                                  height: 25.h,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(width: 10.w),
                              Text(
                                "Favorites",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.sp,
                                  letterSpacing: 1.sp,
                                  fontFamily: fontPoppins,
                                ),
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: hBackgroundColor,
                              size: 15,
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.only(left: 0.w, right: 0.w),
                      child: Divider(
                        color: topCardBackgroundColor,
                        height: 5.w,
                        thickness: 5.w,
                      ),
                    ),
                    InkWell(
                      onTap: () => {Get.toNamed(MyRouter.help)},
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Wrap(
                            direction: Axis.horizontal,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              SizedBox(width: 10.w),
                              ClipOval(
                                child: Image.asset(
                                  'assets/images/icon_question.png',
                                  width: 25.w,
                                  height: 25.h,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(
                                width: 10.w,
                              ),
                              Text(
                                "Help Center",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.sp,
                                  letterSpacing: 1.sp,
                                  fontFamily: fontPoppins,
                                ),
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: hBackgroundColor,
                              size: 15.sp,
                            ),
                          ),
                        ],
                      ),
                    ), //Help Center
                    Container(
                      margin: EdgeInsets.only(left: 0.w, right: 0.w),
                      child: Divider(
                        color: topCardBackgroundColor,
                        height: 0.h,
                        thickness: 1.w,
                      ),
                    ),
                    InkWell(
                      onTap: () => {Get.toNamed(MyRouter.help)},
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Wrap(
                            direction: Axis.horizontal,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              SizedBox(width: 10.w),
                              ClipOval(
                                child: Image.asset(
                                  'assets/images/icon_rating.png',
                                  width: 25.w,
                                  height: 25.h,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(
                                width: 10.w,
                              ),
                              Text(
                                "Feedback",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.sp,
                                  letterSpacing: 1.sp,
                                  fontFamily: fontPoppins,
                                ),
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: hBackgroundColor,
                              size: 15.sp,
                            ),
                          ),
                        ],
                      ),
                    ), //Help Center
                    Container(
                      margin: EdgeInsets.only(left: 0.w, right: 0.w),
                      child: Divider(
                        color: topCardBackgroundColor,
                        height: 0.h,
                        thickness: 1.w,
                      ),
                    ),
                    InkWell(
                      onTap: () => {_authService.signOut()},
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Wrap(
                            direction: Axis.horizontal,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: [
                              SizedBox(width: 10.w),
                              ClipOval(
                                child: Image.asset(
                                  'assets/images/icon_logout.png',
                                  width: 25.w,
                                  height: 25.h,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(
                                width: 10.w,
                              ),
                              Text(
                                "Logout",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.sp,
                                  letterSpacing: 1.sp,
                                  fontFamily: fontPoppins,
                                ),
                              ),
                            ],
                          ),
                          IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: Colors.white,
                              size: 15.sp,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar:
          const CustomBottomNavigationBar(value: NavigationBars.account),
    );
  }
}
