import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';

import 'package:tech_wiz4/core/global/router.dart';

import 'package:tech_wiz4/core/view_model/interfaces/ispecies_view_model.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_auth.dart';
import 'package:tech_wiz4/ui/common_widgets/custom_bottom_navigation.dart';

import '../../core/global/global_data.dart';
import '../../core/global/locator.dart';

class ResultSearchScreen extends StatefulWidget {
  const ResultSearchScreen({Key? key}) : super(key: key);

  @override
  State<ResultSearchScreen> createState() => ResultSearchsScreenState();
}

class ResultSearchsScreenState extends State<ResultSearchScreen> {
  final TextEditingController _searchController = TextEditingController();
  final IFirebaseAuthService _authService = locator<IFirebaseAuthService>();
  bool isLoading = true;
  List<ProductDto> searchResults = [];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    searchResults =
        ModalRoute.of(context)?.settings.arguments as List<ProductDto>;
    Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        isLoading = false;
      });
    });
  }

  late ISpeciesViewModel vm;
  @override
  void initState() {
    super.initState();
    vm = context.read<ISpeciesViewModel>();
  }

  void _performSearch(String query) {
    vm.searchByname(query);
    setState(() {
      searchResults = vm.listAllSearch;
    });
    Navigator.pushNamed(context, MyRouter.searchresult,
        arguments: searchResults);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        title: Text(
          "PlantPal",
          style: TextStyle(
            color: kPrimaryColor,
            fontSize: 18.sp,
            letterSpacing: 5.sp,
            fontWeight: FontWeight.bold,
          ),
        ),
        leading: Image.asset(
          'assets/images/logo.png',
          width: 50.w,
          height: 50.h,
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      endDrawer: DrawerE(authService: _authService),
      body: Consumer<ISpeciesViewModel>(
        builder: (context, vm, child) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 19.w, left: 19.w, top: 40.h),
                  child: TextField(
                    controller: _searchController,
                    onSubmitted: (value) {
                      _performSearch(value);
                    },
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 1.w,
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.circular(16.r),
                      ),
                      suffixIcon: IconButton(
                        onPressed: () {
                          _performSearch(_searchController.text);
                        },
                        icon: const Icon(
                          Icons.search,
                          color: Colors.black,
                        ),
                      ),
                      label: const Text(
                        "Search",
                        style: TextStyle(color: Colors.black),
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                          width: 3.w,
                          color: Colors.black,
                        ),
                        borderRadius: BorderRadius.circular(16.r),
                      ),
                    ),
                  ),
                ),
                if (isLoading)
                  const Center(
                    child: CircularProgressIndicator(),
                  ),
                if (!isLoading)
                  Padding(
                    padding: EdgeInsets.only(top: 5.h),
                    child: SizedBox(
                      height: 400.h,
                      width: double.maxFinite,
                      child: GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 0.45,
                        ),
                        itemCount: searchResults.length,
                        itemBuilder: (_, index) {
                          return InkWell(
                            onTap: () {
                              Get.toNamed(
                                MyRouter.detail,
                                arguments: searchResults[index],
                              );
                            },
                            child: Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      left: 10.w,
                                      bottom: 20.h,
                                      top: 20.h,
                                      right: 10.w),
                                  height: 346.h,
                                  width: 198.w,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: topCardBackgroundColor,
                                    border: Border.all(
                                      color: topCardBackgroundColor,
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.2),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Container(
                                        color: topCardBackgroundColor,
                                        margin: EdgeInsets.only(
                                          bottom: 1.h,
                                        ),
                                        child: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                IconButton(
                                                  onPressed: () {
                                                    setState(() {
                                                      // addToFavorite(
                                                      //     searchResults[
                                                      //         index]);
                                                    });
                                                  },
                                                  icon: const Icon(
                                                      Icons.favorite,
                                                      color: Colors.red),
                                                ),
                                              ],
                                            ),
                                            Image.network(
                                              searchResults[index].image,
                                              height: 195.h,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 5.h),
                                        child: Container(
                                          color: bottomCardBackgroundColor,
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                                left: 10.w,
                                                right: 10.w,
                                                top: 5.h,
                                                bottom: 5.h),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  searchResults[index].name,
                                                  style: TextStyle(
                                                    fontSize: 16.sp,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                                Text(
                                                  searchResults[index]
                                                      .cate
                                                      .name,
                                                  style: TextStyle(
                                                    fontSize: 16.sp,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                                Text(
                                                  searchResults[index]
                                                      .spec
                                                      .name,
                                                  style: TextStyle(
                                                    fontSize: 16.sp,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      vm.listAllPlant[index]
                                                          .cate.name,
                                                    ),
                                                    Text(
                                                      '\$${searchResults[index].price}',
                                                      style: TextStyle(
                                                        fontSize: 16.sp,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                      ),
                                                    ),
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
              ],
            ),
          );
        },
      ),
      bottomNavigationBar:
          const CustomBottomNavigationBar(value: NavigationBars.home),
    );
  }
}

class DrawerE extends StatelessWidget {
  const DrawerE({
    super.key,
    required IFirebaseAuthService authService,
  }) : _authService = authService;

  final IFirebaseAuthService _authService;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 70.h, bottom: 30.h, left: 20.w),
            child: Row(
              children: [
                ClipOval(
                  child: Image.asset(
                    'assets/images/profile_img.png',
                    width: 60.w,
                    height: 60.h,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 10.w),
                  child: Text(
                    locator<GlobalData>().currentUser!.username,
                    style: TextStyle(
                        fontFamily: fontPoppins,
                        fontSize: 20.sp,
                        color: Colors.black,
                        letterSpacing: 1.sp),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20.h,
          ),
          InkWell(
            onTap: () => {},
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Wrap(
                  direction: Axis.horizontal,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    SizedBox(width: 10.w),
                    ClipOval(
                      child: Image.asset(
                        'assets/images/icon_heart.png',
                        width: 25.w,
                        height: 25.h,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(width: 10.w),
                    Text(
                      "Favorites",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.sp,
                        letterSpacing: 1.sp,
                        fontFamily: fontPoppins,
                      ),
                    ),
                  ],
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: hBackgroundColor,
                    size: 15,
                  ),
                ),
              ],
            ),
          ),

          Container(
            margin: EdgeInsets.only(left: 0.w, right: 0.w),
            child: Divider(
              color: topCardBackgroundColor,
              height: 5.w,
              thickness: 5.w,
            ),
          ),
          InkWell(
            onTap: () => {Get.toNamed(MyRouter.help)},
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Wrap(
                  direction: Axis.horizontal,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    SizedBox(width: 10.w),
                    ClipOval(
                      child: Image.asset(
                        'assets/images/icon_question.png',
                        width: 25.w,
                        height: 25.h,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 10.w,
                    ),
                    Text(
                      "Help Center",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.sp,
                        letterSpacing: 1.sp,
                        fontFamily: fontPoppins,
                      ),
                    ),
                  ],
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: hBackgroundColor,
                    size: 15.sp,
                  ),
                ),
              ],
            ),
          ), //Help Center
          Container(
            margin: EdgeInsets.only(left: 0.w, right: 0.w),
            child: Divider(
              color: topCardBackgroundColor,
              height: 0.h,
              thickness: 1.w,
            ),
          ),
          InkWell(
            onTap: () => {Get.toNamed(MyRouter.help)},
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Wrap(
                  direction: Axis.horizontal,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    SizedBox(width: 10.w),
                    ClipOval(
                      child: Image.asset(
                        'assets/images/icon_rating.png',
                        width: 25.w,
                        height: 25.h,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 10.w,
                    ),
                    Text(
                      "Feedback",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.sp,
                        letterSpacing: 1.sp,
                        fontFamily: fontPoppins,
                      ),
                    ),
                  ],
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: hBackgroundColor,
                    size: 15.sp,
                  ),
                ),
              ],
            ),
          ), //Help Center
          Container(
            margin: EdgeInsets.only(left: 0.w, right: 0.w),
            child: Divider(
              color: topCardBackgroundColor,
              height: 0.h,
              thickness: 1.w,
            ),
          ),
          InkWell(
            onTap: () => {_authService.signOut()},
            child: Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Wrap(
                  direction: Axis.horizontal,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    SizedBox(width: 10.w),
                    ClipOval(
                      child: Image.asset(
                        'assets/images/icon_logout.png',
                        width: 25.w,
                        height: 25.h,
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(
                      width: 10.w,
                    ),
                    Text(
                      "Logout",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.sp,
                        letterSpacing: 1.sp,
                        fontFamily: fontPoppins,
                      ),
                    ),
                  ],
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: Colors.white,
                    size: 15.sp,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
