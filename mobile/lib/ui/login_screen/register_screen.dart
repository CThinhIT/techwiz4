import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:tech_wiz4/ui/login_screen/witget/text_field_register.dart';
import '../../constants.dart';
import '../../core/global/locator.dart';
import '../../core/global/router.dart';
import '../../firebase/interfaces/ifirebase_auth.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late TextEditingController nameController;
  late TextEditingController phoneController;
  late TextEditingController emailController;
  late TextEditingController passwordController;

  bool validName = false;
  bool validEmail = false;
  bool validPhone = false;
  bool validPassword = false;

  @override
  void setState(VoidCallback fn) {
    // TODO: implement setState
    super.setState(fn);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nameController = TextEditingController();
    phoneController = TextEditingController();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    passwordController.dispose();
  }

  void doRegister() async {
    setState(() {
      validName = !RegExp(r"^[a-zA-Z0-9]+$").hasMatch(nameController.text);
      validPhone = !RegExp(r"^[0-9]{6,11}$").hasMatch(phoneController.text);
      validEmail = !RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(emailController.text);
      validPassword = !RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{6,}$')
          .hasMatch(passwordController.text);
    });

    if (!validName && !validPhone && !validEmail && !validPassword) {
      locator<IFirebaseAuthService>().signUp(
        nameController.text,
        emailController.text,
        phoneController.text,
        passwordController.text,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/image_login.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 35.w, top: 58.h, right: 35.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 100.h,
                      ),
                      Text(
                        'Register',
                        style: TextStyle(
                          fontSize: 30.sp,
                          fontFamily: fontPoppins,
                          letterSpacing: 3.sp,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 22.h,
                      ),
                      MyTextFieldRegister(
                        controller: nameController,
                        label: "Username",
                        isObscureText: false,
                        valid: validName,
                        icon: Icons.abc,
                        instruction: 'Invalid Username',
                      ),
                      SizedBox(
                        height: 22.5.h,
                      ),
                      MyTextFieldRegister(
                        controller: emailController,
                        label: "Email",
                        isObscureText: false,
                        valid: validEmail,
                        icon: Icons.email,
                        instruction: 'Incvalid Email',
                      ),
                      SizedBox(
                        height: 22.5.h,
                      ),
                      MyTextFieldRegister(
                        controller: phoneController,
                        label: "Phone",
                        isObscureText: false,
                        valid: validPhone,
                        icon: Icons.email,
                        instruction: 'Invalid phone number (6-11 digits)',
                      ),
                      SizedBox(
                        height: 22.5.h,
                      ),
                      MyTextFieldRegister(
                        controller: passwordController,
                        label: "Password",
                        isObscureText: true,
                        valid: validPassword,
                        icon: Icons.lock,
                        instruction:
                            'as least 6 characters(uppercase,lowercase,digit)',
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 36.5.h,
                ),
                InkWell(
                  onTap: () {
                    doRegister();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 47.h,
                    width: 304.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: kPrimaryColor,
                      border: Border.all(
                        color: kPrimaryColor,
                      ),
                    ),
                    child: Text(
                      "Register",
                      style: TextStyle(
                        fontSize: 16.sp,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 2.sp,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.h, bottom: 9.h),
                  child: Text(
                    "Or Register With",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12.sp,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 0.h),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Already have an account ?",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12.sp,
                        ),
                      ),
                      TextButton(
                        onPressed: () => Get.toNamed(MyRouter.login),
                        child: Text(
                          "Login here",
                          style: TextStyle(
                            color: kPrimaryColor,
                            fontSize: 12.sp,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Padding methodRegister(String lable, {bool isObscureText = false}) {
    return Padding(
      padding: EdgeInsets.only(right: 31.5.w),
      child: TextField(
        keyboardType: TextInputType.emailAddress,
        obscureText: isObscureText,
        decoration: InputDecoration(
          label: Text(
            lable,
            style: TextStyle(
              color: tHintColor,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
          prefixIcon: Icon(
            isObscureText ? Icons.lock : Icons.account_circle_outlined,
            color: kPrimaryColor,
          ),
        ),
      ),
    );
  }
}
