import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/core/global/locator.dart';
import 'package:tech_wiz4/core/global/router.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_order.dart';
import 'package:tech_wiz4/ui/common_widgets/custom_bottom_navigation.dart';

import '../../constants.dart';
import '../../core/dto/model/product_dto.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  late IBasketViewViewModel vm;
  final IFirebaseOrderService _firebaseOrderService =
      locator<IFirebaseOrderService>();
  @override
  void initState() {
    vm = context.read<IBasketViewViewModel>();
    _firebaseOrderService;
    super.initState();
  }

  void deteleProduct(ProductDto id) {
    vm.deleteToCart(id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: bottomCardBackgroundColor,
        centerTitle: true,
        title: Text(
          "Shopping Cart",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            letterSpacing: 1.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
        elevation: 0,
      ),
      body: Consumer<IBasketViewViewModel>(
        builder: (context, vm, child) {
          return SafeArea(
            child: SingleChildScrollView(
              child: vm.totalItems == 0
                  ? Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 200.h,
                          ),
                          Text(
                            "There are no products in the cart yet",
                            style: TextStyle(
                              fontSize: 18.sp,
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Get.toNamed(MyRouter.home);
                            },
                            child: Container(
                              margin: EdgeInsets.only(top: 20.h),
                              alignment: Alignment.center,
                              height: 50.h,
                              width: 200.w,
                              decoration: BoxDecoration(
                                color: kPrimaryColor,
                                borderRadius: BorderRadius.circular(16.r),
                                border: Border.all(color: Colors.black),
                              ),
                              child: Text(
                                "Back to shopping",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.sp,
                                    fontFamily: fontPoppins),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Column(
                      children: [
                        SizedBox(
                          height: 590.h,
                          child: ListView.builder(
                            itemBuilder: (_, index) {
                              return Slidable(
                                endActionPane: ActionPane(
                                    motion: const ScrollMotion(),
                                    children: [
                                      SlidableAction(
                                        onPressed: (context) {
                                          deteleProduct(
                                              vm.cartItems[index].productId);
                                        },
                                        backgroundColor: Color(0xFFFE4A49),
                                        foregroundColor: Colors.white,
                                        icon: Icons.delete,
                                        label: 'Delete',
                                      ),
                                    ]),
                                child: Container(
                                  margin: EdgeInsets.only(top: 20.h),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: topCardBackgroundColor,
                                    border: Border.all(
                                      color: topCardBackgroundColor,
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.1),
                                        spreadRadius: 5,
                                        blurRadius: 7,
                                        offset: const Offset(
                                            0, 3), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  height: 130.h,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Image.network(
                                          vm.cartItems[index].productId.image,
                                          // height: 100.h,
                                          // width: 100.w,
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          SizedBox(
                                            height: 10.h,
                                          ),
                                          Text(
                                            vm.cartItems[index].productId.name,
                                            style: TextStyle(
                                              fontSize: 17.sp,
                                              fontWeight: FontWeight.w400,
                                              letterSpacing: 1.sp,
                                            ),
                                          ),
                                          Text(
                                            vm.cartItems[index].productId.spec
                                                .name,
                                            style: TextStyle(
                                              fontSize: 13.sp,
                                              fontWeight: FontWeight.w300,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 3.h,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Text(
                                                '\$',
                                                style: TextStyle(
                                                  fontSize: 15.sp,
                                                  fontWeight: FontWeight.w300,
                                                  height: .5.h,
                                                ),
                                              ),
                                              Text(
                                                '${vm.cartItems[index].productId.price}',
                                                style: TextStyle(
                                                  fontSize: 20.sp,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            ],
                                          ),
                                          Container(
                                            width: 90.w,
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 10.w,
                                                vertical: 2.h),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                InkWell(
                                                  onTap: () => {
                                                    vm.removeToCart(vm
                                                        .cartItems[index]
                                                        .productId)
                                                  },
                                                  child: Icon(
                                                    Icons.remove,
                                                    color:
                                                        const Color(0xFF454545),
                                                    size: 18.sp,
                                                  ),
                                                ),
                                                Text(
                                                  '${vm.cartItems[index].quantity}',
                                                  style: TextStyle(
                                                    fontSize: 17.sp,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () => {
                                                    vm.addToCart(vm
                                                        .cartItems[index]
                                                        .productId)
                                                  },
                                                  child: Icon(
                                                    Icons.add,
                                                    color:
                                                        const Color(0xFF454545),
                                                    size: 18.sp,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5.h,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                'Total:',
                                                style: TextStyle(
                                                  fontSize: 15.sp,
                                                  fontWeight: FontWeight.w300,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 5.w,
                                              ),
                                              Text(
                                                '${vm.cartItems[index].price}',
                                                style: TextStyle(
                                                  fontSize: 17.sp,
                                                  fontWeight: FontWeight.w400,
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                            itemCount: vm.cartItems.length,
                            // scrollDirection: Axis.vertical,
                            // shrinkWrap: true,
                          ),
                        ),
                        Positioned(
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              padding: EdgeInsets.only(bottom: 10.h, top: 10.h),
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: topCardBackgroundColor,
                                border: Border.all(
                                  color: bottomCardBackgroundColor,
                                  width: 2.0,
                                ),
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30.r),
                                    topRight: Radius.circular(30.r)),
                                boxShadow: const [
                                  BoxShadow(
                                    color: tHintColor,
                                    spreadRadius: 3,
                                    blurRadius: 7,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Wrap(
                                spacing: double.infinity,
                                direction: Axis.horizontal,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      right: 50.w,
                                      left: 50.w,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                      right: 50.w,
                                      left: 50.w,
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Total',
                                          style: TextStyle(
                                            fontSize: 30.sp,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        Text(
                                          '\$${vm.totalPrice}',
                                          style: TextStyle(
                                            fontSize: 30.sp,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () => {
                                      Get.toNamed(MyRouter.checkout),
                                    },
                                    child: Container(
                                      width: double.infinity,
                                      margin: EdgeInsets.only(
                                          right: 50.w, left: 50.w, top: 5.h),
                                      padding: EdgeInsets.only(
                                          top: 10.h, bottom: 10.h),
                                      decoration: BoxDecoration(
                                        color: Colors.black87,
                                        borderRadius:
                                            BorderRadius.circular(16.r),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'Checkout',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 17.sp,
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
            ),
          );
        },
      ),
      bottomNavigationBar:
          const CustomBottomNavigationBar(value: NavigationBars.shopping),
    );
  }
}
