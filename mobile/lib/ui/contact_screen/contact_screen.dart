import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:tech_wiz4/constants.dart';

import 'package:tech_wiz4/core/model_ui/carousel_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/species_model_ui.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({super.key});

  @override
  State<ContactScreen> createState() => ContactScreenState();
}

class ContactScreenState extends State<ContactScreen> {
  final List<CarouselModelUI> carousels = CarouselModelUI.items;
  final List<SpeciesModelUI> categorys = SpeciesModelUI.items;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background_contact.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 60.h),
                child: Center(
                  child: Text(
                    "Contact Us",
                    style: TextStyle(
                      fontSize: 25.sp,
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF325223),
                      fontFamily: fontPoppins,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 100.h, right: 160.w),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.phone,
                        size: 24.sp,
                        color: Color(0xFF325223),
                      ),
                      SizedBox(width: 10.w),
                      Text(
                        "000-111-222-333",
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontFamily: fontPoppins,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30.h, right: 140.w),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.email,
                        size: 24.sp,
                        color: Color(0xFF325223),
                      ),
                      SizedBox(width: 10), // Khoảng cách giữa icon và văn bản
                      Text(
                        "abc12@gmail.com",
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontFamily: fontPoppins,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30.h, left: 40.w),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.location_on_outlined,
                        size: 24.sp,
                        color: Color(0xFF325223),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Text(
                          "590 Cach Mang Thang 8, Ward 11, District 3, Ho Chi Minh City",
                          style: TextStyle(
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontFamily: fontPoppins,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.h, left: 0.w),
                child: SizedBox(
                  width: 300.w,
                  child: const TextField(
                    decoration: InputDecoration(
                      hintText: "Name",
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.green),
                      ),
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w300, color: Colors.grey),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.h, left: 0.w),
                child: SizedBox(
                  width: 300.w,
                  child: const TextField(
                    decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                        hintText: "Email",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w300, color: Colors.grey)),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20.h, left: 0.w),
                child: SizedBox(
                  width: 300.w,
                  child: const TextField(
                    maxLines: null,
                    decoration: InputDecoration(
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.green),
                        ),
                        hintText: "Message",
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w300, color: Colors.grey)),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(20.h),
                child: TextButton(
                  onPressed: () {},
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.green[600],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  child: Text(
                    "Send Message",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.sp,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    ));
  }
}
