import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/core/view_model/interfaces/iorder_view_model.dart';

import '../../constants.dart';

class HistoryOrderScreen extends StatefulWidget {
  const HistoryOrderScreen({super.key});

  @override
  State<HistoryOrderScreen> createState() => _HistoryOrderScreenState();
}

class _HistoryOrderScreenState extends State<HistoryOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text(
          "History Order",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            letterSpacing: 1.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: SizedBox(
        height: double.infinity,
        child: Consumer<IOrderViewModel>(
          builder: (context, vm, child) {
            return Padding(
              padding: EdgeInsets.only(left: 20.w, right: 20.w),
              child: ListView.builder(
                itemBuilder: (_, index) {
                  return Container(
                    margin: EdgeInsets.only(top: 20.h, bottom: 20.h),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: topCardBackgroundColor,
                      border: Border.all(
                        color: topCardBackgroundColor,
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                              const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    height: 130.h,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        // Padding(
                        //   padding: const EdgeInsets.all(8.0),
                        //   child: Image.network(
                        //     vm.historyOrders[index].status.image,
                        //   ),
                        // ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10.h,
                            ),
                            // Text(
                            //   vm.historyOrders[index].status.toString(),
                            //   style: TextStyle(
                            //     fontSize: 17.sp,
                            //     fontWeight: FontWeight.w400,
                            //     letterSpacing: 1.sp,
                            //   ),
                            // ),
                            // Text(
                            //   vm.historyOrders[index].userId.id.toString(),
                            //   style: TextStyle(
                            //     fontSize: 13.sp,
                            //     fontWeight: FontWeight.w300,
                            //   ),
                            // ),
                            SizedBox(
                              height: 3.h,
                            ),
                            SizedBox(
                              width: 70.w,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  // Row(
                                  //   children: [
                                  //     Text(
                                  //       '\$',
                                  //       style: TextStyle(
                                  //         fontSize: 15.sp,
                                  //         fontWeight: FontWeight.w300,
                                  //         height: .5.h,
                                  //       ),
                                  //     ),
                                  //     Text(
                                  //       '${vm.historyOrders[index].price}',
                                  //       style: TextStyle(
                                  //         fontSize: 17.sp,
                                  //         fontWeight: FontWeight.w400,
                                  //       ),
                                  //     ),
                                  //   ],
                                  // ),
                                  Text(
                                    'x${vm.historyOrders[index].quantity}',
                                    style: TextStyle(
                                      fontSize: 17.sp,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 5.h,
                            ),
                            Row(
                              children: [
                                Text(
                                  'Total:',
                                  style: TextStyle(
                                    fontSize: 15.sp,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                                SizedBox(
                                  width: 5.w,
                                ),
                                Text(
                                  '${vm.historyOrders[index].price}',
                                  style: TextStyle(
                                    fontSize: 17.sp,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            InkWell(
                              onTap: () => {
                                // Get.toNamed(MyRouter.checkout),
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15.w, vertical: 7.h),
                                decoration: BoxDecoration(
                                  color: Colors.black87,
                                  borderRadius: BorderRadius.circular(5.r),
                                ),
                                child: Center(
                                  child: Text(
                                    'Buy again',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 13.sp,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
                itemCount: vm.historyOrders.length,
                // scrollDirection: Axis.vertical,
                // shrinkWrap: true,
              ),
            );
          },
        ),
      ),
    );
  }
}
