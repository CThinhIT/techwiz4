import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../constants.dart';
import '../../core/view_model/interfaces/ibasket_view_model.dart';

class FeedbackOrderScreen extends StatefulWidget {
  const FeedbackOrderScreen({super.key});

  @override
  State<FeedbackOrderScreen> createState() => _FeedbackOrderScreenState();
}

class _FeedbackOrderScreenState extends State<FeedbackOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: double.infinity,
      child: Consumer<IBasketViewViewModel>(
          builder: (context, vm, child) {
            return Padding(
              padding: EdgeInsets.only(left: 20.w, right: 20.w),
              child: ListView.builder(
                itemBuilder: (_, index) {
                  return Container(
                    margin: EdgeInsets.only(top: 20.h,bottom: 20.h),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: topCardBackgroundColor,
                      border: Border.all(
                        color: topCardBackgroundColor,
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(
                              0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    height: 130.h,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.network(
                            vm.cartItems[index].productId.image,
                            // height: 100.h,
                            // width: 100.w,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10.h,
                            ),
                            Text(
                              vm.cartItems[index].productId.name,
                              style: TextStyle(
                                fontSize: 17.sp,
                                fontWeight: FontWeight.w400,
                                letterSpacing: 1.sp,
                              ),
                            ),
                            Text(
                              vm.cartItems[index].productId.spec.name,
                              style: TextStyle(
                                fontSize: 13.sp,
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                            SizedBox(
                              height: 3.h,
                            ),
                            SizedBox(
                              width: 70.w,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        '\$',
                                        style: TextStyle(
                                          fontSize: 15.sp,
                                          fontWeight: FontWeight.w300,
                                          height: .5.h,
                                        ),
                                      ),
                                      Text(
                                        '22',
                                        style: TextStyle(
                                          fontSize: 17.sp,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    'x 2',
                                    style: TextStyle(
                                      fontSize: 17.sp,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 5.h,
                            ),
                            Row(
                              children: [
                                Text(
                                  'Total:',
                                  style: TextStyle(
                                    fontSize: 15.sp,
                                    fontWeight: FontWeight.w300,
                                  ),
                                ),
                                SizedBox(
                                  width: 5.w,
                                ),
                                Text(
                                  '44',
                                  style: TextStyle(
                                    fontSize: 17.sp,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ],
                            ),
                            InkWell(
                              onTap: () => {
                                // Get.toNamed(MyRouter.checkout),
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 15.w, vertical: 7.h),
                                decoration: BoxDecoration(
                                  color: tHintColor,
                                  borderRadius: BorderRadius.circular(5.r),
                                ),
                                child: Center(
                                  child: Text(
                                    'Feedback',
                                    style: TextStyle(
                                      color: topCardBackgroundColor,
                                      fontSize: 15.sp,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
                itemCount: vm.cartItems.length,
                // scrollDirection: Axis.vertical,
                // shrinkWrap: true,
              ),
            );
          },
        ),
    );
  }
}