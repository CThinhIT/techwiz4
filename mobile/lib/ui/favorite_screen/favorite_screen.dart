import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/core/global/locator.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_order.dart';
import 'package:tech_wiz4/ui/common_widgets/custom_bottom_navigation.dart';

import '../../constants.dart';
import '../../core/dto/model/product_dto.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({super.key});

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  late IBasketViewViewModel vm;
  final IFirebaseOrderService _firebaseOrderService =
      locator<IFirebaseOrderService>();
  @override
  void initState() {
    vm = context.read<IBasketViewViewModel>();
    _firebaseOrderService;
    super.initState();
  }

  void deteleProduct(ProductDto id) {
    vm.deleteToCart(id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: bottomCardBackgroundColor,
        centerTitle: true,
        title: Text(
          "Favorite Product",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            letterSpacing: 1.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
        elevation: 0,
      ),
      body: Consumer<IBasketViewViewModel>(
        builder: (context, vm, child) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 20.w, right: 20.w),
                    child: SizedBox(
                      height: 590.h,
                      child: ListView.builder(
                        itemBuilder: (_, index) {
                          return Slidable(
                            endActionPane: ActionPane(
                                motion: const ScrollMotion(),
                                children: [
                                  SlidableAction(
                                    onPressed: (context) {
                                      deteleProduct(
                                          vm.favorites[index].productId);
                                    },
                                    backgroundColor: Color(0xFFFE4A49),
                                    foregroundColor: Colors.white,
                                    icon: Icons.delete,
                                    label: 'Delete',
                                  ),
                                ]),
                            child: Container(
                              margin: EdgeInsets.only(top: 20.h),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: topCardBackgroundColor,
                                border: Border.all(
                                  color: topCardBackgroundColor,
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black.withOpacity(0.1),
                                    spreadRadius: 5,
                                    blurRadius: 7,
                                    offset: const Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                              ),
                              height: 130.h,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Image.network(
                                      vm.favorites[index].productId.image,
                                      // height: 100.h,
                                      // width: 100.w,
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 10.h,
                                      ),
                                      Text(
                                        vm.favorites[index].productId.name,
                                        style: TextStyle(
                                          fontSize: 17.sp,
                                          fontWeight: FontWeight.w400,
                                          letterSpacing: 1.sp,
                                        ),
                                      ),
                                      Text(
                                        vm.favorites[index].productId.spec.name,
                                        style: TextStyle(
                                          fontSize: 13.sp,
                                          fontWeight: FontWeight.w300,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 3.h,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            '\$',
                                            style: TextStyle(
                                              fontSize: 15.sp,
                                              fontWeight: FontWeight.w300,
                                              height: .5.h,
                                            ),
                                          ),
                                          Text(
                                            '${vm.favorites[index].productId.price}',
                                            style: TextStyle(
                                              fontSize: 20.sp,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5.h,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        itemCount: vm.favorites.length,
                        // scrollDirection: Axis.vertical,
                        // shrinkWrap: true,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
      bottomNavigationBar:
          const CustomBottomNavigationBar(value: NavigationBars.favorite),
    );
  }
}
