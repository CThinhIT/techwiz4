import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../constants.dart';

class HelpScreen extends StatefulWidget {
  const HelpScreen({super.key});

  @override
  State<HelpScreen> createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text(
          "Help Center",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            letterSpacing: 1.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(left: 0.w, right: 0.w),
              child: Divider(
                color: topCardBackgroundColor,
                height: 0.h,
                thickness: 6.w,
              ),
            ),
            SizedBox(height: 10.h,),
            const ExpansionTile(
              title: Text('[New Member] How to track the shipping status of my order?'),
              children: <Widget>[
                ListTile(title: Text('Go to Profile > Pickup > See if the Seller has sent the goods to the carrier')),
                ListTile(title: Text('At Profile > Delivering > Order status information will be updated here')),
              ],
            ),
            const ExpansionTile(
              title: Text('[Scam alert] Shop safely with PlantPal'),
              children: <Widget>[
                ListTile(title: Text('1. Download the PlantPal app to your device from authorized apps')),
                ListTile(title: Text('2. Only make transactions on PlantPal official app')),
                ListTile(title: Text('3. Beware of suspicious messages')),
                ListTile(title: Text('3. Never share your Password')),
                ListTile(title: Text('4. Please check your order information carefully before receiving')),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
