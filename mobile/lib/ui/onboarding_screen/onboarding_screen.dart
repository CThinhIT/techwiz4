import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/core/global/router.dart';
import 'package:tech_wiz4/core/model_ui/onboard_model_ui.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({super.key});

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreen();
}

class _OnBoardingScreen extends State<OnBoardingScreen> {
  final PageController _controller = PageController();
  final List<OnboardModelUI> items = OnboardModelUI.items;
  int a = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        child: Stack(
          children: [
            PageView(
              controller: _controller,
              scrollDirection: Axis.horizontal,
              children: items.map((e) => OnBoardingItem(item: e)).toList(),
              onPageChanged: (value) {
                setState(() {
                  a = value;
                });
              },
            ),
            Container(
              alignment: Alignment(0.sp, 0.5.sp),
              child: Padding(
                padding: EdgeInsets.only(left: 29.w, right: 29.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 665.h,
                    ),
                    SmoothPageIndicator(
                      controller: _controller,
                      count: items.length,
                      effect: const ExpandingDotsEffect(
                        dotColor: Color(0xffffffff),
                        dotHeight: 7,
                        dotWidth: 7,
                        activeDotColor: kPrimaryColor,
                        spacing: 16,
                      ),
                    ),
                    SizedBox(
                      height: 51.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        a > 0
                            ? GestureDetector(
                                onTap: () {
                                  _controller.previousPage(
                                    duration: const Duration(milliseconds: 300),
                                    curve: Curves.easeIn,
                                  );
                                  setState(
                                    () {
                                      a--;
                                    },
                                  );
                                },
                                child: const Icon(Icons.arrow_back),
                              )
                            : GestureDetector(
                                onTap: () {},
                                child: const SizedBox(
                                  height: 40,
                                  width: 40,
                                ),
                              ),
                        a != items.length - 1
                            ? GestureDetector(
                                onTap: () {
                                  _controller.nextPage(
                                    duration: const Duration(milliseconds: 300),
                                    curve: Curves.easeIn,
                                  );
                                  setState(
                                    () {
                                      if (a > items.length) {
                                        a = items.length;
                                      } else {
                                        a++;
                                      }
                                    },
                                  );
                                },
                                child: const Icon(Icons.arrow_forward),
                              )
                            : GestureDetector(
                                onTap: () {
                                  Get.toNamed(MyRouter.login);
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  height: 47.h,
                                  width: 100.w,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.r),
                                    color: kPrimaryColor,
                                    border: Border.all(
                                      color: kPrimaryColor,
                                    ),
                                  ),
                                  child: Text(
                                    "EXPLORE",
                                    style: TextStyle(
                                      fontSize: 16.sp,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 2.sp,
                                    ),
                                  ),
                                ),
                              ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class OnBoardingItem extends StatelessWidget {
  final OnboardModelUI item;
  const OnBoardingItem({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(item.image),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ],
    );
  }
}
