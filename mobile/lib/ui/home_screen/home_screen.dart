import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/global/locator.dart';
import 'package:tech_wiz4/core/global/router.dart';
import 'package:tech_wiz4/core/model_ui/carousel_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/category_model_ui.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ispecies_view_model.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_product.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_species.dart';
import 'package:tech_wiz4/ui/common_widgets/custom_bottom_navigation.dart';
import 'package:tech_wiz4/ui/result_search_screen/result_search_screen.dart';

import '../../firebase/interfaces/ifirebase_auth.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<CarouselModelUI> carousels = CarouselModelUI.items;
  final List<CategoryModelUI> categorys = CategoryModelUI.items;
  // final List<ProductModelUI> products = ProductModelUI.items;
  late IBasketViewViewModel vmb;

  final IFirebaseProductService _firebaseProductService =
      locator<IFirebaseProductService>();
  final IFirebaseSpeciesService _firebaseCategoryService =
      locator<IFirebaseSpeciesService>();
  late ISpeciesViewModel vm;
  final TextEditingController _searchController = TextEditingController();
  List<ProductDto> searchResults = [];
  final IFirebaseAuthService _authService = locator<IFirebaseAuthService>();
  @override
  void initState() {
    super.initState();
    vm = context.read<ISpeciesViewModel>();
    vmb = context.read<IBasketViewViewModel>();
    vm.getAllSpecies();
  }

  void addToFavorite(ProductDto id) {
    vmb.addToFavorite(id);
  }

  Future<String> getImageUrl(String imagePath) async {
    FirebaseStorage storage = FirebaseStorage.instance;
    Reference ref = storage.ref().child(imagePath);

    String url = await ref.getDownloadURL();
    return url;
  }

  void _performSearch(String query) {
    vm.searchByname(query);
    setState(() {
      searchResults = vm.listAllSearch;
    });
    Navigator.pushNamed(context, MyRouter.searchresult,
        arguments: searchResults);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        title: Text(
          "PlantPal",
          style: TextStyle(
            color: kPrimaryColor,
            fontSize: 18.sp,
            letterSpacing: 3.sp,
            fontWeight: FontWeight.bold,
          ),
        ),
        leading: Padding(
          padding: EdgeInsets.only(left: 19.w),
          child: Image.asset(
            'assets/images/logo.png',
            width: 50.w,
            height: 50.h,
          ),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      endDrawer: DrawerE(authService: _authService),
      extendBodyBehindAppBar: true,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(right: 19.w, left: 19.w),
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 26.h, bottom: 20.h),
                      child: CarouselSlider(
                        options: CarouselOptions(
                          viewportFraction: 1,
                          aspectRatio: 2.2,
                          enlargeCenterPage: true,
                          enableInfiniteScroll: false,
                          initialPage: 0,
                          autoPlay: true,
                          autoPlayInterval: const Duration(seconds: 5),
                        ),
                        items: carousels
                            .map(
                              (item) => ClipRRect(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(5.r)),
                                child: Image.asset(
                                  item.images,
                                  fit: BoxFit.cover,
                                  width: 1000.w,
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: 320.w,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            spreadRadius: 3,
                            blurRadius: 5,
                            offset: const Offset(
                                0, 1), // changes position of shadow
                          ),
                        ],
                        borderRadius: BorderRadius.circular(30.r),
                      ),
                      child: TextField(
                        controller: _searchController,
                        onSubmitted: (value) {
                          _performSearch(value);
                        },
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 0.2.w,
                              color: tHintColor,
                            ),
                            borderRadius: BorderRadius.circular(30.r),
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              _performSearch(_searchController.text);
                            },
                            icon: Icon(
                              Icons.search,
                              color: Colors.black,
                            ),
                          ),
                          hintText: "Search",
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 0.2.w,
                              color: tHintColor,
                            ),
                            borderRadius: BorderRadius.circular(30.r),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.h),
                      child: SizedBox(
                        height: 80.h,
                        width: double.maxFinite,
                        child: FutureBuilder<List<CategoryModelUI>>(
                          future: _firebaseCategoryService.getCategory(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return const Center(
                                  child: CircularProgressIndicator());
                            } else if (snapshot.hasError) {
                              return Text('Error: ${snapshot.error}');
                            } else if (!snapshot.hasData ||
                                snapshot.data!.isEmpty) {
                              return const Text('No categories available.');
                            } else {
                              List<CategoryModelUI> categories = snapshot.data!;
                              return ListView.builder(
                                itemCount: categories.length,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () async {
                                      vm.getList(categories[index].id);
                                      vm.selectCate(categories[index].name);
                                      Get.toNamed(
                                        MyRouter.species,
                                        arguments: categories[index],
                                      );
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(right: 40.w),
                                      width: 90.w,
                                      height: 70.h,
                                      child: Column(
                                        children: [
                                          FutureBuilder<String>(
                                            future: getImageUrl(
                                                categories[index].icon),
                                            builder: (context, snapshot) {
                                              if (snapshot.connectionState ==
                                                  ConnectionState.waiting) {
                                                return const CircularProgressIndicator();
                                              } else if (snapshot.hasError) {
                                                return Text(
                                                    'Error: ${snapshot.error}');
                                              } else if (!snapshot.hasData) {
                                                return const Text(
                                                    'No image URL available.');
                                              } else {
                                                return Image.network(
                                                  snapshot.data!,
                                                  width: 35.w,
                                                  height: 35.h,
                                                );
                                              }
                                            },
                                          ),
                                          Text(
                                            categories[index].name,
                                            style: const TextStyle(
                                                fontFamily: fontPoppins),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 5.h),
                child: SizedBox(
                  height: 400.h,
                  width: double.maxFinite,
                  child: FutureBuilder<List<ProductDto>>(
                    future: _firebaseProductService.getProductForHome(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
                        return const Text('No products available.');
                      } else {
                        List<ProductDto> products = snapshot.data!;
                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 4,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Get.toNamed(
                                  MyRouter.detail,
                                  arguments: products[index],
                                );
                              },
                              child: Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 10.w,
                                        bottom: 20.h,
                                        top: 20.h,
                                        right: 30.w),
                                    height: 346.h,
                                    width: 198.w,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: topCardBackgroundColor,
                                      border: Border.all(
                                        color: topCardBackgroundColor,
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black.withOpacity(0.2),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: const Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      children: [
                                        Container(
                                          color: topCardBackgroundColor,
                                          margin: EdgeInsets.only(
                                            bottom: 1.h,
                                          ),
                                          child: Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  IconButton(
                                                    onPressed: () {
                                                      setState(() {
                                                        addToFavorite(
                                                            products[index]);
                                                      });
                                                    },
                                                    icon: Icon(
                                                      Icons.favorite,
                                                      color: vmb.favo
                                                          ? Colors.red
                                                          : Colors.grey,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Image.network(
                                                products[index].image,
                                                height: 195.h,
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 5.h),
                                          child: Container(
                                            color: bottomCardBackgroundColor,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                  left: 10.w,
                                                  right: 10.w,
                                                  top: 5.h,
                                                  bottom: 5.h),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    products[index].name,
                                                    style: TextStyle(
                                                      fontSize: 16.sp,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    products[index].cate.name,
                                                    style: TextStyle(
                                                      fontSize: 16.sp,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Text(
                                                    products[index].spec.name,
                                                    style: TextStyle(
                                                      fontSize: 16.sp,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                    ),
                                                  ),
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        products[index]
                                                            .cate
                                                            .name,
                                                      ),
                                                      Text(
                                                        '\$${products[index].price}',
                                                        style: TextStyle(
                                                          fontSize: 16.sp,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar:
          const CustomBottomNavigationBar(value: NavigationBars.home),
    );
  }
}
