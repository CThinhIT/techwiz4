import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/global/locator.dart';
import 'package:tech_wiz4/core/global/router.dart';
import 'package:tech_wiz4/core/model_ui/carousel_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/category_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/species_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/product_model_ui.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ispecies_view_model.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_auth.dart';
import 'package:tech_wiz4/ui/common_widgets/custom_bottom_navigation.dart';

import '../result_search_screen/result_search_screen.dart';

class SpeciesScreen extends StatefulWidget {
  const SpeciesScreen({super.key, required this.item});
  final CategoryModelUI item;

  @override
  State<SpeciesScreen> createState() => _SpeciesScreenState();
}

class _SpeciesScreenState extends State<SpeciesScreen> {
  final List<CarouselModelUI> carousels = CarouselModelUI.items;
  late List<SpeciesModelUI> categorys = [];
  // final List<ProductModelUI> products = ProductModelUI.items;
  late List<ProductModelUI> product = [];
  // List<PlantsDto> _listPlant = [];
  // List<SeedDto> _listSeed = [];
  String selectedCategoryName = "All";
  final TextEditingController _searchController = TextEditingController();
  List<ProductDto> searchResults = [];
  final IFirebaseAuthService _authService = locator<IFirebaseAuthService>();
  bool showPlants = true;

  // String? selectedSpeciesId = null;

  late ISpeciesViewModel vm;
  @override
  void initState() {
    super.initState();
    vm = context.read<ISpeciesViewModel>();
  }

  @override
  void dispose() {
    // fetchSpeciesData();
    super.dispose();
  }

  void _performSearch(String query) {
    vm.searchByname(query);
    setState(() {
      searchResults = vm.listAllSearch;
    });
    Navigator.pushNamed(context, MyRouter.searchresult,
        arguments: searchResults);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        title: Text(
          "PlantPal",
          style: TextStyle(
            color: kPrimaryColor,
            fontSize: 18.sp,
            letterSpacing: 5.sp,
            fontWeight: FontWeight.bold,
          ),
        ),
        leading: Image.asset(
          'assets/images/logo.png',
          width: 50.w,
          height: 50.h,
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      endDrawer: DrawerE(authService: _authService),
      body: Consumer<ISpeciesViewModel>(
        builder: (context, vm, child) {
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 19.w, left: 19.w, top: 40.h),
                  child: Column(
                    children: [
                      TextField(
                        controller: _searchController,
                        onSubmitted: (value) {
                          _performSearch(value);
                        },
                        decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 1.w,
                              color: Colors.black,
                            ),
                            borderRadius: BorderRadius.circular(16.r),
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              _performSearch(_searchController.text);
                            },
                            icon: const Icon(
                              Icons.search,
                              color: Colors.black,
                            ),
                          ),
                          label: const Text(
                            "Search",
                            style: TextStyle(color: Colors.black),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              width: 3.w,
                              color: Colors.black,
                            ),
                            borderRadius: BorderRadius.circular(16.r),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.h),
                        child: SizedBox(
                          height: 100.h,
                          width: double.maxFinite,
                          child: ListView.builder(
                            itemCount: vm.listSpec.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (_, index) {
                              return GestureDetector(
                                onTap: () {
                                  selectedCategoryName =
                                      vm.listSpec[index].name;
                                  vm.getListPlant(vm.listSpec[index].id);
                                },
                                child: Container(
                                  margin: EdgeInsets.only(right: 10.w),
                                  width: 80.w,
                                  height: 73.h,
                                  child: Container(
                                    width: double.infinity,
                                    height: 100.h,
                                    decoration: BoxDecoration(
                                      color: const Color.fromARGB(
                                          255, 123, 175, 139),
                                      borderRadius: BorderRadius.circular(10.0),
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color.fromARGB(
                                                  255, 39, 99, 30)
                                              .withOpacity(0.2),
                                          spreadRadius: 2,
                                          blurRadius: 2,
                                          offset: const Offset(0, 3),
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Image.network(
                                          vm.listSpec[index].icon,
                                          height: 50.h,
                                          fit: BoxFit.contain,
                                        ),
                                        SizedBox(height: 8.h),
                                        Text(
                                          vm.listSpec[index].name,
                                          textAlign: TextAlign.center,
                                          overflow: TextOverflow.ellipsis,
                                          style: const TextStyle(
                                              color: Color(0xFF325223),
                                              fontFamily: fontPoppins),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(2.0),
                        child: Row(
                          children: [
                            Text(
                              "Product - $selectedCategoryName",
                              style: TextStyle(
                                  fontSize: 20.sp,
                                  color: const Color(0xFF3e662c),
                                  fontWeight: FontWeight.bold,
                                  fontFamily: fontPoppins),
                            ),
                            Expanded(
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: IconButton(
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: const Text(
                                            'Sort Options',
                                            style: TextStyle(
                                                color: Color.fromARGB(
                                                    255, 58, 158, 62)),
                                          ),
                                          content: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              TextButton(
                                                onPressed: () {
                                                  vm.sortProductLow();
                                                  Navigator.pop(context);
                                                },
                                                child: Text(
                                                  'Sort by Lowest Price',
                                                  style: TextStyle(
                                                      color:
                                                          const Color.fromARGB(
                                                              255,
                                                              166,
                                                              225,
                                                              168),
                                                      fontSize: 20.sp,
                                                      fontFamily: fontPoppins),
                                                ),
                                              ),
                                              TextButton(
                                                onPressed: () {
                                                  vm.sortProductHigh();
                                                  Navigator.pop(context);
                                                },
                                                child: Text(
                                                  'Sort by Highest Price',
                                                  style: TextStyle(
                                                      color:
                                                          const Color.fromARGB(
                                                              255,
                                                              166,
                                                              225,
                                                              168),
                                                      fontSize: 20.sp,
                                                      fontFamily: fontPoppins),
                                                ),
                                              ),
                                            ],
                                          ),
                                          actions: [
                                            TextButton(
                                              onPressed: () {
                                                Navigator.pop(
                                                    context); // Close the dialog
                                              },
                                              child: const Text(
                                                'Close',
                                                style: TextStyle(
                                                    color: Color.fromARGB(
                                                        255, 28, 73, 29)),
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                  icon: const Icon(Icons.sort),
                                  color: const Color(0xFF3e662c),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                selectedCategoryName.contains("All")
                    ? Padding(
                        padding: EdgeInsets.only(top: 5.h),
                        child: SizedBox(
                          height: 400.h,
                          width: double.maxFinite,
                          child: GridView.builder(
                              gridDelegate:
                                  const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio: 0.45,
                              ),
                              itemCount: vm.listAllProduct.length,
                              itemBuilder: (_, index) {
                                return InkWell(
                                  onTap: () {
                                    Get.toNamed(
                                      MyRouter.detail,
                                      arguments: vm.listAllProduct[index],
                                    );
                                  },
                                  child: Column(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: 10.w,
                                            bottom: 20.h,
                                            top: 20.h,
                                            right: 10.w),
                                        height: 346.h,
                                        width: 198.w,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: topCardBackgroundColor,
                                          border: Border.all(
                                            color: topCardBackgroundColor,
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.black.withOpacity(0.2),
                                              spreadRadius: 5,
                                              blurRadius: 7,
                                              offset: const Offset(0,
                                                  3), // changes position of shadow
                                            ),
                                          ],
                                        ),
                                        child: Column(
                                          children: [
                                            Container(
                                              color: topCardBackgroundColor,
                                              margin: EdgeInsets.only(
                                                bottom: 1.h,
                                              ),
                                              child: Column(
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [
                                                      IconButton(
                                                        onPressed: () {
                                                          setState(() {
                                                            // addToFavorite(
                                                            //     vm.listAllProduct[
                                                            //         index]);
                                                          });
                                                        },
                                                        icon: const Icon(
                                                            Icons.favorite,
                                                            color: Colors.red),
                                                      ),
                                                    ],
                                                  ),
                                                  Image.network(
                                                    vm.listAllProduct[index]
                                                        .image,
                                                    height: 195.h,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(top: 5.h),
                                              child: Container(
                                                color:
                                                    bottomCardBackgroundColor,
                                                child: Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 10.w,
                                                      right: 10.w,
                                                      top: 5.h,
                                                      bottom: 5.h),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Text(
                                                        vm.listAllProduct[index]
                                                            .name,
                                                        style: TextStyle(
                                                          fontSize: 16.sp,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                      Text(
                                                        vm.listAllProduct[index]
                                                            .cate.name,
                                                        style: TextStyle(
                                                          fontSize: 16.sp,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                      Text(
                                                        vm.listAllProduct[index]
                                                            .spec.name,
                                                        style: TextStyle(
                                                          fontSize: 16.sp,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                        ),
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text(
                                                            vm
                                                                .listAllPlant[
                                                                    index]
                                                                .cate
                                                                .name,
                                                          ),
                                                          Text(
                                                            '\$${vm.listAllProduct[index].price}',
                                                            style: TextStyle(
                                                              fontSize: 16.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                            ),
                                                          ),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        ),
                      )
                    : Padding(
                        padding: EdgeInsets.only(top: 5.h),
                        child: vm.listPlant.isEmpty
                            ? Center(
                                child: Text(
                                  "There are currently no suitable products",
                                  style: TextStyle(
                                    fontSize: 18.sp,
                                  ),
                                ),
                              )
                            : SizedBox(
                                height: 400.h,
                                width: double.maxFinite,
                                child: GridView.builder(
                                  gridDelegate:
                                      const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: 0.45,
                                  ),
                                  itemCount: vm.listPlant.length,
                                  itemBuilder: (_, index) {
                                    return InkWell(
                                      onTap: () {
                                        Get.toNamed(
                                          MyRouter.detail,
                                          arguments: vm.listPlant[index],
                                        );
                                      },
                                      child: Column(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(
                                                left: 10.w,
                                                bottom: 20.h,
                                                top: 20.h,
                                                right: 10.w),
                                            height: 346.h,
                                            width: 198.w,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              color: topCardBackgroundColor,
                                              border: Border.all(
                                                color: topCardBackgroundColor,
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.black
                                                      .withOpacity(0.2),
                                                  spreadRadius: 5,
                                                  blurRadius: 7,
                                                  offset: const Offset(0,
                                                      3), // changes position of shadow
                                                ),
                                              ],
                                            ),
                                            child: Column(
                                              children: [
                                                Container(
                                                  color: topCardBackgroundColor,
                                                  margin: EdgeInsets.only(
                                                    bottom: 1.h,
                                                  ),
                                                  child: Column(
                                                    children: [
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        children: [
                                                          IconButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                // addToFavorite(
                                                                //     vm.listAllProduct[
                                                                //         index]);
                                                              });
                                                            },
                                                            icon: const Icon(
                                                                Icons.favorite,
                                                                color:
                                                                    Colors.red),
                                                          ),
                                                        ],
                                                      ),
                                                      Image.network(
                                                        vm.listPlant[index]
                                                            .image,
                                                        height: 195.h,
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                      EdgeInsets.only(top: 5.h),
                                                  child: Container(
                                                    color:
                                                        bottomCardBackgroundColor,
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 10.w,
                                                          right: 10.w,
                                                          top: 5.h,
                                                          bottom: 5.h),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            vm.listPlant[index]
                                                                .name,
                                                            style: TextStyle(
                                                              fontSize: 16.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                            ),
                                                          ),
                                                          Text(
                                                            vm.listPlant[index]
                                                                .cate.name,
                                                            style: TextStyle(
                                                              fontSize: 16.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                            ),
                                                          ),
                                                          Text(
                                                            vm.listPlant[index]
                                                                .spec.name,
                                                            style: TextStyle(
                                                              fontSize: 16.sp,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                            ),
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Text(
                                                                vm
                                                                    .listPlant[
                                                                        index]
                                                                    .cate
                                                                    .name,
                                                              ),
                                                              Text(
                                                                '\$${vm.listPlant[index].price}',
                                                                style:
                                                                    TextStyle(
                                                                  fontSize:
                                                                      16.sp,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                ),
                                                              ),
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                      ),
              ],
            ),
          );
        },
      ),
      bottomNavigationBar:
          const CustomBottomNavigationBar(value: NavigationBars.home),
    );
  }
}
