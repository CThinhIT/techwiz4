import 'package:flutter/material.dart';
import 'package:flutter_paypal/flutter_paypal.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/core/global/router.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';
import 'package:tech_wiz4/ui/common_widgets/custom_bottom_navigation.dart';

import '../../core/global/global_data.dart';
import '../../core/global/locator.dart';

class CheckoutScreen extends StatefulWidget {
  const CheckoutScreen({super.key});

  @override
  State<CheckoutScreen> createState() => ContactScreenState();
}

class ContactScreenState extends State<CheckoutScreen> {
  bool paymentMethodCash = false;
  bool paymentMethodPaypal = false;
  late bool payment;
  late IBasketViewViewModel vm;
  @override
  void initState() {
    vm = context.read<IBasketViewViewModel>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: bottomCardBackgroundColor,
        centerTitle: true,
        title: Text(
          "Check Out",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            letterSpacing: 1.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 30.h, left: 10.w),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.contacts,
                      size: 24.sp,
                      color: Color(0xFF325223),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        "Contact",
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontFamily: fontPoppins,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.w),
                    child: SizedBox(
                      width: 300.w,
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: locator<GlobalData>().currentUser!.name,
                          focusedBorder: const UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green),
                          ),
                          hintStyle: const TextStyle(
                            fontWeight: FontWeight.w300,
                            color: Color.fromARGB(255, 0, 0, 0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10.w),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(right: 10.w),
                    child: SizedBox(
                      width: 300.w,
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: locator<GlobalData>().currentUser!.phone,
                          focusedBorder: const UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green),
                          ),
                          hintStyle: const TextStyle(
                            fontWeight: FontWeight.w300,
                            color: Color.fromARGB(255, 0, 0, 0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 30.h, left: 10.w),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.location_on_outlined,
                      size: 24.sp,
                      color: Color(0xFF325223),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        "Location",
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontFamily: fontPoppins,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 10.w, left: 10.w),
              child: SizedBox(
                width: 500.w,
                child: const TextField(
                  decoration: InputDecoration(
                    hintText: "Address",
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.green),
                    ),
                    hintStyle: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w300,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 30.h, left: 10.w),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.payment,
                      size: 24.sp,
                      color: Color(0xFF325223),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        "Payment method",
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontFamily: fontPoppins,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20.h,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10.sp),
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                border: Border.all(
                    width: 2.sp, color: Color.fromARGB(255, 143, 193, 114)),
                borderRadius: BorderRadius.circular(10.sp),
              ),
              child: Row(
                children: [
                  Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromARGB(255, 159, 224, 161),
                    ),
                    child: Center(
                      child: Image(
                          image: AssetImage("assets/images/icon_cash.png")),
                    ),
                  ),
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Payment on delivery",
                              style: TextStyle(
                                fontSize: 16.sp,
                                fontFamily: fontPoppins,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 8.0),
                            Text(
                              "Household collection fee: 0",
                              style: TextStyle(
                                fontSize: 13.sp,
                                fontFamily: fontPoppins,
                              ),
                            ),
                          ],
                        ),
                        Radio(
                          fillColor: MaterialStateColor.resolveWith(
                              (states) => Colors.green),
                          value: true,
                          groupValue: paymentMethodCash,
                          onChanged: (value) {
                            setState(() {
                              paymentMethodCash = true;
                              paymentMethodPaypal = false;
                              payment = false;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.h),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10.sp),
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                border: Border.all(
                    width: 2.sp, color: Color.fromARGB(255, 143, 193, 114)),
                borderRadius: BorderRadius.circular(10.sp),
              ),
              child: Row(
                children: [
                  Container(
                    width: 60,
                    height: 60,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromARGB(255, 159, 224, 161),
                    ),
                    child: Center(
                      child: Image(
                          image: AssetImage("assets/images/icon_paypal.png")),
                    ),
                  ),
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Paypal",
                              style: TextStyle(
                                fontSize: 16.sp,
                                fontFamily: fontPoppins,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 8.0),
                            Text(
                              "Payment online",
                              style: TextStyle(
                                fontSize: 13.sp,
                                fontFamily: fontPoppins,
                              ),
                            ),
                          ],
                        ),
                        Radio(
                          fillColor: MaterialStateColor.resolveWith(
                              (states) => Colors.green),
                          value: true,
                          groupValue: paymentMethodPaypal,
                          onChanged: (value) {
                            setState(() {
                              paymentMethodPaypal = true;
                              paymentMethodCash = false;
                              payment = true;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 50.h,
            ),
            Column(
              children: [
                Positioned(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: EdgeInsets.only(bottom: 10.h, top: 10.h),
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: topCardBackgroundColor,
                        border: Border.all(
                          color: bottomCardBackgroundColor,
                          width: 2.0,
                        ),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30.r),
                            topRight: Radius.circular(30.r)),
                        boxShadow: const [
                          BoxShadow(
                            color: tHintColor,
                            spreadRadius: 3,
                            blurRadius: 7,
                            offset: Offset(0, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      child: Wrap(
                        spacing: double.infinity,
                        direction: Axis.horizontal,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                              right: 50.w,
                              left: 50.w,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(
                              right: 50.w,
                              left: 50.w,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Total',
                                  style: TextStyle(
                                    fontSize: 30.sp,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  '\$${vm.totalPrice}',
                                  style: TextStyle(
                                    fontSize: 30.sp,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          InkWell(
                            onTap: () => {
                              vm.checkOut(payment, payment ? 2 : 1).then(
                                (value) {
                                  if (payment) {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            UsePaypal(
                                                sandboxMode: true,
                                                clientId: clientId,
                                                secretKey: secretKey,
                                                returnURL: returnURL,
                                                cancelURL: cancelURL,
                                                transactions: const [
                                                  {
                                                    "amount": {
                                                      "total": '10.12',
                                                      "currency": "USD",
                                                      "details": {
                                                        "subtotal": '10.12',
                                                        "shipping": '0',
                                                        "shipping_discount": 0
                                                      }
                                                    },
                                                    "description":
                                                        "The payment transaction description.",
                                                    "item_list": {
                                                      "items": [
                                                        {
                                                          "name":
                                                              "A demo product",
                                                          "quantity": 1,
                                                          "price": '10.12',
                                                          "currency": "USD"
                                                        }
                                                      ],
                                                      "shipping_address": {
                                                        "recipient_name":
                                                            "Jane Foster",
                                                        "line1":
                                                            "Travis County",
                                                        "line2": "",
                                                        "city": "Austin",
                                                        "country_code": "US",
                                                        "postal_code": "73301",
                                                        "phone": "+00000000",
                                                        "state": "Texas"
                                                      },
                                                    }
                                                  }
                                                ],
                                                note:
                                                    "Contact us for any questions on your order.",
                                                onSuccess: (Map params) async {
                                                  dialogS(context);
                                                },
                                                onError: (error) {
                                                  dialogS(context);
                                                },
                                                onCancel: (params) {
                                                  dialogS(context);
                                                }),
                                      ),
                                    );
                                  } else {
                                    dialogS(context);
                                  }
                                },
                              ),
                            },
                            child: Container(
                              width: double.infinity,
                              margin: EdgeInsets.only(
                                  right: 50.w, left: 50.w, top: 5.h),
                              padding: EdgeInsets.only(top: 10.h, bottom: 10.h),
                              decoration: BoxDecoration(
                                color: Colors.black87,
                                borderRadius: BorderRadius.circular(16.r),
                              ),
                              child: Center(
                                child: Text(
                                  'Checkout',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 17.sp,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar:
          const CustomBottomNavigationBar(value: NavigationBars.shopping),
    );
  }

  Future<dynamic> dialogS(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          elevation: 0,
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
              side: BorderSide(color: const Color(0XFF344A43), width: 5.sp)),
          child: SizedBox(
            height: 350.h,
            width: 400.w,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 0.w, top: 20.h),
                  child: Image.asset(
                    'assets/images/thanks_order.png',
                    width: 180.w,
                    height: 180.h,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 0.w, top: 20.h),
                  child: Text(
                    "Order Success",
                    style: TextStyle(
                      color: const Color(0XFF344A43),
                      fontSize: 20.sp,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2.sp,
                      fontFamily: fontPoppins,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.h),
                  child: ElevatedButton(
                    onPressed: () {
                      Get.toNamed(MyRouter.home);
                    },
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(
                          horizontal:
                              30.0), // Điều chỉnh padding ngang để làm cho button dài hơn
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: const BorderSide(color: Colors.black, width: 2.0),
                      ),
                      backgroundColor: Colors.white,
                    ),
                    child: Container(
                      width: 150.0, // Điều chỉnh chiều dài của button
                      child: Center(
                        child: Text(
                          'Close',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.sp,
                            fontWeight: FontWeight.bold,
                            fontFamily: fontPoppins,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
