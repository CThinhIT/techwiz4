import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tech_wiz4/ui/manage_order_screen/confirm_order_screen.dart';
import 'package:tech_wiz4/ui/manage_order_screen/feedback_order_screen.dart';
import '../../constants.dart';
import '../../core/global/router.dart';
import '../manage_order_screen/delivery_order_screen.dart';
import '../manage_order_screen/pickup_order_screen.dart';

class TabScreen extends StatefulWidget {
  final int cureentIndex;
  const TabScreen({super.key, required this.cureentIndex});

  @override
  State<TabScreen> createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen>
    with SingleTickerProviderStateMixin {
  final List<Widget> myTabs = const <Widget>[
    ConfirmOrderScreen(),
    PickupOrderScreen(),
    DeliveryOrderScreen(),
  ];
  late TabController controller;
  @override
  void initState() {
    super.initState();
    controller = TabController(length: myTabs.length, vsync: this);
    controller.addListener(() {
      setState(() {});
    });
    controller.animateTo(widget.cureentIndex);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: bottomCardBackgroundColor,
        title: InkWell(
          onTap: () {
            Get.toNamed(
              MyRouter.account,
            );
          },
          child: const Icon(
            Icons.arrow_back,
          ),
        ),
        elevation: 0,
        bottom: TabBar(
          controller: controller,
          indicatorColor: hBackgroundColor,
          labelColor: Colors.black,
          tabs: const [
            Tab(
              text: "Confirm",
            ),
            Tab(
              text: "Pickup",
            ),
            Tab(
              text: "Delivery",
            ),
          ],
          isScrollable: true,
        ),
      ),
      body: TabBarView(
        controller: controller,
        children: myTabs.map((Widget tab) {
          return tab;
        }).toList(),
      ),
    );
  }
}
