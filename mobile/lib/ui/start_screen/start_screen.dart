import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ispecies_view_model.dart';

import '../../core/view_model/interfaces/istart_screen_view_model.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  late IStartScreenViewModel _viewModel;
  late ISpeciesViewModel _vm;
  @override
  void initState() {
    _viewModel = context.read<IStartScreenViewModel>();
    _vm = context.read<ISpeciesViewModel>();
    _vm.getAllPlant();
    _vm.getAllAcces();
    _vm.getAllSeed();
    super.initState();
    initialization();
  }

  void initialization() async {
    await Future.delayed(const Duration(seconds: 2));
    _viewModel.goToNextPage();
  }
  // @override
  // void initState() {
  //   Future.delayed(const Duration(milliseconds: 2000), () {
  //     Get.toNamed(MyRouter.login);
  //   });
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffecfae6),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 50.h,
              ),
              Image.asset(
                'assets/images/logo.png',
                width: 200.w,
                height: 200.h,
              ),
              Padding(
                padding: EdgeInsets.only(top: 50.h, left: 77.w, right: 77.w),
                child: Column(
                  children: [
                    Text(
                      "PlantPal",
                      style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: 36.sp,
                        letterSpacing: 5.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
