import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tech_wiz4/constants.dart';

import 'package:tech_wiz4/core/model_ui/carousel_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/species_model_ui.dart';
class ConfirmScreen extends StatefulWidget {
  const ConfirmScreen({super.key});

  @override
  State<ConfirmScreen> createState() => ConfirmsScreenState();
}

class ConfirmsScreenState extends State<ConfirmScreen> {
  final List<CarouselModelUI> carousels = CarouselModelUI.items;
  final List<SpeciesModelUI> categorys = SpeciesModelUI.items;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 10.h, left: 10.w),
            child: Center(
              child: Column(
                mainAxisAlignment:
                    MainAxisAlignment.spaceEvenly, // Thay đổi chỉ mục này
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 100.h),
                    child: Image(
                        width: 80.w,
                        height: 80.h,
                        image: AssetImage("assets/images/icon_success.png")),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30.h),
                    child: Text(
                      "Thank you !",
                      style: TextStyle(
                        color: Color(0xFF004643),
                        fontSize: 35.sp,
                        fontFamily: fontPoppins,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30.h),
                    child: Text(
                      "Your Order  #2564866 is Placed Successfully !\n You can Track delivery from here !",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color.fromARGB(255, 161, 162, 161),
                        fontSize: 15.sp,
                        fontFamily: fontPoppins,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.h),
                    child: Image(
                      image: AssetImage("assets/images/thanks_order.png"),
                      width: 300.w,
                      height: 300.h,
                    ),
                  ),
                  InkWell(
                    onTap: () => {},
                    child: Container(
                      width: double.infinity,
                      margin:
                          EdgeInsets.only(right: 50.w, left: 50.w, top: 30.h),
                      padding: EdgeInsets.only(top: 10.h, bottom: 10.h),
                      decoration: BoxDecoration(
                        color: const Color(0xFF004643),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      child: Center(
                        child: Text('Continue Shopping',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.sp,
                              fontWeight: FontWeight.w500,
                            )),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
