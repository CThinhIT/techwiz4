import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';

class CustomBottomCart extends StatefulWidget {
  final ProductDto item;
  const CustomBottomCart({Key? key, required this.item}) : super(key: key);

  @override
  State<CustomBottomCart> createState() => _CustomBottomCartState();
}

class _CustomBottomCartState extends State<CustomBottomCart> {
  late IBasketViewViewModel vm;

  @override
  void initState() {
    vm = context.read<IBasketViewViewModel>();
    super.initState();
  }

  void addToCart(ProductDto id) {
    vm.addToCart(id);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        addToCart(widget.item);
        // print(vm.totalBasket());
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 15.h, left: 15.w, right: 15.w),
        decoration: BoxDecoration(
          color: backgroundCartColor,
          borderRadius: BorderRadius.circular(50.r),
          boxShadow: [
            BoxShadow(
              color: const Color.fromARGB(255, 255, 255, 255).withOpacity(0.8),
              spreadRadius: 5,
              blurRadius: 7,
              offset: const Offset(0, -20), // changes position of shadow
            ),
          ],
        ),
        height: 70.h,
        padding: EdgeInsets.only(
          bottom: 12.h,
          top: 12.h,
          right: 32.w,
          left: 32.w,
        ),
        child: Row(
          children: [
            Container(
              padding: EdgeInsets.only(right: 40.w),
              decoration: const BoxDecoration(
                border: Border(
                  right: BorderSide(color: Color.fromARGB(255, 218, 214, 214)),
                ),
              ),
              child: Column(
                children: [
                  Text(
                    "\$35",
                    style: TextStyle(
                      fontSize: 16.sp,
                      color: Colors.white,
                      fontFamily: fontPoppins,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                    "Price",
                    style: TextStyle(
                      fontSize: 12.sp,
                      color: Colors.white,
                      fontFamily: fontPoppins,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 80.w),
              child: Text(
                'Add to cart',
                style: TextStyle(
                  fontSize: 16.sp,
                  color: Colors.white,
                  fontFamily: fontPoppins,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
