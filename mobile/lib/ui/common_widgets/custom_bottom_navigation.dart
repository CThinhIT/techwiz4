import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/core/global/router.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';
import 'package:tech_wiz4/ui/common_widgets/ripple_click_effect.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  final NavigationBars value;
  const CustomBottomNavigationBar({Key? key, required this.value})
      : super(key: key);

  @override
  State<CustomBottomNavigationBar> createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.transparent,
          border: Border(
            top: BorderSide(color: Color.fromARGB(255, 218, 214, 214)),
          )),
      height: 60.h,
      padding: EdgeInsets.only(
        bottom: 2.h,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: BottomBarItem(
              onTap: () {
                Get.offNamed(MyRouter.home);
              },
              icon: Icons.home,
              isSelected: widget.value == NavigationBars.home,
              label: "Home",
              cart: false,
            ),
          ),
          Expanded(
            child: BottomBarItem(
              onTap: () {
                Get.offNamed(MyRouter.favorite);
              },
              icon: Icons.favorite,
              isSelected: widget.value == NavigationBars.favorite,
              label: "Favorite",
              cart: false,
            ),
          ),
          Expanded(
            child: BottomBarItem(
              onTap: () {
                Get.offNamed(MyRouter.cart);
              },
              icon: Icons.shopping_bag,
              isSelected: widget.value == NavigationBars.shopping,
              label: "Cart",
              cart: true,
            ),
          ),
          Expanded(
            child: BottomBarItem(
              onTap: () {
                Get.offNamed(MyRouter.account);
              },
              icon: Icons.account_circle,
              isSelected: widget.value == NavigationBars.account,
              label: "Profile",
              cart: false,
            ),
          ),
        ],
      ),
    );
  }
}

class BottomBarItem extends StatefulWidget {
  final VoidCallback onTap;
  final IconData icon;
  final bool isSelected;
  final String label;
  final bool cart;
  const BottomBarItem({
    Key? key,
    required this.onTap,
    required this.icon,
    required this.isSelected,
    required this.label,
    required this.cart,
  }) : super(key: key);

  @override
  State<BottomBarItem> createState() => _BottomBarItemState();
}

class _BottomBarItemState extends State<BottomBarItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<IBasketViewViewModel>(
      builder: (context, vm, child) {
        return RippleClickEffect(
          onPressed: widget.onTap,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(25.w, 10.h, 20.w, 2.h),
                child: Stack(
                  children: [
                    Column(
                      children: [
                        Icon(
                          widget.icon,
                          color: widget.isSelected
                              ? Color.fromARGB(255, 0, 0, 0)
                              : Color.fromARGB(255, 145, 129, 129),
                          size: 25.sp,
                        ),
                        Text(widget.label)
                      ],
                    ),
                    (widget.cart && vm.totalItems != 0)
                        ? Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(left: 15.w),
                            height: 20.h,
                            width: 20.h,
                            decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(100.r),
                            ),
                            child: Text(
                              '${vm.totalItems}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12.sp,
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

enum NavigationBars {
  @JsonValue(0)
  home,
  @JsonValue(1)
  favorite,
  @JsonValue(2)
  shopping,
  @JsonValue(3)
  account,
}
