import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:provider/provider.dart';
import 'package:tech_wiz4/core/global/router.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';

class CustomAppBar extends StatefulWidget {
  const CustomAppBar({
    super.key,
  });

  @override
  State<CustomAppBar> createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return Consumer<IBasketViewViewModel>(
      builder: (context, vm, child) {
        return Stack(
          children: [
            IconButton(
              onPressed: () {
                Get.toNamed(MyRouter.cart);
              },
              icon: const Icon(
                Icons.shopping_bag_outlined,
                color: Colors.black,
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(left: 25.w, top: 10.h),
              height: 20.h,
              width: 20.h,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(100.r),
              ),
              child: Text(
                "${vm.totalItems}",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12.sp,
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
