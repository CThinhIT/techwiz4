import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:tech_wiz4/constants.dart';
import 'package:tech_wiz4/ui/common_widgets/custom_bottom_cart.dart';

import '../../core/dto/model/product_dto.dart';
import '../common_widgets/custom_app_bar.dart';

class DetailProductScreen extends StatefulWidget {
  final ProductDto item;

  const DetailProductScreen({super.key, required this.item});

  @override
  State<DetailProductScreen> createState() => _DetailProductScreenState();
}

class _DetailProductScreenState extends State<DetailProductScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        elevation: 0,
        backgroundColor: Colors.transparent,
        actions: const [
          CustomAppBar(),
        ],
      ),
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  color: topCardBackgroundColor,
                  width: double.maxFinite,
                  height: 307.h,
                ),
                Container(
                  margin: EdgeInsets.only(top: 30.h),
                  alignment: Alignment.center,
                  child: Image(
                    image: NetworkImage(
                      widget.item.image,
                    ),
                    fit: BoxFit.fill,
                    height: 363.h,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 85.h, left: 10.w, right: 10.w),
                  width: 331.w,
                  height: 182.h,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CardItems(
                            l: 15.w,
                            height: 31.h,
                            witdh: 111.w,
                            icon: const Icon(
                              Icons.wb_sunny_outlined,
                              color: Colors.yellow,
                            ),
                            label: "Medium Light",
                          ),
                          CardItems(
                            l: 0.h,
                            height: 31.h,
                            witdh: 104.w,
                            icon: const Icon(
                              Icons.tag_faces,
                              color: Colors.purple,
                            ),
                            label: "Easy to care",
                          ),
                          CardItems(
                            l: 15.w,
                            height: 31.h,
                            witdh: 87.w,
                            icon: const Icon(
                              Icons.thermostat,
                              color: Colors.red,
                            ),
                            label: "10-24C",
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          CardItems(
                            l: 0.h,
                            height: 31.h,
                            witdh: 90.w,
                            icon: const Icon(
                              Icons.height,
                              color: Colors.green,
                            ),
                            label: "10-20 cm",
                          ),
                          CardItems(
                            l: 0.h,
                            height: 31.h,
                            witdh: 116.w,
                            icon: const Icon(
                              Icons.water_drop,
                              color: Colors.blue,
                            ),
                            label: "Light watering",
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(left: 20.w, right: 20.w, top: 30.h),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.item.name,
                            style: TextStyle(
                              fontSize: 24.sp,
                              fontFamily: fontPoppins,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Row(
                            children: [
                              const Icon(
                                Icons.favorite,
                                color: Colors.pink,
                              ),
                              Text(
                                "   Broadleaf evergreen",
                                style: TextStyle(
                                  fontSize: 14.sp,
                                  fontFamily: fontPoppins,
                                  color: tHintColor,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.all(10.r),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(16.r),
                          border: Border.all(color: Colors.black),
                        ),
                        child: Text(
                          "Indoor",
                          style: TextStyle(
                            fontSize: 12.sp,
                            fontFamily: fontPoppins,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 15.h, bottom: 15.h),
                    decoration: const BoxDecoration(
                      border: Border(
                        top: BorderSide(
                          color: tHintColor,
                        ),
                        bottom: BorderSide(
                          color: tHintColor,
                        ),
                      ),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              top: 10.h, bottom: 10.h, right: 15.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Biontanical Name",
                                style: TextStyle(
                                  fontSize: 12.sp,
                                  fontFamily: fontPoppins,
                                  fontWeight: FontWeight.w400,
                                  color: tHintColor,
                                ),
                              ),
                              Text(
                                "Ficus lyrata",
                                style: TextStyle(
                                  fontSize: 16.sp,
                                  fontFamily: fontPoppins,
                                  fontWeight: FontWeight.w600,
                                  color: hHintColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const VerticalDivider(
                          width: 20,
                          thickness: 1,
                          indent: 20,
                          endIndent: 0,
                          color: Colors.grey,
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 10.h, bottom: 10.h, left: 15.w),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Nick Name",
                                style: TextStyle(
                                  fontSize: 12.sp,
                                  fontFamily: fontPoppins,
                                  fontWeight: FontWeight.w400,
                                  color: tHintColor,
                                ),
                              ),
                              Text(
                                "Fiddle-leaf fig, banjo fig",
                                style: TextStyle(
                                  fontSize: 16.sp,
                                  fontFamily: fontPoppins,
                                  fontWeight: FontWeight.w600,
                                  color: hHintColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    "Description",
                    style: TextStyle(
                      fontSize: 24.sp,
                      fontFamily: fontPoppins,
                      fontWeight: FontWeight.w600,
                      color: hHintColor,
                      letterSpacing: 0.24.sp,
                    ),
                  ),
                  Text(
                    "Native to the Yunnan and Sichuan provinces of southern China, the Chinese money plant was first brought to the UK in 1906 by Scottish botanist George Forrest (yes, we know the exact man who found it). It became a popular houseplant later in the 20th century because it is simple to grow and really easy to propagate, meaning friends could pass cuttings around amongst themselves. That earned it the nickname 'pass it on plant'.",
                    style: TextStyle(
                      fontSize: 12.sp,
                      fontFamily: fontPoppins,
                      fontWeight: FontWeight.w400,
                      letterSpacing: 0.12.sp,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20.h,
            )
          ],
        ),
      ),
      bottomNavigationBar: CustomBottomCart(item: widget.item),
    );
  }
}

class CardItems extends StatelessWidget {
  final String label;
  final Icon icon;
  final double height, witdh, l;

  const CardItems({
    super.key,
    required this.label,
    required this.icon,
    required this.height,
    required this.witdh,
    required this.l,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: l),
      alignment: Alignment.center,
      height: height,
      width: witdh,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.r),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          icon,
          Text(
            label,
            style: TextStyle(
              fontFamily: fontPoppins,
              fontSize: 12.sp,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }
}
