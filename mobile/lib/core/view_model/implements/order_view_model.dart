import 'package:flutter/material.dart';
import 'package:tech_wiz4/core/dto/model/order_dto.dart';
import 'package:tech_wiz4/core/view_model/interfaces/iorder_view_model.dart';
import '../../../firebase/interfaces/ifirebase_order.dart';
import '../../dto/model/order_detail_dto.dart';
import '../../global/locator.dart';

class OrderViewModel extends ChangeNotifier implements IOrderViewModel {
  final IFirebaseOrderService _orderService = locator<IFirebaseOrderService>();
  List<OrderDetailDto> _historyOrders = [];
  List<OrderDetailDto> _confirmOrders = [];
  List<OrderDetailDto> _pickupOrders = [];
  List<OrderDetailDto> _deliveryOrders = [];

  @override
  Future<List<OrderDto>> getOrder() async {
    return await _orderService.getOrder();
  }

  @override
  Future<void> getHistoryOrder() async {
    _historyOrders.clear();
    var list = await _orderService.getOrder();
    for (var i in list) {
      if (i.status == 4) {
        var detail = await _orderService.getOrderDetail(i.id);
        _historyOrders.addAll(detail);
      }
    }
    notifyListeners();
  }

  @override
  Future<void> getConfirm() async {
    _confirmOrders.clear();
    var list = await _orderService.getOrder();
    for (var i in list) {
      if (i.status == 1) {
        var detail = await _orderService.getOrderDetail(i.id);
        _confirmOrders.addAll(detail);
      }
    }
    notifyListeners();
  }

  @override
  Future<void> getDelivery() async {
    _deliveryOrders.clear();
    var list = await _orderService.getOrder();
    for (var i in list) {
      if (i.status == 2) {
        var detail = await _orderService.getOrderDetail(i.id);
        _deliveryOrders.addAll(detail);
      }
    }
    notifyListeners();
  }

  @override
  Future<void> getPickup() async {
    _pickupOrders.clear();
    var list = await _orderService.getOrder();
    for (var i in list) {
      if (i.status == 3) {
        var detail = await _orderService.getOrderDetail(i.id);
        _pickupOrders.addAll(detail);
      }
    }
    notifyListeners();
  }

  @override
  // TODO: implement cartItems
  List<OrderDetailDto> get historyOrders => _historyOrders;

  @override
  // TODO: implement confirmOrders
  List<OrderDetailDto> get confirmOrders => _confirmOrders;

  @override
  // TODO: implement deliveryOrders
  List<OrderDetailDto> get deliveryOrders => _deliveryOrders;

  @override
  // TODO: implement pickupOrders
  List<OrderDetailDto> get pickupOrders => _pickupOrders;
}
