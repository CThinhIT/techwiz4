import 'package:flutter/foundation.dart';
import 'package:get/get_utils/get_utils.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/dto/model/seed_dto.dart';
import 'package:tech_wiz4/core/global/locator.dart';
import 'package:tech_wiz4/core/model_ui/species_model_ui.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ispecies_view_model.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_species.dart';

class SpeciesViewViewModel extends ChangeNotifier implements ISpeciesViewModel {
  final List<SpeciesModelUI> _listSpec = [];
  final List<ProductDto> _listPlant = [];
  final List<SeedDto> _listSeed = [];

  final IFirebaseSpeciesService _speciesService =
      locator<IFirebaseSpeciesService>();

  @override
  Future<void> getListSeed(String id) async {
    _listPlant.clear();
    List<SeedDto> species = await _speciesService.getSeedBySpeciesId(id);

    var get = species.where((element) => element.spec.id == id);

    for (var data in get) {
      _listSeed.add(SeedDto(
        id: data.id,
        about: data.about,
        categoryId: data.categoryId,
        speciesId: data.speciesId,
        image: data.image,
        incubate: data.incubate,
        name: data.name,
        price: data.price,
        stock: data.stock,
        cate: data.cate,
        spec: data.spec,
        description: data.description,
      ));
    }
    notifyListeners();
  }

  @override
  List<SeedDto> get listSeed => _listSeed;

  final List<ProductDto> _listPlants = [];
  List<ProductDto> _listPlantBySpec = [];
  final List<ProductDto> _listPlantByName = [];
  final List<ProductDto> _listSeeds = [];
  final List<ProductDto> _listAccess = [];
  List<ProductDto> _listProduct = [];
  @override
  Future<void> getAllPlant() async {
    _listPlants.clear();
    var plants = await _speciesService.getAllPlant();
    for (var e in plants) {
      _listPlants.add(e);
    }
    notifyListeners();
  }

  @override
  List<ProductDto> get listAllPlant => _listPlants;

  @override
  Future<void> getListPlant(String id) async {
    _listPlantBySpec.clear();
    if (id == 'all') {
      _listPlantBySpec = _listPlant;
    } else {
      var plants = _listPlants.where((element) => element.speciesId.id == id);
      for (var e in plants) {
        _listPlantBySpec.add(e);
      }
    }
    notifyListeners();
  }

  @override
  Future<void> sortProductLow() async {
    _listPlantBySpec.sort((a, b) => a.price.compareTo(b.price));
    _listPlants.sort((a, b) => a.price.compareTo(b.price));
    notifyListeners();
  }

  @override
  Future<void> sortProductHigh() async {
    _listPlantBySpec.sort((a, b) => b.price.compareTo(a.price));
    _listPlant.sort((a, b) => b.price.compareTo(a.price));
    notifyListeners();
  }

  @override
  List<ProductDto> get listPlant => _listPlantBySpec;

  List<SpeciesModelUI> _listAllSpecs = [];
  @override
  Future<void> getAllSpecies() async {
    var allspecie = await _speciesService.getAllSpecies();
    _listAllSpecs.clear();
    for (var e in allspecie) {
      _listAllSpecs.add(e);
    }
    notifyListeners();
  }

  @override
  Future<void> getList(String id) async {
    _listSpec.clear();
    var get = _listAllSpecs.where((element) => element.categoryId == id);

    for (var e in get) {
      _listSpec.add(e);
    }
    notifyListeners();
  }

  @override
  List<SpeciesModelUI> get listSpec => _listSpec;

  @override
  Future<void> searchByname(String name) async {
    _listPlantByName.clear();
    List<ProductDto> species = _listPlants;

    // print(species);
    var getPlant = species
        .where((element) => element.name.isCaseInsensitiveContainsAny(name));

    for (var data in getPlant) {
      _listPlantByName.add(data);
    }
    notifyListeners();
  }

  @override
  List<ProductDto> get listAllSearch => _listPlantByName;

  @override
  Future<void> getAllSeed() async {
    _listSeeds.clear();
    var plants = await _speciesService.getAllSeed();
    for (var e in plants) {
      _listSeeds.add(e);
    }
    notifyListeners();
  }

  @override
  Future<void> getAllAcces() async {
    _listAccess.clear();
    var plants = await _speciesService.getAllAcces();
    for (var e in plants) {
      _listAccess.add(e);
    }
    notifyListeners();
  }

  @override
  List<ProductDto> get listAllAcces => _listAccess;

  @override
  List<ProductDto> get listAllSeed => _listSeeds;

  @override
  Future<void> selectCate(String name) async {
    if (name == 'Plant') {
      _listProduct = _listPlants;
    } else if (name == 'Seed') {
      _listProduct = _listSeeds;
    } else if (name == 'Accessories') {
      _listProduct = _listAccess;
    }
    notifyListeners();
  }
  
  @override
  List<ProductDto> get listAllProduct => _listProduct;
  
}
