import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:tech_wiz4/core/global/locator.dart';

import '../../../firebase/interfaces/ifirebase_auth.dart';
import '../../global/router.dart';
import '../interfaces/istart_screen_view_model.dart';

class StartScreenViewModel extends ChangeNotifier
    implements IStartScreenViewModel {
  // final IAuthService _authService = locator<IAuthService>();
  @override
  Future<void> goToNextPage() async {
    bool check = await locator<IFirebaseAuthService>().checkLogin();

    if (check) {
      Get.offNamed(MyRouter.home);
    } else {
      Get.offNamed(MyRouter.login);
    }
  }
}
