import 'package:flutter/foundation.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/global/locator.dart';
import 'package:tech_wiz4/core/model_ui/basket_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/favorite_model_ui.dart';

import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_order.dart';

class BasketViewViewModel extends ChangeNotifier
    implements IBasketViewViewModel {
  final List<BasketModelUI> _cartItems = [];
  final List<FavoriteModelUI> _favorites = [];
  final IFirebaseOrderService _orderService = locator<IFirebaseOrderService>();
  int _totalItems = 0;
  num _totalPrice = 0;
  num _totalPriceItems = 0;
  @override
  int get totalItems => _totalItems;
  @override
  List<FavoriteModelUI> get favorites => _favorites;
  @override
  List<BasketModelUI> get cartItems => _cartItems;
  bool _favo = false;
  @override
  Future<void> addToFavorite(ProductDto id) async {
    bool isProductInCart = _favorites.any((item) => item.productId == id);
    if (isProductInCart) {
      var getItem = _favorites.firstWhere((item) => item.productId == id);
      getItem.favorite == false;
      _favorites.removeWhere((item) => item.productId == id);
    } else {
      _favo = true;
      _favorites.add(FavoriteModelUI(productId: id, favorite: true));
    }
    notifyListeners();
  }

  @override
  Future<void> addToCart(ProductDto id) async {
    bool isProductInCart = _cartItems.any((item) => item.productId == id);
    _totalPriceItems = id.price;
    if (isProductInCart) {
      var getItem = _cartItems.firstWhere((item) => item.productId == id);
      getItem.quantity++;
      _totalPriceItems = getItem.productId.price * getItem.quantity;
      getItem.price = _totalPriceItems;
    } else {
      _cartItems.add(
          BasketModelUI(productId: id, quantity: 1, price: _totalPriceItems));
    }
    totalBasket();
    notifyListeners();
  }

  void totalBasket() {
    _totalItems = _cartItems.length;
    notifyListeners();
  }

  @override
  Future<void> checkOut(bool payment, int status) async {
    await _orderService.createOrder(_cartItems, _totalPrice, payment, status);
    _totalItems = 0;
    _totalPrice = 0;
    _cartItems.clear();
    notifyListeners();
  }

  @override
  Future<void> removeToCart(ProductDto id) async {
    if (_cartItems.firstWhere((item) => item.productId == id).quantity > 0) {
      var getItem = _cartItems.firstWhere((item) => item.productId == id);
      getItem.quantity--;
      _totalPriceItems = getItem.productId.price * getItem.quantity;
      getItem.price = _totalPriceItems;
    }
    if (_cartItems.firstWhere((item) => item.productId == id).quantity == 0) {
      _cartItems.firstWhere((item) => item.productId == id).price = 0;
    }

    _cartItems
        .removeWhere((item) => item.productId == id && item.quantity == 0);
    // setPrice();
    totalBasket();
    notifyListeners();
  }

  @override
  Future<void> deleteToCart(ProductDto id) async {
    _cartItems.removeWhere((item) => item.productId == id);
    // setPrice();
    totalBasket();
    notifyListeners();
  }

  @override
  num get totalPrice {
    _totalPrice = 0;
    for (var e in _cartItems) {
      _totalPrice += e.quantity * e.productId.price;
    }
    return _totalPrice;
  }

  @override
  bool get favo => _favo;
}
