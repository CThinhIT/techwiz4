import 'package:flutter/material.dart';

import '../../dto/model/product_dto.dart';
import '../../model_ui/basket_model_ui.dart';
import '../../model_ui/favorite_model_ui.dart';

abstract class IBasketViewViewModel implements ChangeNotifier {
  Future<void> addToCart(ProductDto id);
  List<BasketModelUI> get cartItems;
  Future<void> checkOut(bool payment, int status);
  int get totalItems;
  Future<void> removeToCart(ProductDto id);
  Future<void> deleteToCart(ProductDto id);
  Future<void> addToFavorite(ProductDto id);
  num get totalPrice;
  List<FavoriteModelUI> get favorites;
  bool get favo;
}
