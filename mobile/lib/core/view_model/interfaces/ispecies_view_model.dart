import 'package:flutter/material.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/dto/model/seed_dto.dart';
import 'package:tech_wiz4/core/model_ui/species_model_ui.dart';

abstract class ISpeciesViewModel implements ChangeNotifier {
  Future<void> getList(String id);
  Future<void> getAllSpecies();

  List<SpeciesModelUI> get listSpec;

  Future<void> getListPlant(String id);

  List<ProductDto> get listPlant;

  Future<void> getListSeed(String id);

  List<SeedDto> get listSeed;

  Future<void> getAllPlant();
  Future<void> getAllSeed();
  Future<void> getAllAcces();
  List<ProductDto> get listAllPlant;
  List<ProductDto> get listAllAcces;
  List<ProductDto> get listAllSeed;
  Future<void> searchByname(String name);
  List<ProductDto> get listAllSearch;
  Future<void> sortProductLow();
  Future<void> sortProductHigh();
  Future<void> selectCate(String name);
  List<ProductDto> get listAllProduct;
}
