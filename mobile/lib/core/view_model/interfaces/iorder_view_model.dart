import 'package:flutter/material.dart';
import 'package:tech_wiz4/core/dto/model/order_dto.dart';

import '../../dto/model/order_detail_dto.dart';

abstract class IOrderViewModel implements ChangeNotifier{
  Future<List<OrderDto>> getOrder();
  Future<void> getHistoryOrder();
  Future<void> getConfirm();
  Future<void> getPickup();
  Future<void> getDelivery();
  List<OrderDetailDto> get historyOrders;
  List<OrderDetailDto> get confirmOrders;
  List<OrderDetailDto> get pickupOrders;
  List<OrderDetailDto> get deliveryOrders;
}