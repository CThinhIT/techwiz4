import 'package:json_annotation/json_annotation.dart';

part 'customer_dto.g.dart';

@JsonSerializable(explicitToJson: true)
class CustomerDto {
  int id;
  String? email;
  String? name;
  String password;
  String? token;
  String? address;
  CustomerDto({
    required this.id,
    this.email,
    this.name,
    required this.password,
    this.token,
    this.address,
  });

  factory CustomerDto.fromJson(Map<String, dynamic> json) =>
      _$CustomerDtoFromJson(json);
  Map<String, dynamic> toJson() => _$CustomerDtoToJson(this);
}
