import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';

class OrderDetailDto {
  String id;
  num price;
  num quantity;
  DocumentReference productId;
  DocumentReference orderId;
  OrderDetailDto({
    required this.id,
    required this.price,
    required this.quantity,
    required this.productId,
    required this.orderId,
  });
}
