// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cloud_firestore/cloud_firestore.dart';

class BillingDto {
  String id;
  num billAmount;
  DocumentReference orderId;
  bool methodPayment;
  num status;
  BillingDto({
    required this.id,
    required this.billAmount,
    required this.orderId,
    required this.methodPayment,
    required this.status,
  });
}
