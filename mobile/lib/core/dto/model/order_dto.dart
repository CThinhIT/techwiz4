import 'package:cloud_firestore/cloud_firestore.dart';

class OrderDto {
  String id;
  Timestamp date;
  num status;
  DocumentReference userId;

  OrderDto({
    required this.id,
    required this.date,
    required this.status,
    required this.userId,
  });
}
