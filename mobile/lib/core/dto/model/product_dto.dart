import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tech_wiz4/core/dto/model/category_dto.dart';
import 'package:tech_wiz4/core/dto/model/species_dto.dart';

class ProductDto {
  String id;
  String about;
  String care;
  String description;
  String image;
  String light;
  String name;
  num price;
  String size;
  num stock;
  String temperature;
  String water;
  String incubate;
  DocumentReference catergoryId;
  DocumentReference speciesId;
  CategoryDto cate;
  SpeciesDto spec;
  ProductDto({
    required this.id,
    required this.about,
    required this.care,
    required this.catergoryId,
    required this.description,
    required this.image,
    required this.light,
    required this.name,
    required this.price,
    required this.size,
    required this.speciesId,
    required this.stock,
    required this.temperature,
    required this.water,
    required this.incubate,
    required this.cate,
    required this.spec,
  });

  static fromMap(Map<String, dynamic> map) {}
}
