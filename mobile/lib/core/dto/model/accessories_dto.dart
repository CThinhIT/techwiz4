import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tech_wiz4/core/dto/model/category_dto.dart';
import 'package:tech_wiz4/core/dto/model/species_dto.dart';

class AccessoriesDto {
  String id;
  String about;
  CategoryDto categoryId;
  String description;
  String image;
  String name;
  num price;
  num stock;
  DocumentReference cateid;
  DocumentReference spec;
  SpeciesDto speciesDto;
  AccessoriesDto({
    required this.id,
    required this.about,
    required this.categoryId,
    required this.description,
    required this.image,
    required this.name,
    required this.price,
    required this.stock,
    required this.cateid,
    required this.spec,
    required this.speciesDto,
  });
}
