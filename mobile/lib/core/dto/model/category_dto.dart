// ignore_for_file: public_member_api_docs, sort_constructors_first

class CategoryDto {
  String id;
  String name;
  CategoryDto({
    required this.id,
    required this.name,
  });
}
