// ignore_for_file: public_member_api_docs, sort_constructors_first
class RolesDto {
  String id;
  String description;
  String name;

  RolesDto({
    required this.id,
    required this.description,
    required this.name,
  });
}
