// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tech_wiz4/core/dto/model/category_dto.dart';

class SpeciesDto {
  String id;
  String icon;
  String name;
  CategoryDto cateDto;
  DocumentReference cateDoc;

  SpeciesDto({
    required this.id,
    required this.icon,
    required this.name,
    required this.cateDoc,
    required this.cateDto,
  });
}
