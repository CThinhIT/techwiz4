// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cloud_firestore/cloud_firestore.dart';

class UsersDto {
  String id;
  String address;
  num age;
  String email;
  bool gender = false;
  String name;
  String password;
  String phone;
  DocumentReference roleId;
  num status;
  String username;

  UsersDto({
    required this.id,
    required this.address,
    required this.age,
    required this.email,
    required this.gender,
    required this.name,
    required this.password,
    required this.phone,
    required this.roleId,
    required this.status,
    required this.username,
  });
}
