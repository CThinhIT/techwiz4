// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tech_wiz4/core/dto/model/category_dto.dart';
import 'package:tech_wiz4/core/dto/model/species_dto.dart';

class SeedDto {
  String id;
  String about;
  String image;
  String incubate;
  String name;
  num price;
  num stock;
  String description;
  DocumentReference categoryId;
  DocumentReference speciesId;
  CategoryDto cate;
  SpeciesDto spec;
  SeedDto({
    required this.id,
    required this.about,
    required this.categoryId,
    required this.speciesId,
    required this.image,
    required this.incubate,
    required this.name,
    required this.price,
    required this.stock,
    required this.cate,
    required this.spec,
    required this.description,
  });
}
