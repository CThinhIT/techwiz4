// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:tech_wiz4/core/dto/model/Users_dto.dart';

class FeedbackDto {
  String id;
  String title;
  String content;
  UsersDto userId;

  FeedbackDto({
    required this.id,
    required this.title,
    required this.content,
    required this.userId,
  });
}
