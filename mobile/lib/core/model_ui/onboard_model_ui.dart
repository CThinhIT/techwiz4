import 'package:uuid/uuid.dart';

class OnboardModelUI {
  var id = const Uuid().v4();
  String image;
  
  OnboardModelUI({
    required this.image,
  });

  static List<OnboardModelUI> items = [
    OnboardModelUI(
      image: "assets/images/onboard1.png",
    ),
    OnboardModelUI(
      image: "assets/images/onboard2.png",
    ),
    OnboardModelUI(
      image: "assets/images/onboard3.png",
    ),
  ];
}
