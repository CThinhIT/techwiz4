import 'package:uuid/uuid.dart';

class CarouselModelUI {
  var id = const Uuid().v4();
  String images;
  CarouselModelUI({
    required this.images,
  });

  static List<CarouselModelUI> items = [
    CarouselModelUI(
      images: "assets/images/carousel_home1.png",
    ),
    CarouselModelUI(
      images: "assets/images/carousel_home2.png",
    ),
    CarouselModelUI(
      images: "assets/images/carousel_home3.png",
    ),
  ];
}
