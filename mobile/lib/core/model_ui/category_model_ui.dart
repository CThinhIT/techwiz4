class CategoryModelUI {
  String id;
  String icon;
  String name;
  CategoryModelUI({
    required this.id,
    required this.icon,
    required this.name,
  });

  static List<CategoryModelUI> items = [
    CategoryModelUI(
      icon: "assets/images/Sparkle.png",
      name: "For you",
      id: "0",
    ),
    CategoryModelUI(
      icon: "assets/images/Trees.png",
      name: "Trees",
      id: "1",
    ),
    CategoryModelUI(
      icon: "assets/images/Seed.png",
      name: "Seed",
      id: "2",
    ),
    CategoryModelUI(
      icon: "assets/images/Garder.png",
      name: "Garden",
      id: "3",
    ),
  ];
}
