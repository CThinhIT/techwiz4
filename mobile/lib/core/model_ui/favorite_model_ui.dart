import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:uuid/uuid.dart';

class FavoriteModelUI {
  var id = const Uuid().v4();
  ProductDto productId;
  bool favorite;
  FavoriteModelUI({
    required this.productId,
    required this.favorite,
  });
}
