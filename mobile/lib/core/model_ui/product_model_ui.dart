import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tech_wiz4/core/model_ui/species_model_ui.dart';

class ProductModelUI {
  String id;
  String family;
  String name;
  String type;
  num price;
  String image;
  bool favorite = false;
  DocumentReference speId;
  SpeciesModelUI species;
  ProductModelUI({
    required this.id,
    required this.family,
    required this.name,
    required this.type,
    required this.price,
    required this.image,
    required this.speId,
    required this.species,
  });

  // static List<ProductModelUI> items = [
  //   ProductModelUI(
  //     id: "1",
  //     family: 'Monstera family',
  //     type: "Indoor",
  //     name: "Monstera Adansonii",
  //     image: "assets/images/plant1.png",
  //     price: 35,
  //   ),
  //   ProductModelUI(
  //     id: "2",
  //     family: 'Monstera family',
  //     type: "Indoor",
  //     name: "Monstera Adansonii",
  //     image: "assets/images/plant1.png",
  //     price: 35,
  //   ),
  //   ProductModelUI(
  //     id: "3",
  //     family: 'Monstera family',
  //     type: "Indoor",
  //     name: "Monstera Adansonii",
  //     image: "assets/images/plant1.png",
  //     price: 35,
  //   ),
  //   ProductModelUI(
  //     id: "4",
  //     family: 'Monstera family',
  //     type: "Indoor",
  //     name: "Monstera Adansonii",
  //     image: "assets/images/plant1.png",
  //     price: 35,
  //   ),
  // ];
}
