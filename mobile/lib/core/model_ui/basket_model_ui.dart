import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:uuid/uuid.dart';

class BasketModelUI {
  var id = const Uuid().v4();
  ProductDto productId;
  int quantity;
  num price;
  BasketModelUI({
    required this.productId,
    required this.quantity,
    required this.price,
  });
}
