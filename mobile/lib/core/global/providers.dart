import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:tech_wiz4/core/view_model/implements/auth_view_model.dart';
import 'package:tech_wiz4/core/view_model/implements/basket_view_model.dart';
import 'package:tech_wiz4/core/view_model/implements/order_view_model.dart';
import 'package:tech_wiz4/core/view_model/implements/species_view_model.dart';
import 'package:tech_wiz4/core/view_model/implements/start_screen_view_model.dart';
import 'package:tech_wiz4/core/view_model/interfaces/iauth_view_model.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ibasket_view_model.dart';
import 'package:tech_wiz4/core/view_model/interfaces/iorder_view_model.dart';
import 'package:tech_wiz4/core/view_model/interfaces/ispecies_view_model.dart';
import 'package:tech_wiz4/core/view_model/interfaces/istart_screen_view_model.dart';

List<SingleChildWidget> viewModelProviders = [
  ChangeNotifierProvider<IStartScreenViewModel>(
    create: (_) => StartScreenViewModel(),
  ),
  ChangeNotifierProvider<IAuthViewModel>(
    create: (_) => AuthViewModel(),
  ),
  ChangeNotifierProvider<IBasketViewViewModel>(
    create: (_) => BasketViewViewModel(),
  ),
  ChangeNotifierProvider<ISpeciesViewModel>(
    create: (_) => SpeciesViewViewModel(),
  ),
  ChangeNotifierProvider<IOrderViewModel>(
    create: (_) => OrderViewModel(),
  ),
];
