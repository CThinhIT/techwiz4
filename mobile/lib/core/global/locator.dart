import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:tech_wiz4/api/rest_client.dart';
import 'package:tech_wiz4/core/global/global_data.dart';
import 'package:tech_wiz4/core/global/locator_service.dart';

GetIt locator = GetIt.instance;

RestClient getRestClient() {
  return locator.get<RestClient>(instanceName: 'RestClient');
}

Future<void> setupLocator() async {
  locator.registerLazySingleton(() => GlobalData());
  registerServiceSingletons(locator);
  setupRestClient();
  // await checkLogin();
}



void setupRestClient() {
  var dio = Dio();
  try {
    locator.registerLazySingleton(
      () => RestClient(dio, baseUrl: 'http://10.0.2.2:8888/api'),
      instanceName: "RestClient",
    );
  } catch (e) {
    // Future.wait([LoggerUtils.logException(e)]);
  }
}
