import 'package:tech_wiz4/core/dto/auth/access_token_dto.dart';
import 'package:tech_wiz4/core/dto/model/users_dto.dart';


class GlobalData {
  UsersDto? currentUser;
  AccessToken? token;
}
