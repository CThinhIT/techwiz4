import 'package:flutter/material.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/model_ui/category_model_ui.dart';
import 'package:tech_wiz4/ui/account_screen/account_screen.dart';
import 'package:tech_wiz4/ui/cart_screen/cart_screen.dart';
import 'package:tech_wiz4/ui/checkout_screen/checkout_screen.dart';
import 'package:tech_wiz4/ui/contact_screen/contact_screen.dart';
import 'package:tech_wiz4/ui/detail_product/detail_product_screen.dart';
import 'package:tech_wiz4/ui/favorite_screen/favorite_screen.dart';
import 'package:tech_wiz4/ui/help_screen/help_screen.dart';
import 'package:tech_wiz4/ui/home_screen/home_screen.dart';
import 'package:tech_wiz4/ui/login_screen/login_screen.dart';
import 'package:tech_wiz4/ui/login_screen/register_screen.dart';
import 'package:tech_wiz4/ui/manage_order_screen/confirm_order_screen.dart';
import 'package:tech_wiz4/ui/manage_order_screen/delivery_order_screen.dart';
import 'package:tech_wiz4/ui/manage_order_screen/feedback_order_screen.dart';
import 'package:tech_wiz4/ui/manage_order_screen/history_order_screen.dart';
import 'package:tech_wiz4/ui/manage_order_screen/pickup_order_screen.dart';
import 'package:tech_wiz4/ui/onboarding_screen/onboarding_screen.dart';
import 'package:tech_wiz4/ui/personal_screen/edit/info_screen.dart';
import 'package:tech_wiz4/ui/personal_screen/edit/password_screen.dart';
import 'package:tech_wiz4/ui/personal_screen/persional_screen.dart';
import 'package:tech_wiz4/ui/result_search_screen/result_search_screen.dart';
import 'package:tech_wiz4/ui/species_screen/species_screen.dart';
import 'package:tech_wiz4/ui/start_screen/start_screen.dart';
import 'package:tech_wiz4/ui/tab_screen/tab_screen.dart';
import '../../ui/confirm_checkout_screen/confirm_checkout_screen.dart';

class MyRouter {
  static const String home = '/home';
  static const String start = '/start';
  static const String login = '/login';
  static const String register = '/register';
  static const String verify = '/verify';
  static const String fogot = '/fogot';
  static const String account = '/account';
  static const String persional = '/persional';
  static const String updateInfo = '/updateInfo';
  static const String resetPassword = '/resetPassword';
  static const String term = '/term';
  static const String onboard = '/onboard';
  static const String searchs = '/searchs';
  static const String search = '/search';
  static const String detail = '/detail';
  static const String chat = '/chat';
  static const String species = '/species';
  static const String contact = '/contact';
  static const String confirm = '/confirm';
  static const String cart = '/cart';
  static const String defaut = '/';
  static const String checkout = '/checkout';
  static const String tab = '/tab';
  static const String help = '/help';
  static const String searchresult = '/search';
  static const String favorite = '/favorite';
  static const String history = '/history';
  static const String confirmOrder = '/confirmOrder';
  static const String delivery = '/delivery';
  static const String feedback = '/feedback';
  static const String pickup = '/pickup';
  static PageRouteBuilder _buildRouteNavigationWithoutEffect(
      RouteSettings settings, Widget widget) {
    return PageRouteBuilder(
      pageBuilder: (_, __, ___) => widget,
      transitionDuration: Duration.zero,
      settings: settings,
    );
  }

  static PageRouteBuilder buildRouteNavigation(
      RouteSettings settings, Widget widget) {
    return PageRouteBuilder(
      pageBuilder: (_, __, ___) => widget,
      settings: settings,
    );
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case start:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const StartScreen(),
        );
      case home:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const HomeScreen(),
        );
      case login:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const LoginScreen(),
        );
      case register:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const RegisterScreen(),
        );
      case detail:
        return _buildRouteNavigationWithoutEffect(
          settings,
          DetailProductScreen(
            item: settings.arguments as ProductDto,
          ),
        );
      case species:
        return _buildRouteNavigationWithoutEffect(
          settings,
          SpeciesScreen(
            item: settings.arguments as CategoryModelUI,
          ),
        );
      case cart:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const CartScreen(),
        );
      case checkout:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const CheckoutScreen(),
        );
      // case fogot:
      //   return _buildRouteNavigationWithoutEffect(
      //     settings,
      //     const StartScreen(),
      //   );

      case account:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const AccountScreen(),
        );

      case persional:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const PersionalScreen(),
        );

      case updateInfo:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const InfomationScreen(),
        );

      case resetPassword:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const PasswordScreen(),
        );

      case help:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const HelpScreen(),
        );

      // case term:
      //   return _buildRouteNavigationWithoutEffect(
      //     settings,
      //     const TermOfServiceScreen(),
      //   );

      case onboard:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const OnBoardingScreen(),
        );
      case contact:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const ContactScreen(),
        );
      case confirm:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const ConfirmScreen(),
        );

      case tab:
        return _buildRouteNavigationWithoutEffect(
          settings,
          TabScreen(
            cureentIndex: settings.arguments as int,
          ),
        );
      case searchresult:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const ResultSearchScreen(),
        );

      // case search:
      //   return _buildRouteNavigationWithoutEffect(
      //     settings,
      //     const SearchTourScreen(),
      //   );

      // case searchs:
      //   return _buildRouteNavigationWithoutEffect(
      //     settings,
      //     const SearchScreen(),
      //   );

      case favorite:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const FavoriteScreen(),
        );
      case history:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const HistoryOrderScreen(),
        );

      case confirmOrder:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const ConfirmOrderScreen(),
        );
      case delivery:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const DeliveryOrderScreen(),
        );
      case feedback:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const FeedbackOrderScreen(),
        );
      case pickup:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const PickupOrderScreen(),
        );
      case defaut:
        return _buildRouteNavigationWithoutEffect(
          settings,
          const HomeScreen(),
        );
      default:
        return _buildRouteNavigationWithoutEffect(
          settings,
          Scaffold(
            body: Center(
              child: Text('No route found: ${settings.name}.'),
            ),
          ),
        );
    }
  }
}
