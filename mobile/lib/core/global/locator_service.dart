import 'package:get_it/get_it.dart';
import 'package:tech_wiz4/core/services/implements/auth_service.dart';
import 'package:tech_wiz4/core/services/interfaces/iauth_service.dart';
import 'package:tech_wiz4/firebase/implemnts/firebase_auth.dart';
import 'package:tech_wiz4/firebase/implemnts/firebase_order.dart';
import 'package:tech_wiz4/firebase/implemnts/firebase_species.dart';
import 'package:tech_wiz4/firebase/implemnts/firebase_update.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_auth.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_order.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_product.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_species.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_update.dart';

import '../../firebase/implemnts/firebase_product.dart';

void registerServiceSingletons(GetIt locator) {
  locator.registerLazySingleton<IAuthService>(() => AuthService());
  locator
      .registerLazySingleton<IFirebaseAuthService>(() => FirebaseAuthService());
  locator.registerLazySingleton<IFirebaseProductService>(
      () => FirebaseProductService());
  locator.registerLazySingleton<IFirebaseOrderService>(
      () => FirebaseOrderService());    
  locator.registerLazySingleton<IFirebaseSpeciesService>(
      () => FirebaseSpeciesService());
  locator.registerLazySingleton<IFirebaseUpdateService>(
      () => FireBaseUpdateService());
}
