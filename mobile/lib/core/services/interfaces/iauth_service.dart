import 'package:tech_wiz4/core/dto/customers/customer_dto.dart';

abstract class IAuthService {
  Future<CustomerDto?> login(String email, String password);
  Future<bool> checkLogin();
  Future<void> logOut();
}
