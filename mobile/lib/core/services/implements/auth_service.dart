import 'package:get/get.dart';
import 'package:tech_wiz4/core/dto/customers/customer_dto.dart';
import 'package:tech_wiz4/core/global/router.dart';
import '../../global/global_data.dart';
import '../../global/locator.dart';
import '../../utils/token_utils.dart';
import '../interfaces/iauth_service.dart';

class AuthService implements IAuthService {
  @override
  Future<CustomerDto?> login(String email, String password) async {
    // try {
    //   final result = await getRestClient().login(email, password);
    //   TokenUtils.saveToken("Bearer ${result.token}");
    // } on Exception catch (e) {
    //   print(e);
    // }
    // var token = await TokenUtils.getToken();
    // if (token != null) {
    //   try {
    //     account = await getRestClient().getProfile(token: token);
    //     // account = profileResponseDto.result;
    //     locator<GlobalData>().currentUser = account;
    //     return account;
    //   } on Exception catch (e) {
    //     print(e);
    //   } finally {}
    // }
    return null;
  }

  @override
  Future<bool> checkLogin() async {
    // var token = await TokenUtils.getToken();
    // if (token != null) {
    //   try {
    //     var result = await getRestClient().getProfile(token: token);
    //     TokenUtils.saveToken(token);
    //     locator<GlobalData>().currentUser = result;
    //     locator<GlobalData>().token = AccessToken(
    //       token: token,
    //       id: result.id,
    //     );
    //     return true;
    //   } catch (e) {
    //     print("chec ${e}");
    //   } finally {}
    // }
    return false;
  }

  @override
  Future<void> logOut() async {
    locator<GlobalData>().token = null;
    locator<GlobalData>().currentUser = null;
    TokenUtils.removeToken();
    Get.offNamed(MyRouter.login);
  }
}
