import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:tech_wiz4/core/dto/customers/customer_dto.dart';
part 'rest_client.g.dart';

@RestApi()
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  // Rest api for account
  @GET('/users/profile')
  Future<CustomerDto> getProfile({
    @Header('Authorization') required String token,
  });

  @POST('/auth/login')
  Future<CustomerDto> login(
    @Query('email') String email,
    @Query('password') String password,
  );

  // @POST('/auth/register')
  // Future<AccountModelUI> register(
  //   @Body() RegisterDto model,
  // );

  // @PUT('/users/change-password/{id}')
  // Future<bool> changePassword(
  //   @Path('id') int id,
  //   @Query('oldPassword') String oldPassword,
  //   @Query('newPassword') String newPassword,
  // );

  // // Rest api for tours
  // @GET('/tours/')
  // Future<List<TourModelUI>> getTours();

  // @GET('/tours/{id}')
  // Future<TourModelUI> getTour(
  //   @Path('id') int id,
  // );

  // @GET('/tours/region')
  // Future<List<RegionModelUI>> getRegions();

  // @GET('/tours/regions/{id}/tours')
  // Future<List<TourModelUI>> getToursByRegions(
  //   @Path('id') int id,
  // );
}
