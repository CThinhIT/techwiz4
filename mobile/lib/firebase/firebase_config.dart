import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class FirebaseConfig {
  static late FirebaseApp firebaseApp;
  static late FirebaseDatabase database;

  static Future<void> initialize() async {
    firebaseApp = await Firebase.initializeApp(
      // options: DefaultFirebaseOptions.currentPlatform,
    );
    database = FirebaseDatabase.instance;
  }

  static DatabaseReference getReference(String path) {
    return database.ref(path);
  }

  static Stream<DatabaseEvent>? getListFixture() {
    return database.ref('/ListFixture').onValue;
  }
}
