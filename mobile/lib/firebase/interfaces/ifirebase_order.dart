import 'package:tech_wiz4/core/dto/model/billing_dto.dart';
import 'package:tech_wiz4/core/dto/model/order_detail_dto.dart';
import 'package:tech_wiz4/core/dto/model/order_dto.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/model_ui/basket_model_ui.dart';

abstract class IFirebaseOrderService {
  // Future<void> checkOut(List<Map<String, dynamic>> ordersList);
  Future<List<OrderDto>> getOrder();
  Future<void> createOrder(
      List<BasketModelUI> basket, num amount, bool payment, int status);
  Future<List<OrderDetailDto>> getOrderDetail(String id);
}
