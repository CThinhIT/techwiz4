import 'package:tech_wiz4/core/dto/model/users_dto.dart';

abstract class IFirebaseAuthService {
  Future<void> signIn(String email, String password);
  Future<void> signUp(String name, String email, String phone,
      String passsword);
  // Future signInWithGoogle();
  // Future<void> signInAnonymously();
  Future<void> signOut();
  Future<bool> checkLogin();
  Future<void> deleteAccount();
  UsersDto? get currentUser;
}
