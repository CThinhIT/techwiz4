import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/dto/model/seed_dto.dart';
import 'package:tech_wiz4/core/model_ui/category_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/species_model_ui.dart';

abstract class IFirebaseSpeciesService {
  Future<List<SpeciesModelUI>> getSpecies(String id);
  Future<List<SpeciesModelUI>> getAllSpecies();
  Future<List<CategoryModelUI>> getCategory();
  Future<List<ProductDto>> getPlantBySpeciesId(String id);
  Future<List<SeedDto>> getSeedBySpeciesId(String id);
  Future<List<ProductDto>> getAllPlant();
  Future<List<ProductDto>> getAllSeed();
  Future<List<ProductDto>> getAllAcces();
}
