abstract class IFirebaseUpdateService{
  Future<void> updateUser(String username, String name, String address, String email, String phone);

  Future<void> updatePassword(String password);
}