import 'package:tech_wiz4/core/dto/model/product_dto.dart';

abstract class IFirebaseProductService {
  Future<List<ProductDto>> getProduct();
  Future<List<ProductDto>> getProductForHome();
}
