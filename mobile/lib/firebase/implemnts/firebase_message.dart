// import 'dart:convert';

// import 'package:catch_the_word/core/firebase/interfaces/ifirebase_message.dart';
// import 'package:catch_the_word/core/model_ui/user_ui_model.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';

// class FirebaseMessageService implements IFirebaseMessageService {
//   final String _collection = 'messages';
//   static final _firestore = FirebaseFirestore.instance;
//   @override
//   Stream<QuerySnapshot> messageStream(groupId) {
//     return _firestore
//         .collection(_collection)
//         .doc('groupId')
//         .collection(groupId)
//         .orderBy('time', descending: true)
//         .snapshots();
//   }

//   @override
//   Future<void> sendMessage(
//       String message, String senderId, String groupId) async {
//     await _firestore
//         .collection(_collection)
//         .doc("groupId")
//         .collection(groupId)
//         .add({
//       'senderId': senderId,
//       'message': message,
//       'time': DateTime.now(),
//     });
//   }

//   @override
//   CollectionReference<User> chatList()  {
//     var data =  FirebaseFirestore.instance
//         .collection('users')
//         .withConverter<User>(
//             fromFirestore: (snapshot, _) => User.fromJson(snapshot.data()!),
//            toFirestore: (user, _) => user.toJson(),);
//     return data;
//   }
// }
