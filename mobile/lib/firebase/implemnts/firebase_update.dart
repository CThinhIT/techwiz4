import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:tech_wiz4/core/dto/model/users_dto.dart';

import '../../core/global/global_data.dart';
import '../../core/global/locator.dart';
import '../../core/utils/token_utils.dart';
import '../interfaces/ifirebase_update.dart';

class FireBaseUpdateService implements IFirebaseUpdateService {
  static final _firestore = FirebaseFirestore.instance;
  static final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  Future<void> updateUser(String username, String name, String address,
      String email, String phone) async {
    var token = await TokenUtils.getToken();
    DocumentReference docRef = _firestore.collection('users').doc(token);
    await docRef.update({
      'username': username,
      'name': name,
      'address': address,
      'email': email,
      'phone': phone,
    });
    locator<GlobalData>().currentUser = await _getUser();
  }

  @override
  Future<void> updatePassword(String password) async {
    var token = await TokenUtils.getToken();
    DocumentReference docRef = _firestore.collection('users').doc(token);
    await docRef.update({
      'password': password,
    });
    AuthCredential credential = EmailAuthProvider.credential(
        email: locator<GlobalData>().currentUser!.email,
        password: locator<GlobalData>().currentUser!.password);
    await _firebaseAuth.currentUser!.reauthenticateWithCredential(credential);

    // Update the user's password
    await _firebaseAuth.currentUser!.updatePassword(password);
    
    locator<GlobalData>().currentUser = await _getUser();
  }

  Future<UsersDto> _getUser() async {
    var token = await TokenUtils.getToken();
    final userSnapshot = await _firestore.collection('users').doc(token).get();
    return UsersDto(
      id: userSnapshot.id,
      address: userSnapshot['address'] ?? '',
      age: userSnapshot['age'] ?? 0,
      email: userSnapshot['email'],
      gender: userSnapshot['gender'] ?? true,
      name: userSnapshot['name'] ?? '',
      password: userSnapshot['password'],
      phone: userSnapshot['phone'],
      roleId: userSnapshot['role_id'],
      status: userSnapshot['status'],
      username: userSnapshot['username'],
    );
  }
}
