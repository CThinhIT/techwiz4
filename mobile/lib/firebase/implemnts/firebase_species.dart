import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:tech_wiz4/core/dto/model/category_dto.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/dto/model/seed_dto.dart';
import 'package:tech_wiz4/core/dto/model/species_dto.dart';
import 'package:tech_wiz4/core/model_ui/category_model_ui.dart';
import 'package:tech_wiz4/core/model_ui/species_model_ui.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_species.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseSpeciesService implements IFirebaseSpeciesService {
  static final _firestore = FirebaseFirestore.instance;
  static final _fireStorage = FirebaseStorage.instance;
  @override
  Future<List<SpeciesModelUI>> getSpecies(String id) async {
    List<SpeciesModelUI> species = [];
    try {
      QuerySnapshot querySnapshot =
          await _firestore.collection('species').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['icon']);
        var url = await ref.getDownloadURL();
        if (doc.exists) {
          Map<String, dynamic> data = doc.data() as Map<String, dynamic>;

          // Fetch the referenced category document
          DocumentReference categoryReference = data['category_id'];
          DocumentSnapshot categorySnapshot = await categoryReference.get();
          Map<String, dynamic> categoryData =
              categorySnapshot.data() as Map<String, dynamic>;

          CategoryModelUI category = CategoryModelUI(
            id: categorySnapshot.id,
            name: categoryData['name'] ?? '',
            icon: categoryData['icon'] ?? '',
          );

          // final ref = _fireStorage.ref().child(doc['icon']);
          //         var url = await ref.getDownloadURL();
          SpeciesModelUI specie = SpeciesModelUI(
            id: doc.id,
            icon: url,
            name: data['name'] ?? '',
            categoryId: category.id,
          );
          species.add(specie);
        }
      }
    } catch (e) {
      print("Error getting species: $e");
    }
    return species;
  }

  @override
  Future<List<CategoryModelUI>> getCategory() async {
    List<CategoryModelUI> categories = [];
    try {
      QuerySnapshot querySnapshot =
          await _firestore.collection('category').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        CategoryModelUI category = CategoryModelUI(
          id: doc.id,
          name: doc['name'] ?? '',
          icon: doc['icon'] ?? '',
        );

        categories.add(category);
      }
    } catch (e) {
      print('Error getting products: $e');
    }

    return categories;
  }

  @override
  Future<List<ProductDto>> getPlantBySpeciesId(String id) async {
    List<ProductDto> plants = [];
    try {
      QuerySnapshot querySnapshot = await _firestore.collection('plants').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        if (doc.exists) {
          Map<String, dynamic> data = doc.data() as Map<String, dynamic>;

          // Fetch the referenced category document
          DocumentReference categoryReference = data['category_id'];
          DocumentSnapshot categorySnapshot = await categoryReference.get();
          Map<String, dynamic> categoryData =
              categorySnapshot.data() as Map<String, dynamic>;

          DocumentReference speciesReference = data['species_id'];
          DocumentSnapshot speciesSnapshot = await speciesReference.get();
          Map<String, dynamic> speciesData =
              speciesSnapshot.data() as Map<String, dynamic>;

          CategoryDto category = CategoryDto(
            id: categorySnapshot.id,
            name: categoryData['name'] ?? '',
          );

          SpeciesDto species = SpeciesDto(
            id: speciesSnapshot.id,
            icon: speciesData['icon'] ?? '',
            name: speciesData['name'] ?? '',
            cateDoc: categoryReference,
            cateDto: category,
          );

          ProductDto specie = ProductDto(
            id: doc.id,
            about: data['about'] ?? '',
            size: data['size'] ?? '',
            care: data['care'] ?? '',
            description: data['description'] ?? '',
            image: data['image'] ?? '',
            light: data['light'] ?? '',
            name: data['name'] ?? '',
            price: data['price'] ?? '',
            stock: data['stock'] ?? '',
            incubate: data['incubate,'] ?? '',
            temperature: data['temperature'] ?? '',
            water: data['water'] ?? '',
            speciesId: speciesReference,
            cate: category,
            spec: species,
            catergoryId: data['category_id'] ?? '',
          );
          plants.add(specie);
        }
      }
    } catch (e) {
      print("Error getting species: $e");
    }
    return plants;
  }

  @override
  Future<List<ProductDto>> getAllPlant() async {
    List<ProductDto> plants = [];
    try {
      QuerySnapshot querySnapshot = await _firestore.collection('plants').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['image']);
        var url = await ref.getDownloadURL();
        if (doc.exists) {
          Map<String, dynamic> data = doc.data() as Map<String, dynamic>;

          // Fetch the referenced category document
          DocumentReference categoryReference = data['category_id'];
          DocumentSnapshot categorySnapshot = await categoryReference.get();
          Map<String, dynamic> categoryData =
              categorySnapshot.data() as Map<String, dynamic>;

          DocumentReference speciesReference = data['species_id'];
          DocumentSnapshot speciesSnapshot = await speciesReference.get();
          Map<String, dynamic> speciesData =
              speciesSnapshot.data() as Map<String, dynamic>;

          CategoryDto category = CategoryDto(
            id: categorySnapshot.id,
            name: categoryData['name'] ?? '',
          );

          SpeciesDto species = SpeciesDto(
            id: speciesSnapshot.id,
            icon: speciesData['icon'] ?? '',
            name: speciesData['name'] ?? '',
            cateDoc: categoryReference,
            cateDto: category,
          );

          ProductDto specie = ProductDto(
            id: doc.id,
            about: data['about'] ?? '',
            size: data['size'] ?? '',
            care: data['care'] ?? '',
            description: data['description'] ?? '',
            image: url,
            light: data['light'] ?? '',
            name: data['name'] ?? '',
            price: data['price'] ?? '',
            stock: data['stock'] ?? '',
            incubate: data['incubate,'] ?? '',
            temperature: data['temperature'] ?? '',
            water: data['water'] ?? '',
            catergoryId: data['category_id'] ?? '',
            speciesId: speciesReference,
            cate: category,
            spec: species,
          );
          plants.add(specie);
        }
      }
    } catch (e) {
      print("Error getting species: $e");
    }
    return plants;
  }

  @override
  Future<List<SeedDto>> getSeedBySpeciesId(String id) async {
    List<SeedDto> seeds = [];
    try {
      QuerySnapshot querySnapshot = await _firestore.collection('seed').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        if (doc.exists) {
          Map<String, dynamic> data = doc.data() as Map<String, dynamic>;

          // Fetch the referenced category document
          DocumentReference categoryReference = data['category_id'];
          DocumentSnapshot categorySnapshot = await categoryReference.get();
          Map<String, dynamic> categoryData =
              categorySnapshot.data() as Map<String, dynamic>;

          DocumentReference speciesReference = data['species_id'];
          DocumentSnapshot speciesSnapshot = await speciesReference.get();
          Map<String, dynamic> speciesData =
              speciesSnapshot.data() as Map<String, dynamic>;

          CategoryDto category = CategoryDto(
            id: categorySnapshot.id,
            name: categoryData['name'] ?? '',
          );

          SpeciesDto species = SpeciesDto(
            id: speciesSnapshot.id,
            icon: speciesData['icon'] ?? '',
            name: speciesData['name'] ?? '',
            cateDoc: categoryReference,
            cateDto: category,
          );

          SeedDto specie = SeedDto(
            id: doc.id,
            about: data['about'] ?? '',
            incubate: data['incubate'] ?? '',
            image: data['image'] ?? '',
            name: data['name'] ?? '',
            price: data['price'] ?? '',
            stock: data['stock'] ?? '',
            categoryId: data['category_id'] ?? '',
            speciesId: speciesReference,
            cate: category,
            description: data['description'],
            spec: species,
          );
          seeds.add(specie);
        }
      }
    } catch (e) {
      print("Error getting species: $e");
    }
    return seeds;
  }

  @override
  Future<List<SpeciesModelUI>> getAllSpecies() async {
    List<SpeciesModelUI> species = [];
    try {
      QuerySnapshot querySnapshot =
          await _firestore.collection('species').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['icon']);
        var url = await ref.getDownloadURL();
        if (doc.exists) {
          DocumentReference cateid = doc['category_id'];
          // CategoryModelUI cate = await getCate(cateid);

          SpeciesModelUI specie = SpeciesModelUI(
            id: doc.id,
            name: doc['name'] ?? '',
            icon: url,
            categoryId: cateid.id,
          );
          species.add(specie);
        }
      }
    } catch (e) {
      print("Error getting species: $e");
    }
    return species;
  }

  Future<SpeciesModelUI> getSpeciesRe(DocumentReference speciesRef) async {
    DocumentSnapshot speciesDoc = await speciesRef.get();
    DocumentReference cateId = speciesDoc['category_id'];
    CategoryModelUI cate = await getCate(cateId);
    final speciesData = speciesDoc.data() as Map<String, dynamic>;
    return SpeciesModelUI(
      id: speciesDoc.id,
      icon: speciesData['icon'] ?? '',
      name: speciesData['name'] ?? '',
      categoryId: cate.id,
    );
  }

  Future<CategoryModelUI> getCate(DocumentReference speciesRef) async {
    DocumentSnapshot cateDoc = await speciesRef.get();
    final cateData = cateDoc.data() as Map<String, dynamic>;
    return CategoryModelUI(
      id: cateDoc.id,
      icon: cateData['icon'] ?? '',
      name: cateData['name'] ?? '',
    );
  }
  
  @override
  Future<List<ProductDto>> getAllAcces() async{
    List<ProductDto> plants = [];
    try {
      QuerySnapshot querySnapshot = await _firestore.collection('accessories').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['image']);
        var url = await ref.getDownloadURL();
        if (doc.exists) {
          Map<String, dynamic> data = doc.data() as Map<String, dynamic>;

          // Fetch the referenced category document
          DocumentReference categoryReference = data['category_id'];
          DocumentSnapshot categorySnapshot = await categoryReference.get();
          Map<String, dynamic> categoryData =
              categorySnapshot.data() as Map<String, dynamic>;

          DocumentReference speciesReference = data['species_id'];
          DocumentSnapshot speciesSnapshot = await speciesReference.get();
          Map<String, dynamic> speciesData =
              speciesSnapshot.data() as Map<String, dynamic>;

          CategoryDto category = CategoryDto(
            id: categorySnapshot.id,
            name: categoryData['name'] ?? '',
          );

          SpeciesDto species = SpeciesDto(
            id: speciesSnapshot.id,
            icon: speciesData['icon'] ?? '',
            name: speciesData['name'] ?? '',
            cateDoc: categoryReference,
            cateDto: category,
          );

          ProductDto specie = ProductDto(
            id: doc.id,
            about: data['about'] ?? '',
            size: data['size'] ?? '',
            care: data['care'] ?? '',
            description: data['description'] ?? '',
            image: url,
            light: data['light'] ?? '',
            name: data['name'] ?? '',
            price: data['price'] ?? '',
            stock: data['stock'] ?? '',
            incubate: data['incubate,'] ?? '',
            temperature: data['temperature'] ?? '',
            water: data['water'] ?? '',
            catergoryId: data['category_id'] ?? '',
            speciesId: speciesReference,
            cate: category,
            spec: species,
          );
          plants.add(specie);
        }
      }
    } catch (e) {
      print("Error getting species: $e");
    }
    return plants;
  }
  
  @override
  Future<List<ProductDto>> getAllSeed() async{
    List<ProductDto> plants = [];
    try {
      QuerySnapshot querySnapshot = await _firestore.collection('seed').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['image']);
        var url = await ref.getDownloadURL();
        if (doc.exists) {
          Map<String, dynamic> data = doc.data() as Map<String, dynamic>;

          // Fetch the referenced category document
          DocumentReference categoryReference = data['category_id'];
          DocumentSnapshot categorySnapshot = await categoryReference.get();
          Map<String, dynamic> categoryData =
              categorySnapshot.data() as Map<String, dynamic>;

          DocumentReference speciesReference = data['species_id'];
          DocumentSnapshot speciesSnapshot = await speciesReference.get();
          Map<String, dynamic> speciesData =
              speciesSnapshot.data() as Map<String, dynamic>;

          CategoryDto category = CategoryDto(
            id: categorySnapshot.id,
            name: categoryData['name'] ?? '',
          );

          SpeciesDto species = SpeciesDto(
            id: speciesSnapshot.id,
            icon: speciesData['icon'] ?? '',
            name: speciesData['name'] ?? '',
            cateDoc: categoryReference,
            cateDto: category,
          );

          ProductDto specie = ProductDto(
            id: doc.id,
            about: data['about'] ?? '',
            size: data['size'] ?? '',
            care: data['care'] ?? '',
            description: data['description'] ?? '',
            image: url,
            light: data['light'] ?? '',
            name: data['name'] ?? '',
            price: data['price'] ?? '',
            stock: data['stock'] ?? '',
            incubate: data['incubate,'] ?? '',
            temperature: data['temperature'] ?? '',
            water: data['water'] ?? '',
            catergoryId: data['category_id'] ?? '',
            speciesId: speciesReference,
            cate: category,
            spec: species,
          );
          plants.add(specie);
        }
      }
    } catch (e) {
      print("Error getting species: $e");
    }
    return plants;
  }
  

}
