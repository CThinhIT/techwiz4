import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:tech_wiz4/core/dto/model/accessories_dto.dart';
import 'package:tech_wiz4/core/dto/model/category_dto.dart';
import 'package:tech_wiz4/core/dto/model/plants_dto.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/dto/model/seed_dto.dart';
import 'package:tech_wiz4/core/dto/model/species_dto.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_product.dart';

class FirebaseProductService implements IFirebaseProductService {
  static final _firestore = FirebaseFirestore.instance;
  static final _fireStorage = FirebaseStorage.instance;
  List<ProductDto> products = [];
  List<String> addedProductIds = [];
  @override
  Future<List<ProductDto>> getProduct() async {
    try {
      QuerySnapshot querySnapshot = await _firestore.collection('plants').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['image']);
        var url = await ref.getDownloadURL();
        DocumentReference speciesid = doc['species_id'];
        SpeciesDto species = await getSpecies(speciesid);
        PlantsDto plant = PlantsDto(
          id: doc.id,
          about: doc['about'] ?? '',
          care: doc['care'] ?? '',
          description: doc['description'] ?? '',
          image: url,
          light: doc['light'] ?? '',
          name: doc['name'] ?? '',
          price: doc['price'] ?? '',
          size: doc['size'] ?? '',
          speciesId: speciesid,
          stock: doc['stock'] ?? '',
          temperature: doc['temperature'] ?? '',
          water: doc['water'] ?? '',
          catergoryId: doc['category_id'],
          spec: species,
          cate: species.cateDto,
        );

        ProductDto product = ProductDto(
          id: plant.id,
          about: plant.about,
          care: plant.care,
          description: plant.description,
          image: plant.image,
          light: plant.light,
          name: plant.name,
          price: plant.price,
          size: plant.size,
          speciesId: plant.speciesId,
          stock: plant.stock,
          temperature: plant.temperature,
          water: plant.water,
          incubate: '',
          catergoryId: plant.catergoryId,
          spec: plant.spec,
          cate: plant.cate,
        );
        products.add(product);
      }
    } catch (e) {
      print('Error getting products1: $e');
    }

    try {
      QuerySnapshot querySnapshot = await _firestore.collection('seed').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['image']);
        var url = await ref.getDownloadURL();
        DocumentReference speciesid = doc['species_id'];
        SpeciesDto species = await getSpecies(speciesid);
        SeedDto seed = SeedDto(
          id: doc.id,
          about: doc['about'] ?? '',
          image: url,
          name: doc['name'] ?? '',
          price: doc['price'] ?? '',
          stock: doc['stock'] ?? '',
          spec: species,
          cate: species.cateDto,
          categoryId: doc['category_id'],
          incubate: doc['incubate'],
          speciesId: speciesid,
          description: doc['description'],
        );

        ProductDto product = ProductDto(
          id: seed.id,
          about: seed.about,
          care: '',
          description: seed.description,
          image: seed.image,
          light: '',
          name: seed.name,
          price: seed.price,
          size: '',
          speciesId: seed.speciesId,
          stock: seed.stock,
          temperature: '',
          water: '',
          incubate: seed.incubate,
          catergoryId: seed.categoryId,
          spec: seed.spec,
          cate: seed.cate,
        );
        products.add(product);
      }
    } catch (e) {
      print('Error getting products1: $e');
    }
    try {
      QuerySnapshot querySnapshot =
          await _firestore.collection('accessories').get();
      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['image']);
        var url = await ref.getDownloadURL();
        DocumentReference speciesid = doc['species_id'];
        SpeciesDto species = await getSpecies(speciesid);
        AccessoriesDto access = AccessoriesDto(
          id: doc.id,
          about: doc['about'] ?? '',
          image: url,
          name: doc['name'] ?? '',
          price: doc['price'] ?? '',
          stock: doc['stock'] ?? '',
          categoryId: species.cateDto,
          description: doc['description'],
          cateid: doc['category_id'],
          spec: doc['species_id'],
          speciesDto: species,
        );

        ProductDto product = ProductDto(
          id: access.id,
          about: access.about,
          care: '',
          description: access.description,
          image: access.image,
          light: '',
          name: access.name,
          price: access.price,
          size: '',
          speciesId: speciesid,
          stock: access.stock,
          temperature: '',
          water: '',
          incubate: '',
          catergoryId: access.cateid,
          spec: access.speciesDto,
          cate: access.categoryId,
        );
        products.add(product);
      }
    } catch (e) {
      print('Error getting products3: $e');
    }
    return products;
  }

  @override
  Future<List<ProductDto>> getProductForHome() async {
    try {
      QuerySnapshot querySnapshot =
          await _firestore.collection('plants').limit(4).get();

      for (QueryDocumentSnapshot doc in querySnapshot.docs) {
        final ref = _fireStorage.ref().child(doc['image']);
        var url = await ref.getDownloadURL();
        DocumentReference speciesid = doc['species_id'];
        SpeciesDto species = await getSpecies(speciesid);
        PlantsDto plant = PlantsDto(
          id: doc.id,
          about: doc['about'] ?? '',
          care: doc['care'] ?? '',
          description: doc['description'] ?? '',
          image: url,
          light: doc['light'] ?? '',
          name: doc['name'] ?? '',
          price: doc['price'] ?? '',
          size: doc['size'] ?? '',
          speciesId: speciesid,
          stock: doc['stock'] ?? '',
          temperature: doc['temperature'] ?? '',
          water: doc['water'] ?? '',
          catergoryId: doc['category_id'],
          spec: species,
          cate: species.cateDto,
        );

        ProductDto product = ProductDto(
          id: plant.id,
          about: plant.about,
          care: plant.care,
          description: plant.description,
          image: plant.image,
          light: plant.light,
          name: plant.name,
          price: plant.price,
          size: plant.size,
          speciesId: plant.speciesId,
          stock: plant.stock,
          temperature: plant.temperature,
          water: plant.water,
          incubate: '',
          catergoryId: plant.catergoryId,
          spec: plant.spec,
          cate: plant.cate,
        );
        if (!addedProductIds.contains(doc.id)) {
          addedProductIds.add(doc.id);
          products.add(product);
        }
      }
    } catch (e) {
      print('Error getting products1: $e');
    }
    return products;
  }

  Future<SpeciesDto> getSpecies(DocumentReference speciesRef) async {
    DocumentSnapshot speciesDoc = await speciesRef.get();
    DocumentReference cateId = speciesDoc['category_id'];
    CategoryDto cate = await getCate(cateId);
    final speciesData = speciesDoc.data() as Map<String, dynamic>;
    return SpeciesDto(
      id: speciesDoc.id,
      name: speciesData['name'] ?? '',
      cateDoc: cateId,
      cateDto: cate,
      icon: speciesData['icon'],
    );
  }

  Future<CategoryDto> getCate(DocumentReference speciesRef) async {
    DocumentSnapshot cateDoc = await speciesRef.get();
    final cateData = cateDoc.data() as Map<String, dynamic>;
    return CategoryDto(
      id: cateDoc.id,
      name: cateData['name'] ?? '',
    );
  }
}
