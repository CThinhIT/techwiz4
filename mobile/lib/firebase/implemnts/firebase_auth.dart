import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:tech_wiz4/core/dto/model/users_dto.dart';
import 'package:tech_wiz4/core/global/router.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_auth.dart';

import '../../core/global/global_data.dart';
import '../../core/global/locator.dart';
import '../../core/utils/token_utils.dart';

class FirebaseAuthService implements IFirebaseAuthService {
  static final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  static final _firestore = FirebaseFirestore.instance;
  UsersDto? user;
  var data;
  @override
  Future<void> signIn(String email, String password) async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password)
          .then((value) async {
        data = value.user!.uid;

        await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: email, password: password)
            .then((value) async {
          user = await _getUser();
        });
      });

      if (user != null) {
        if (user!.status != 0) {
          TokenUtils.saveToken(data);
          locator<GlobalData>().currentUser = user;
          Get.toNamed(MyRouter.home);
        }
      } else {
        Get.toNamed(MyRouter.login);
      }
    } on FirebaseAuthException catch (e) {
      print(e);
    }
  }

  Future<void> setUp(String email, String password) async {}

  @override
  Future<void> signUp(
      String name, String email, String phone, String password) async {
    await _firebaseAuth
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((user) async {
      await _createUser(user.user!.uid, name, email, phone, password);
    }).catchError((onError) {
      Get.snackbar("SignUp failed!", onError);
    });
  }

  _createUser(String userId, String name, String email, String phone,
      String password) async {
    DocumentReference roleReference =
        _firestore.doc("roles/0OX6T54myQw2k7DadF3U");
    await _firestore.collection("users").doc(userId).set({
      "username": name,
      "email": email,
      "phone": phone,
      'password': password,
      "address": null,
      "age": null,
      "gender": null,
      "name": null,
      "status": 1,
      "role_id": roleReference,
    })

        // add({

        //   "username": name,
        //   "email": email,
        //   "phone": phone,
        //   'password': password,
        //   "address" : null,
        //   "age" : null,
        //   "gender" : null,
        //   "name" : null,
        //   "status" : 1,
        //   "role_id" : "0OX6T54myQw2k7DadF3U",
        // })

        .then((value) {
      Get.toNamed(MyRouter.login);
      Get.snackbar("SignUp success!", "");
    }).catchError((onError) {
      Get.snackbar("SignUp failed!", onError);
    });
  }

  Future<UsersDto> _getUser() async {
    final userSnapshot = await _firestore.collection('users').doc(data).get();
    return UsersDto(
      id: userSnapshot.id,
      address: userSnapshot['address'] ?? '',
      age: userSnapshot['age'] ?? 0,
      email: userSnapshot['email'],
      gender: userSnapshot['gender'] ?? true,
      name: userSnapshot['name'] ?? '',
      password: userSnapshot['password'],
      phone: userSnapshot['phone'],
      roleId: userSnapshot['role_id'],
      status: userSnapshot['status'],
      username: userSnapshot['username'],
    );
  }

  @override
  Future<bool> checkLogin() async {
    var token = await TokenUtils.getToken();
    if (token != 'null') {
      final userSnapshot =
          await _firestore.collection('users').doc(token).get();
      locator<GlobalData>().currentUser = UsersDto(
        id: userSnapshot.id,
        address: userSnapshot['address'] ?? '',
        age: userSnapshot['age'] ?? 0,
        email: userSnapshot['email'],
        gender: userSnapshot['gender'] ?? true,
        name: userSnapshot['name'] ?? '',
        password: userSnapshot['password'],
        phone: userSnapshot['phone'],
        roleId: userSnapshot['role_id'],
        status: userSnapshot['status'],
        username: userSnapshot['username'],
      );
      return true;
    }
    return false;
  }

  // @override
  // Future signInWithGoogle() async {
  //   // Trigger the authentication flow
  //   final googleUser = await GoogleSignIn().signIn();
  //   if (googleUser == null) return;
  //   // Obtain the auth details from the request
  //   final GoogleSignInAuthentication? googleAuth =
  //       await googleUser.authentication;

  //   // Create a new credential
  //   final credential = GoogleAuthProvider.credential(
  //     accessToken: googleAuth?.accessToken,
  //     idToken: googleAuth?.idToken,
  //   );

  //   // Once signed in, return the UserCredential
  //   UserCredential userCredential =
  //       await FirebaseAuth.instance.signInWithCredential(credential);

  //   var ref = await _firestore
  //       .collection("users")
  //       .where("userId", isEqualTo: userCredential.user?.uid)
  //       .get();
  //   var x = ref.docs.isEmpty;
  //   //     .then((a) async {
  //   //   if (a.docs.g ==null)
  //   //     await _createUser(
  //   //         userCredential.user!.uid,
  //   //         userCredential.user!.displayName!,
  //   //         userCredential.user!.phoneNumber!,
  //   //         "");
  //   // });
  //   if (ref.docs.isEmpty) {
  //     await _createUser(
  //         userCredential.user!.uid,
  //         userCredential.user!.displayName!,
  //         userCredential.user!.phoneNumber,
  //         "");
  //   }
  //   Get.toNamed(MyRouter.chatList);
  // }

  @override
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    locator<GlobalData>().currentUser = null;
    TokenUtils.removeToken();
    Get.toNamed(MyRouter.login);
  }

  @override
  // TODO: implement currentUser
  UsersDto? get currentUser => user;

  @override
  Future<void> deleteAccount() async {
    var token = await TokenUtils.getToken();
    DocumentReference docRef = _firestore.collection('users').doc(token);
    await docRef.update({
      'status': 0,
    });
    signOut();
  }

  // @override
  // Future<void> signInAnonymously() async {
  //   try {
  //     final userCredential = await FirebaseAuth.instance.signInAnonymously();
  //     print("Signed in with temporary account.");
  //     Get.toNamed(MyRouter.draw);
  //   } on FirebaseAuthException catch (e) {
  //     switch (e.code) {
  //       case "operation-not-allowed":
  //         print("Anonymous auth hasn't been enabled for this project.");
  //         break;
  //       default:
  //         print("Unknown error.");
  //     }
  //   }
  // }
}
