import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tech_wiz4/core/dto/model/billing_dto.dart';
import 'package:tech_wiz4/core/dto/model/order_detail_dto.dart';
import 'package:tech_wiz4/core/dto/model/order_dto.dart';
import 'package:tech_wiz4/core/dto/model/product_dto.dart';
import 'package:tech_wiz4/core/model_ui/basket_model_ui.dart';
import 'package:tech_wiz4/firebase/interfaces/ifirebase_order.dart';

import '../../core/utils/token_utils.dart';

class FirebaseOrderService implements IFirebaseOrderService {
  static final _firestore = FirebaseFirestore.instance;

  @override
  Future<List<OrderDto>> getOrder() async {
    List<OrderDto> orders = [];
    var token = await TokenUtils.getToken();
    QuerySnapshot querySnapshot = await _firestore
        .collection('order')
        .where('user_id', isEqualTo: _firestore.doc('users/$token'))
        .get();
    for (QueryDocumentSnapshot doc in querySnapshot.docs) {
      OrderDto order = OrderDto(
        id: doc.id,
        date: doc['date'],
        status: doc['status'],
        userId: doc['user_id'],
      );
      orders.add(order);
    }
    return orders;
  }

  @override
  Future<List<OrderDetailDto>> getOrderDetail(String id) async {
    List<OrderDetailDto> ordersDetail = [];
    QuerySnapshot querySnapshot = await _firestore
        .collection('order_detail')
        .where('order_id', isEqualTo: _firestore.doc('order/$id'))
        .get();

    for (QueryDocumentSnapshot doc in querySnapshot.docs) {
      OrderDetailDto order = OrderDetailDto(
        id: doc.id,
        price: doc['price'],
        productId: doc['product_id'],
        orderId: doc['order_id'],
        quantity: doc['quantity'],
      );
      ordersDetail.add(order);
    }
    return ordersDetail;
  }

  @override
  Future<void> createOrder(
      List<BasketModelUI> basket, num amount, bool payment, int status) async {
    var token = await TokenUtils.getToken();
    DocumentReference docRef = _firestore.collection('users').doc("$token");

    DocumentReference orderRef = await _firestore.collection('order').add({
      'date': DateTime.now(),
      'status': 1,
      'user_id': docRef,
    });
    DocumentReference billRef = await _firestore.collection('billing').add({
      'bill_amount': amount,
      'method_payment': payment,
      'order_id': orderRef,
      'status': status
    });

    for (var item in basket) {
      var checkPlant =
          await _firestore.collection('plants').doc(item.productId.id).get();
      var checkAcces = await _firestore
          .collection('accessories')
          .doc(item.productId.id)
          .get();
      var checkSeed =
          await _firestore.collection('seed').doc(item.productId.id).get();
      DocumentReference plants =
          _firestore.collection('plants').doc(item.productId.id);
      DocumentReference acces =
          _firestore.collection('accessories').doc(item.productId.id);
      DocumentReference seed =
          _firestore.collection('seed').doc(item.productId.id);
      Map<String, dynamic> orderDetail = {};
      if (checkPlant.exists) {
        orderDetail = {
          'product_id': plants,
          'price': item.price,
          'quantity': item.quantity,
          'order_id': orderRef,
        };
      } else if (checkAcces.exists) {
        orderDetail = {
          'product_id': acces,
          'price': item.price,
          'quantity': item.quantity,
          'order_id': orderRef,
        };
      } else if (checkSeed.exists) {
        orderDetail = {
          'product_id': seed,
          'price': item.price,
          'quantity': item.quantity,
          'order_id': orderRef,
        };
      }

      await FirebaseFirestore.instance
          .collection('order_detail')
          .add(orderDetail);
    }
  }

  // @override
  // Future<void> checkOut(List<OrderDetailDto> ordersList) async {
  //   // List<OrderDetailDto> ordersList = [];
  //   for (var item in ordersList) {
  //     final items = FirebaseFirestore.instance.collection('order_detail').doc();
  //     await items.set(item);
  //   }
  // }

  // @override
  // Future<void> orderProducts(List<OrderDetailDto> orders) async {
  //   for (var item in orders) {
  //     final orderRef =
  //         FirebaseFirestore.instance.collection('order_detail').doc();
  //     await orderRef.set(item);
  //   }
  // }
}
