/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package techwiz.example.api.resource;

import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import techwiz.example.api.models.Accessories;
import techwiz.example.api.models.AccessoriesRequest;
import techwiz.example.api.models.Category;
import techwiz.example.api.models.Roles;
import techwiz.example.api.models.UserRequest;
import techwiz.example.api.models.Users;
import techwiz.example.api.service.IAccessories;

/**
 *
 * @author admin
 */
@RestController
@RequestMapping("/api")
public class AccessoriesResource {
    @Autowired
    IAccessories service;
    
    @GetMapping("/getAccessories")
    public List<Accessories> getListAccessories() throws InterruptedException, ExecutionException {
        return service.getAccessories();
    }
    
    @GetMapping("/getAccessory")
    public Accessories getAccessories(@RequestParam("documentId") String documentId) throws InterruptedException, ExecutionException {
        return service.getAccessories(documentId);
    }
    
    @GetMapping("/getCategory")
    public Category getCategory(@RequestParam("documentId") String documentId) throws InterruptedException, ExecutionException {
        return service.getCategory(documentId);
    }
    
    @PostMapping("/createAccessories")
    public AccessoriesRequest createAccessories(@RequestBody AccessoriesRequest accessories) {
        AccessoriesRequest createdAccessories = service.createAccessory(accessories); 
        return createdAccessories;
    }
    
     @PutMapping("/updateAccessories/{documentId}")
    public AccessoriesRequest updateAccessories(@PathVariable String documentId, @RequestBody AccessoriesRequest updatedAccessories) {
        AccessoriesRequest updatedAccessoriesData = service.updateAccessory(documentId, updatedAccessories);
        
        return updatedAccessories;
    }

    @DeleteMapping("/deleteAccessories/{documentId}")
    public boolean deleteAccessories(@PathVariable String documentId) {
        boolean deleted = service.deleteAccessory(documentId);
        return deleted;
    }
}
