package techwiz.example.api.resource;

import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import techwiz.example.api.models.Roles;
import techwiz.example.api.models.UserRequest;
import techwiz.example.api.models.Users;
import techwiz.example.api.service.ITestService;


@RestController
@RequestMapping("/api")
public class TestResource {
    
    @Autowired
    ITestService service;
    
    @GetMapping("/getUsers")
    public List<Users> getUsers() throws InterruptedException, ExecutionException {
        return service.getUsers();
    }
    
    @GetMapping("/getUser")
    public Users getUser(@RequestParam("documentId") String documentId) throws InterruptedException, ExecutionException {
        return service.getUser(documentId);
    }
    
    @GetMapping("/getRole")
    public Roles getRole(@RequestParam("documentId") String documentId) throws InterruptedException, ExecutionException {
        return service.getRole(documentId);
    }
    
    @PostMapping("/createUser")
    public UserRequest createUser(@RequestBody UserRequest user) {
        UserRequest createdUser = service.create(user); 
        return createdUser;
    }
    
     @PutMapping("update/{documentId}")
    public UserRequest updateUser(@PathVariable String documentId, @RequestBody UserRequest updatedUser) {
        UserRequest updatedUserData = service.update(documentId, updatedUser);
        
        return updatedUserData;
    }

    @DeleteMapping("delete/{documentId}")
    public boolean deleteUser(@PathVariable String documentId) {
        boolean deleted = service.delete(documentId);
        return deleted;
    }
    
}
