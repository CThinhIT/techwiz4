/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/Service.java to edit this template
 */
package techwiz.example.api.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.api.models.Accessories;
import techwiz.example.api.models.AccessoriesRequest;
import techwiz.example.api.models.Category;
import techwiz.example.api.models.Roles;
import techwiz.example.api.models.Users;

/**
 *
 * @author admin
 */
@Service
public class AccessoriesService implements IAccessories {

    @Override
    public List<Accessories> getAccessories() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "users"
        CollectionReference accessoriesCollection = firestore.collection("accessories");

        // Tạo danh sách chứa người dùng
        List<Accessories> accessoriesList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách người dùng
            ApiFuture<QuerySnapshot> querySnapshot = accessoriesCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng người dùng từ dữ liệu tài liệu
                Accessories accessories = document.toObject(Accessories.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                String documentId = document.getId();
                accessories.setDocumentId(documentId);

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference categoryReference = document.get("category_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò
                if (categoryReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot categorySnapshot = categoryReference.get().get();

                    // Nếu vai trò tồn tại
                    if (categorySnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Category cate = categorySnapshot.toObject(Category.class);
                        cate.setDocumentId(categorySnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        accessories.setCategory_id(categoryReference);
                        accessories.setCategory(cate);
                    }
                }

                // Thêm người dùng vào danh sách
                accessoriesList.add(accessories);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách người dùng
        return accessoriesList;

    }

    @Override
    public Accessories getAccessories(String documentId) {
// Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu người dùng với documentId tương ứng
        DocumentReference accessoriesRef = firestore.collection("accessories").document(documentId);

        try {
            // Lấy tài liệu người dùng từ Firestore
            DocumentSnapshot accessoriesSnapshot = accessoriesRef.get().get();

            // Nếu tài liệu người dùng tồn tại
            if (accessoriesSnapshot.exists()) {
                // Tạo đối tượng người dùng từ dữ liệu tài liệu
                Accessories accessories = accessoriesSnapshot.toObject(Accessories.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                accessories.setDocumentId(accessoriesSnapshot.getId());

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference categoryReference = accessoriesSnapshot.get("category_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò
                if (categoryReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot categorySnapshot = categoryReference.get().get();

                    // Nếu vai trò tồn tại
                    if (categorySnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Category cate = categorySnapshot.toObject(Category.class);
                        cate.setDocumentId(categorySnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        accessories.setCategory_id(categoryReference);
                        accessories.setCategory(cate);
                    }
                }

                // Trả về đối tượng người dùng
                return accessories;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy người dùng với documentId
        return null;
    }

    @Override
    public AccessoriesRequest createAccessory(AccessoriesRequest accessories) {
        Firestore firestore = FirestoreClient.getFirestore();

        try {
            // Lấy tham chiếu đến collection "users"
            CollectionReference usersCollection = firestore.collection("accessories");

            // Cách set giá trị cho kiểu Reference
            // Tạo một DocumentReference dựa trên document_id
            // DocumentReference roleReference = firestore.collection("roles").document("0OX6T54myQw2k7DadF3U");
            // Gán DocumentReference cho trường role_id trong đối tượng User
            // user.setRole_id(roleReference);
            // Thêm người dùng mới vào Firestore
            ApiFuture<DocumentReference> documentReferenceApiFuture = usersCollection.add(accessories);

            // Lấy DocumentReference của người dùng vừa được thêm
            DocumentReference addedAccessoriesRef = documentReferenceApiFuture.get();

            // Trích xuất role_id từ đối tượng user và gán vào trường role_id
//            DocumentReference roleReference = user.getRole_id();
//            if (roleReference != null) {
//                System.out.println("Before role_id update: " + roleReference.getPath()); // Debug log
//                user.setRole_id(firestore.document(roleReference.getPath())); // Lưu đường dẫn của DocumentReference
//                System.out.println("After role_id update: " + user.getRole_id().getPath()); // Debug log
//            }
            return accessories;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public AccessoriesRequest updateAccessory(String documentId, AccessoriesRequest updatedAccessories) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference accessoriesRef = firestore.collection("accessories").document(documentId);

        try {
            DocumentSnapshot accessoriesSnapshot = accessoriesRef.get().get();

            if (accessoriesSnapshot.exists()) {
                // Cập nhật thông tin người dùng
                accessoriesRef.set(updatedAccessories, SetOptions.merge()).get();

                return updatedAccessories;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean deleteAccessory(String documentId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference accessoriesRef = firestore.collection("accessories").document(documentId);

        try {
            DocumentSnapshot accessoriesSnapshot = accessoriesRef.get().get();

            if (accessoriesSnapshot.exists()) {
                // Xóa người dùng khỏi Firestore
                accessoriesRef.delete().get();

                return true;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Category getCategory(String documentId) {
// Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu vai trò với documentId tương ứng
        DocumentReference categoryRef = firestore.collection("accessories").document(documentId);

        try {
            // Lấy tài liệu vai trò từ Firestore
            DocumentSnapshot categorySnapshot = categoryRef.get().get();

            // Nếu tài liệu vai trò tồn tại
            if (categorySnapshot.exists()) {
                // Tạo đối tượng vai trò từ dữ liệu tài liệu
                Category cate = categorySnapshot.toObject(Category.class);

                // Gán giá trị documentId cho đối tượng vai trò
                cate.setDocumentId(categorySnapshot.getId());

                // Trả về đối tượng vai trò
                return cate;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy vai trò với documentId
        return null;
    }

}
