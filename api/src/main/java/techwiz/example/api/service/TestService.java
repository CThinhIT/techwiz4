package techwiz.example.api.service;

// Import các thư viện cần thiết
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.api.models.Roles;
import techwiz.example.api.models.UserRequest;
import techwiz.example.api.models.Users;

// Đánh dấu đây là một Spring Service
@Service
public class TestService implements ITestService {

    // Ghi đè phương thức để lấy danh sách người dùng
    @Override
    public List<Users> getUsers() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "users"
        CollectionReference usersCollection = firestore.collection("users");

        // Tạo danh sách chứa người dùng
        List<Users> userList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách người dùng
            ApiFuture<QuerySnapshot> querySnapshot = usersCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng người dùng từ dữ liệu tài liệu
                Users user = document.toObject(Users.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                String documentId = document.getId();
                user.setDocumentId(documentId);

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference roleReference = document.get("role_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò
                if (roleReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot roleSnapshot = roleReference.get().get();

                    // Nếu vai trò tồn tại
                    if (roleSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Roles role = roleSnapshot.toObject(Roles.class);
                        role.setDocumentId(roleSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        user.setRole_id(roleReference);
                        user.setRole(role);
                    }
                }

                // Thêm người dùng vào danh sách
                userList.add(user);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách người dùng
        return userList;
    }

    // Ghi đè phương thức để lấy thông tin một người dùng dựa trên documentId
    @Override
    public Users getUser(String documentId) {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu người dùng với documentId tương ứng
        DocumentReference userRef = firestore.collection("users").document(documentId);

        try {
            // Lấy tài liệu người dùng từ Firestore
            DocumentSnapshot userSnapshot = userRef.get().get();

            // Nếu tài liệu người dùng tồn tại
            if (userSnapshot.exists()) {
                // Tạo đối tượng người dùng từ dữ liệu tài liệu
                Users user = userSnapshot.toObject(Users.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                user.setDocumentId(userSnapshot.getId());

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference roleReference = userSnapshot.get("role_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò
                if (roleReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot roleSnapshot = roleReference.get().get();

                    // Nếu vai trò tồn tại
                    if (roleSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Roles role = roleSnapshot.toObject(Roles.class);
                        role.setDocumentId(roleSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        user.setRole_id(roleReference);
                        user.setRole(role);
                    }
                }

                // Trả về đối tượng người dùng
                return user;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy người dùng với documentId
        return null;
    }

    // Ghi đè phương thức để lấy thông tin một vai trò dựa trên documentId
    @Override
    public Roles getRole(String documentId) {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu vai trò với documentId tương ứng
        DocumentReference roleRef = firestore.collection("roles").document(documentId);

        try {
            // Lấy tài liệu vai trò từ Firestore
            DocumentSnapshot roleSnapshot = roleRef.get().get();

            // Nếu tài liệu vai trò tồn tại
            if (roleSnapshot.exists()) {
                // Tạo đối tượng vai trò từ dữ liệu tài liệu
                Roles role = roleSnapshot.toObject(Roles.class);

                // Gán giá trị documentId cho đối tượng vai trò
                role.setDocumentId(roleSnapshot.getId());

                // Trả về đối tượng vai trò
                return role;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy vai trò với documentId
        return null;
    }

    @Override
    public UserRequest create(UserRequest user) {
        Firestore firestore = FirestoreClient.getFirestore();

        try {
            // Lấy tham chiếu đến collection "users"
            CollectionReference usersCollection = firestore.collection("users");

            // Cách set giá trị cho kiểu Reference
            
            // Tạo một DocumentReference dựa trên document_id
            // DocumentReference roleReference = firestore.collection("roles").document("0OX6T54myQw2k7DadF3U");
            // Gán DocumentReference cho trường role_id trong đối tượng User
            // user.setRole_id(roleReference);

            // Thêm người dùng mới vào Firestore
            ApiFuture<DocumentReference> documentReferenceApiFuture = usersCollection.add(user);

            // Lấy DocumentReference của người dùng vừa được thêm
            DocumentReference addedUserRef = documentReferenceApiFuture.get();

            // Trích xuất role_id từ đối tượng user và gán vào trường role_id
//            DocumentReference roleReference = user.getRole_id();
//            if (roleReference != null) {
//                System.out.println("Before role_id update: " + roleReference.getPath()); // Debug log
//                user.setRole_id(firestore.document(roleReference.getPath())); // Lưu đường dẫn của DocumentReference
//                System.out.println("After role_id update: " + user.getRole_id().getPath()); // Debug log
//            }
            return user;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public UserRequest update(String documentId, UserRequest updatedUser) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference userRef = firestore.collection("users").document(documentId);

        try {
            DocumentSnapshot userSnapshot = userRef.get().get();

            if (userSnapshot.exists()) {
                // Cập nhật thông tin người dùng
                userRef.set(updatedUser, SetOptions.merge()).get();

                return updatedUser;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean delete(String documentId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference userRef = firestore.collection("users").document(documentId);

        try {
            DocumentSnapshot userSnapshot = userRef.get().get();

            if (userSnapshot.exists()) {
                // Xóa người dùng khỏi Firestore
                userRef.delete().get();

                return true;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

}
