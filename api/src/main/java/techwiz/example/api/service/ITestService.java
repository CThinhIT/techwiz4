package techwiz.example.api.service;

import java.util.List;
import techwiz.example.api.models.Roles;
import techwiz.example.api.models.UserRequest;
import techwiz.example.api.models.Users;


public interface ITestService {
    List<Users> getUsers();
    Users getUser(String documentId);
    UserRequest create(UserRequest user);
    UserRequest update(String documentId, UserRequest updatedUser);
    boolean delete(String documentId);
    
    Roles getRole(String documentId);
}
