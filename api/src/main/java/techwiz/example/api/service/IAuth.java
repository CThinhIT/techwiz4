package techwiz.example.api.service;

import techwiz.example.api.models.Users;

public interface IAuth {
    Users registerUser(Users user);
    Users loginUser(Users user);
}
