/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package techwiz.example.api.service;

import java.util.List;
import techwiz.example.api.models.Accessories;
import techwiz.example.api.models.AccessoriesRequest;
import techwiz.example.api.models.Category;

/**
 *
 * @author admin
 */
public interface IAccessories {
    List<Accessories> getAccessories();
    Accessories getAccessories(String documentId);
    AccessoriesRequest createAccessory(AccessoriesRequest accessories);
    AccessoriesRequest updateAccessory(String documentId, AccessoriesRequest updatedAccessories);
    boolean deleteAccessory(String documentId);
    Category getCategory(String documentId);
}
