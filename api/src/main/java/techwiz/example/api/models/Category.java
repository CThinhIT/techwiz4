/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package techwiz.example.api.models;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author admin
 */
@Setter
@Getter
public class Category {
    private String DocumentId;
    private String name;
}
