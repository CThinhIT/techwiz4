package techwiz.example.api.models;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import java.io.IOException;

public class DocumentReferenceDeserializer extends JsonDeserializer<DocumentReference> {

    private final Firestore firestore;

    public DocumentReferenceDeserializer(Firestore firestore) {
        this.firestore = firestore;
    }

    @Override
    public DocumentReference deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String referencePath = jsonParser.getValueAsString();
        return firestore.document(referencePath);
    }
}
