package techwiz.example.admin.controller;

import jakarta.servlet.http.HttpSession;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import techwiz.example.admin.enums.CategoryRef;
import techwiz.example.admin.enums.Role;
import techwiz.example.admin.models.Seed;
import techwiz.example.admin.models.SeedRequest;
import techwiz.example.admin.models.Users;
import techwiz.example.admin.service.FirebaseStorageService;
import techwiz.example.admin.service.ISeed;

@Controller
@RequestMapping("/admin")
public class SeedController {

    @Autowired
    ISeed service;

    @Autowired
    private FirebaseStorageService storageService;

    @RequestMapping("/seeds")
    public String seeds(Model model, HttpSession session) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        List<Seed> seed = service.getAllSeed();
        for (Seed seed1 : seed) {
            String imageUrl = storageService.getFirebaseImageUrl(seed1.getImage());
            seed1.setImage(imageUrl);
        }
        model.addAttribute("seeds", seed);
        return "admin/seed/seeds";
    }

    @RequestMapping("/create-seed")
    public String createSeed(Model model, HttpSession session) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        model.addAttribute("specieses", service.getSpecies());
        return "admin/seed/create-seed";
    }

    @RequestMapping(value = "/create-seed", method = RequestMethod.POST)
    public String createSeed(Model model, HttpSession session, @ModelAttribute("SeedRequest") SeedRequest seedRequest, @RequestParam("speciesId") String speciesId, @RequestParam("images") MultipartFile imageFile, RedirectAttributes redirectAttributes) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        // Lấy ảnh
        if (!imageFile.isEmpty()) {
            try {
                // Lấy tên file ảnh
                String imageName = imageFile.getOriginalFilename();

                // Tải ảnh lên Firebase Storage
                String imageUrl = storageService.uploadImage(imageFile, imageName);

                // Set đường dẫn ảnh cho đối tượng PlantRequest
                seedRequest.setImage("images/" + imageName);

            } catch (Exception e) {
                // Xử lý lỗi nếu có (nếu cần)
                model.addAttribute("message", "An error occurred while processing the image.");
                // return hoặc throw exception tùy theo trường hợp
            }
        }

        // set giá trị cho plant
        String categoryId = CategoryRef.SEED.getValue();

        // Tạo plant
        SeedRequest createdSeed = service.createSeed(seedRequest, categoryId, speciesId);
        if (createdSeed == null) {
            redirectAttributes.addFlashAttribute("message", "Create a new seed failed");
            return "redirect:/admin/create-seed";
        }

        redirectAttributes.addFlashAttribute("message", "Create a successful seed!");

        return "redirect:/admin/seeds";
    }

    @RequestMapping("/edit-seed/{seedId}")
    public String editSeed(Model model, HttpSession session, @PathVariable("seedId") String seedId) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        Seed seed = service.getSeedById(seedId);
        // Lấy URL của ảnh từ Firebase Storage và đặt vào thuộc tính image của đối tượng plant
        String imageUrl = storageService.getFirebaseImageUrl(seed.getImage());

        model.addAttribute("imageUrl", imageUrl);
        model.addAttribute("seed", seed);
        model.addAttribute("specieses", service.getSpecies());
        return "admin/seed/edit-seed";
    }
    
    
    @RequestMapping(value = "/edit-seed", method = RequestMethod.POST)
    public String EditSeed(Model model, HttpSession session, @ModelAttribute("SeedRequest") SeedRequest seedRequest, @RequestParam("speciesId") String speciesId, @RequestParam("id") String seedId, @RequestParam("images") MultipartFile imageFile, RedirectAttributes redirectAttributes){
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        // Check trùng
        // Lấy ảnh
        if (!imageFile.isEmpty()) {
            try {
                // Lấy tên file ảnh
                String imageName = imageFile.getOriginalFilename();

                // Tải ảnh lên Firebase Storage
                String imageUrl = storageService.uploadImage(imageFile, imageName);

                // Set đường dẫn ảnh cho đối tượng PlantRequest
                seedRequest.setImage("images/" + imageName);

            } catch (Exception e) {
                // Xử lý lỗi nếu có (nếu cần)
                model.addAttribute("message", "An error occurred while processing the image.");
                // return hoặc throw exception tùy theo trường hợp
            }
        }

        // Lấy ảnh
        if (!seedRequest.getImage().isEmpty()) {
            seedRequest.setImage(seedRequest.getImage());
        }

        // set giá trị cho plant
        String categoryId = CategoryRef.SEED.getValue();


        // Cập nhật plant
        SeedRequest updatedSeed = service.updatedSeed(seedId, seedRequest, categoryId, speciesId);
        if (updatedSeed == null) {
            redirectAttributes.addFlashAttribute("message", "Updated seed failed");
            return "redirect:/admin/update-seed";
        }

        redirectAttributes.addFlashAttribute("message", "Updated a successful seed!");
        return "redirect:/admin/seeds";
    }
    
    
    @RequestMapping("/delete-seed/{seedId}")
    public String deleteSeed(Model model, HttpSession session, @PathVariable("seedId") String seedId, RedirectAttributes redirectAttributes) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        boolean deletedSeed = service.deletedSeed(seedId);

        if (deletedSeed == false) {
            redirectAttributes.addFlashAttribute("errorMessage", "Seed deletion failed!");
            return "redirect:/admin/seeds";
        }

        redirectAttributes.addFlashAttribute("message", "Deleted a successful seed!");
        return "redirect:/admin/seeds";
    }

    @RequestMapping("/detail-seed/{seedId}")
    public String detailPlant(Model model, HttpSession session, @PathVariable("seedId") String seedId) {
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        Seed seed = service.getSeedById(seedId);

        // Lấy URL của ảnh từ Firebase Storage và đặt vào thuộc tính image của đối tượng plant
        String imageUrl = storageService.getFirebaseImageUrl(seed.getImage());

        if (imageUrl != null) {
            seed.setImage(imageUrl);
        }

        model.addAttribute("imageUrl", imageUrl);
        model.addAttribute("seed", seed);

        return "admin/seed/detail-seed";
    }
    

}
