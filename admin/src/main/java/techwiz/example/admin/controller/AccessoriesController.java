package techwiz.example.admin.controller;

import jakarta.servlet.http.HttpSession;
import java.util.Base64;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import techwiz.example.admin.enums.CategoryRef;
import techwiz.example.admin.enums.Role;
import techwiz.example.admin.models.Accessories;
import techwiz.example.admin.models.AccessoriesRequest;
import techwiz.example.admin.models.Users;
import techwiz.example.admin.service.FirebaseStorageService;
import techwiz.example.admin.service.IAccessories;

@Controller
@RequestMapping("/admin")
public class AccessoriesController {

    @Autowired
    IAccessories service;

    @Autowired
    private FirebaseStorageService storageService;

    @RequestMapping("/accessories")
    public String listAccessories(Model model, HttpSession session) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        List<Accessories> list = service.getAccessories();
        for (Accessories accessories : list) {
            String imageUrl = storageService.getFirebaseImageUrl(accessories.getImage());
            accessories.setImage(imageUrl);
        }
        model.addAttribute("list", list);

        return "/admin/accessories/listAccessories";
    }

    @RequestMapping("/create/accessories")
    public String createAccessories(Model model, RedirectAttributes redirectAttributes, HttpSession session) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        model.addAttribute("specieses", service.getSpecies());
        return "/admin/accessories/createAccessories";
    }

    @RequestMapping(value = "/add/accessories", method = RequestMethod.POST)
    public String addAccessories(RedirectAttributes redirectAttributes, Model model, @RequestParam("speciesId") String speciesId, @RequestParam String name, @RequestParam String price, @RequestParam String stock, @RequestParam String introduce, @RequestParam String description, @RequestParam("images") MultipartFile imageFile, HttpSession session) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        // Xử lý ảnh
        String imageName = null;
        if (!imageFile.isEmpty()) {
            try {
                // Lấy tên file ảnh
                imageName = imageFile.getOriginalFilename();
                // Tải ảnh lên Firebase Storage
                String imageUrl = storageService.uploadImage(imageFile, imageName);

            } catch (Exception e) {
                // Xử lý lỗi nếu có (nếu cần)
                model.addAttribute("message", "An error occurred while processing the image.");
                // return hoặc throw exception tùy theo trường hợp
            }
        }

        AccessoriesRequest acc = new AccessoriesRequest();
        acc.setName(name);
        Double pri = Double.parseDouble(price);
        acc.setPrice(pri);
        int sto = Integer.parseInt(stock);
        acc.setStock(sto);
        // acc.setUsage_instruction(usage);
        acc.setDescription(description);
        acc.setAbout(introduce);
        
        acc.setImage("images/" + imageName);
        
        System.out.println(imageName);
        
        String categoryId = CategoryRef.ACCESSORIES.getValue();
        service.createAccessory(acc, categoryId, speciesId);
        
        redirectAttributes.addFlashAttribute("message", "Create Accessories Successful");
        return "redirect:/admin/accessories";
    }

    @RequestMapping("/detail/accessories/{accessoriesId}")
    public String DetailAccessories(Model model, @PathVariable String accessoriesId, HttpSession session) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        Accessories accessories = service.getAccessories(accessoriesId);
        String imageUrl = storageService.getFirebaseImageUrl(accessories.getImage());
        model.addAttribute("imageUrl", imageUrl);
        model.addAttribute("a", accessories);
        return "/admin/accessories/detailAccessories";
    }

    @RequestMapping("/accessories/update/{accessoriesId}")
    public String updateAccessories(Model model, @PathVariable String accessoriesId, HttpSession session) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        Accessories accessories = service.getAccessories(accessoriesId);
        String imageUrl = storageService.getFirebaseImageUrl(accessories.getImage());
        model.addAttribute("imageUrl", imageUrl);
        model.addAttribute("a", accessories);
        model.addAttribute("specieses", service.getSpecies());
        return "/admin/accessories/editAccessories";
    }

    @RequestMapping(value = "/edit/accessories", method = RequestMethod.POST)
    public String editAccessories(Model model, RedirectAttributes redirectAttributes, @RequestParam String documentId, @RequestParam String name, @RequestParam("speciesId") String speciesId, @RequestParam String price, @RequestParam String stock, @RequestParam String introduce, @RequestParam String description, @RequestParam("images") MultipartFile imageFile, HttpSession session) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        // Xử lý ảnh
        String imageName = null;
        if (!imageFile.isEmpty()) {
            try {
                // Lấy tên file ảnh
                imageName = imageFile.getOriginalFilename();
                // Tải ảnh lên Firebase Storage
                String imageUrl = storageService.uploadImage(imageFile, imageName);

            } catch (Exception e) {
                // Xử lý lỗi nếu có (nếu cần)
                model.addAttribute("message", "An error occurred while processing the image.");
                // return hoặc throw exception tùy theo trường hợp
            }
        }
        

        AccessoriesRequest acc = new AccessoriesRequest();
        acc.setName(name);
        Double pri = Double.parseDouble(price);
        acc.setPrice(pri);
        int sto = Integer.parseInt(stock);
        acc.setStock(sto);
        // acc.setUsage_instruction(usage);
        acc.setDescription(description);
        acc.setAbout(introduce);
        
       String categoryId = CategoryRef.ACCESSORIES.getValue();
       
        Accessories a = service.getAccessories(documentId);
        if (a.getImage().equals(imageFile) || imageFile.isEmpty()) {
            acc.setImage(a.getImage());
        } else {
            acc.setImage("images/" + imageName);
        }
        service.updateAccessory(documentId, acc, categoryId, speciesId);
        redirectAttributes.addFlashAttribute("message", "Update Accessories Successful");
        return "redirect:/admin/accessories";
    }

    @RequestMapping("/accessories/delete/{accessoriesId}")
    public String removeAccessories(Model model, @PathVariable String accessoriesId, HttpSession session, RedirectAttributes redirectAttributes) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        service.deleteAccessory(accessoriesId);
        redirectAttributes.addFlashAttribute("message", "Delete Accessories Successful");
        return "redirect:/admin/accessories";
    }

    private String convertImageToBase64(byte[] imageBytes) {
        // Chuyển đổi mảng byte của hình ảnh thành mã Base64
        return Base64.getEncoder().encodeToString(imageBytes);
    }
}
