package techwiz.example.admin.controller;

import jakarta.servlet.http.HttpSession;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import techwiz.example.admin.enums.Role;
import techwiz.example.admin.models.Users;
import techwiz.example.admin.service.IAccount;


@Controller
@RequestMapping("/admin")
public class AccountController {
    @Autowired
    IAccount service;
    
    @RequestMapping("/account")
    public String listAccount(Model model, HttpSession session) {     
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        List<Users> list = service.getListUser();
        model.addAttribute("list", list);
        return "admin/account";
    }
    
    @RequestMapping("/detail/{userId}")
    public String accountDetail(Model model, @PathVariable String userId, HttpSession session) {
        
        Users user = (Users) session.getAttribute("user");     
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        Users users = service.getDetailUser(userId);
        model.addAttribute("user", users);
        return "admin/accountDetail";
    }
}
