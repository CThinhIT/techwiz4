package techwiz.example.admin.controller;

import jakarta.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import techwiz.example.admin.enums.BillStatus;
import techwiz.example.admin.enums.OrderStatus;
import techwiz.example.admin.enums.Role;
import techwiz.example.admin.models.BillingRequest;
import techwiz.example.admin.models.Order;
import techwiz.example.admin.models.OrderDetail;
import techwiz.example.admin.models.OrderRequest;
import techwiz.example.admin.models.Users;
import techwiz.example.admin.service.AccessoriesService;
import techwiz.example.admin.service.AccountService;
import techwiz.example.admin.service.OrderService;
import techwiz.example.admin.service.PlantService;
import techwiz.example.admin.service.SeedService;

@Controller
@RequestMapping("/admin")
public class OrderController {

    private final AccountService accountService;
    private final AccessoriesService accessoriesService;
    private final PlantService plantService;
    private final SeedService seedService;
    private final OrderService orderService;

    @Autowired
    public OrderController(AccountService accountService, AccessoriesService accessoriesService, PlantService plantService, SeedService seedService, OrderService orderService) {
        this.accountService = accountService;
        this.accessoriesService = accessoriesService;
        this.plantService = plantService;
        this.seedService = seedService;
        this.orderService = orderService;
    }

    ;
    
    @RequestMapping("/orders")
    public String orders(Model model, HttpSession session) {
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        List<Order> orders = orderService.getAllOrder();

        model.addAttribute("orders", orders);
        return "admin/order/orders";
    }

    @RequestMapping("/detail-order/{orderId}")
    public String detailOrders(Model model, @PathVariable("orderId") String orderId, HttpSession session) {
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        List<OrderDetail> orderDetail = orderService.getOrderDetailByOrderId(orderId);
        model.addAttribute("orderDetails", orderDetail);

        // Tính tổng giá
        long totalPrice = 0;
        for (OrderDetail detail : orderDetail) {
            totalPrice += detail.getPrice() * detail.getQuantity();
        }
        // Tính quantity
        int totalQuantity = 0;  // Khởi tạo biến để lưu tổng quantity

        for (OrderDetail detail : orderDetail) {
            totalQuantity += detail.getQuantity();  // Cộng dồn quantity của từng chi tiết vào tổng
        }
        // Tính số lượng product
        int countProduct = orderDetail.size();

        model.addAttribute("totalQuantity", totalQuantity);
        model.addAttribute("totalProduct", countProduct);
        model.addAttribute("totalPrice", totalPrice);
        model.addAttribute("order_id", orderId);
        model.addAttribute("order", orderService.getOrderById(orderId));
        model.addAttribute("billing", orderService.getBillingByOrderId(orderId));

        return "admin/order/detail-order";
    }

    @RequestMapping(value = "/solve-order", method = RequestMethod.POST)
    public String approveOrder(Model model, HttpSession session, @RequestParam("orderId") String orderId) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        BillingRequest billingRequest = orderService.getBillingRequestByOrderId(orderId);
        OrderRequest orderRequest = orderService.getOrderRequestById(orderId);

        // set giá trị cập nhật
        billingRequest.setStatus(BillStatus.UNPAID.getValue());

        orderRequest.setStatus(OrderStatus.DELIVERY.getValue());

        // cật nhật giá trị đơn hàng
        orderService.updateBilling(billingRequest, orderId);
        orderService.updateOrder(orderRequest, orderId);

        return "redirect:/admin/detail-order/" + orderId;
    }

    @RequestMapping("/cancel-order/{orderId}")
    public String cancelOrder(Model model, HttpSession session, @PathVariable("orderId") String orderId) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        model.addAttribute("orderId", orderId);
        return "admin/order/cancel-order";
    }

    @RequestMapping(value = "/cancel-order", method = RequestMethod.POST)
    public String cancelOrder(Model model, HttpSession session, @RequestParam("orderId") String orderId, RedirectAttributes redirectAttributes) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        boolean deleteBilling = orderService.deleteBillingByOrderId(orderId);
        boolean deleteOrderDetail = orderService.deleteOrderDetailsByOrderId(orderId);
        boolean deleteOrderById = orderService.deleteOrderByOrderId(orderId);

        if (deleteBilling == false || deleteOrderDetail == false || deleteOrderById == false) {
            redirectAttributes.addFlashAttribute("message", "Cancel failed!");
            return "redirect:/admin/cancel-order";
        }

        redirectAttributes.addFlashAttribute("message", "Cancel successful!");
        return "redirect:/admin/orders";
    }

}
