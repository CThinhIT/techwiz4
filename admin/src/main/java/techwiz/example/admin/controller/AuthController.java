package techwiz.example.admin.controller;

import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import techwiz.example.admin.models.UserRequest;
import techwiz.example.admin.models.Users;
import techwiz.example.admin.service.IAuth;

@Controller
@RequestMapping("/admin")
public class AuthController {

    @Autowired
    IAuth service;

    @RequestMapping("/login")
    public String login(Model model, HttpSession session) {
        //model.addAttribute("attribute", "value");
        return "admin/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String Login(Model model, HttpSession session, @RequestParam("emailOrUsername") String emailOrUsername, @RequestParam("password") String password, RedirectAttributes redirectAttributes) {
        UserRequest user = new UserRequest();
        user.setPassword(password);

        // Kiểm tra xem chuỗi nhập vào có hình thức của email hay username
        if (isValidEmail(emailOrUsername)) {
            user.setEmail(emailOrUsername); // Nếu là email
        } else {
            user.setUsername(emailOrUsername); // Nếu là username
        }

        // Tiếp tục xử lý đăng nhập
        Users users = service.loginUser(user);
        session.setAttribute("user", users);

        if (users == null) {
            redirectAttributes.addFlashAttribute("errorMessage", "Invalid username or password!");
            return "redirect:/admin/login";
        }

        return "redirect:/admin/dashboard";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model, HttpSession session) {
        session.removeAttribute("user");
        return "redirect:/admin/login";
    }

    // Phương thức kiểm tra xem chuỗi có hình thức của email hay không
    private boolean isValidEmail(String input) {
        // Đây là một ví dụ đơn giản, bạn có thể sử dụng biểu thức chính quy phức tạp hơn để kiểm tra email hợp lệ.
        return input.contains("@gmail.com") || input.contains("@example.com");
    }

}
