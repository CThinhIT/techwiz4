package techwiz.example.admin.controller;

import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import techwiz.example.admin.enums.Role;
import techwiz.example.admin.models.Users;
import techwiz.example.admin.service.IFeedback;

@Controller
@RequestMapping("/admin")
public class FeedbackController {
    
    @Autowired
    IFeedback service;
    
    @RequestMapping("/feedbacks")
    public String feedbacks(Model model, HttpSession session) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        model.addAttribute("feedbacks", service.feedbacks());
        return "admin/feedback/feedbacks";
    }
    
    @RequestMapping("/detail-feedback/{feedbackId}")
    public String detailFeedback(Model model, HttpSession session, @PathVariable("feedbackId") String feedbackId) {
        
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        model.addAttribute("feedback", service.getFeedbackById(feedbackId));
        return "admin/feedback/detail-feedback";
    }
    
}
