package techwiz.example.admin.controller;

import jakarta.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import techwiz.example.admin.enums.BillStatus;
import techwiz.example.admin.enums.OrderStatus;
import techwiz.example.admin.enums.Role;
import techwiz.example.admin.models.Billing;
import techwiz.example.admin.models.Feedback;
import techwiz.example.admin.models.Order;
import techwiz.example.admin.models.Users;
import techwiz.example.admin.service.AccessoriesService;
import techwiz.example.admin.service.AccountService;
import techwiz.example.admin.service.BillingService;
import techwiz.example.admin.service.FeedbackService;
import techwiz.example.admin.service.OrderService;
import techwiz.example.admin.service.PlantService;
import techwiz.example.admin.service.SeedService;

@Controller
@RequestMapping("/admin")
public class DashboardController {

    private final AccountService accountService;
    private final AccessoriesService accessoriesService;
    private final PlantService plantService;
    private final SeedService seedService;
    private final OrderService orderService;
    private final BillingService billingService;
    private final FeedbackService feedbackService;

    @Autowired
    public DashboardController(AccountService accountService, AccessoriesService accessoriesService, PlantService plantService, SeedService seedService, OrderService orderService, BillingService billingService, FeedbackService feedbackService) {
        this.accountService = accountService;
        this.accessoriesService = accessoriesService;
        this.plantService = plantService;
        this.seedService = seedService;
        this.orderService = orderService;
        this.billingService = billingService;
        this.feedbackService = feedbackService;
    }

    @RequestMapping("/dashboard")
    public String dashboard(Model model, HttpSession session) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        // Tính toán sản phẩm để thống kê
        int countPlant = plantService.getAllPlants().size();
        int countAccessories = accessoriesService.getAccessories().size();
        int countSeed = seedService.getAllSeed().size();
        int totalProduct = countPlant + countAccessories + countSeed;

        // Tính toán order
        int countOrderPending = (int) orderService.getAllOrder().stream().filter(order -> order.getStatus() == OrderStatus.PENDING.getValue()).count();
        int countOrderDelivery = (int) orderService.getAllOrder().stream().filter(order -> order.getStatus() == OrderStatus.DELIVERY.getValue()).count();
        int countOrderSolved = (int) orderService.getAllOrder().stream().filter(order -> order.getStatus() == OrderStatus.SOLVE.getValue()).count();

        Date currentDate = new Date(); // Lấy ngày hiện tại
        LocalDate today = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(); // Chuyển đổi sang LocalDate

        int countOrderToday = (int) orderService.getAllOrder().stream()
                .filter(order -> order.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().isEqual(today))
                .count();

        // Tính toán billing để thống kê doanh thu
        List<Billing> bill = billingService.getAllBilling();
        List<Billing> filteredBill = bill.stream()
                .filter(item -> item.getStatus() == BillStatus.PAID.getValue())
                .collect(Collectors.toList());

        int currentYear = today.getYear();
        int currentMonth = today.getMonthValue();
        int currentQuarter = (currentMonth - 1) / 3 + 1;

        double totalRevenueToday = calculateRevenueByDay(filteredBill, currentDate);
        double totalRevenueThisMonth = calculateRevenueByMonth(filteredBill, currentYear, currentMonth);
        double totalRevenueThisYear = calculateRevenueByYear(filteredBill, currentYear);
        double totalRevenueThisQuarter = calculateRevenueByQuarter(filteredBill, currentYear, currentQuarter);
        // Tính số lượng doanh thu tới đây là hết

        // Lấy danh sách order
        List<Order> orders = orderService.getAllOrder();
        List<Order> filteredOrders = orders.stream()
                .filter(order -> order.getStatus() == 0)
                .skip(Math.max(0, orders.size() - 5)) // Bỏ qua (skip) các phần tử trước khi chỉ lấy 5 phần tử cuối cùng
                .collect(Collectors.toList());

        // lấy danh sách feedback
        List<Feedback> feedback = feedbackService.feedbacks();
        int totalFeedbacks = feedback.size();
        int startIndex = Math.max(0, totalFeedbacks - 5); // Xác định chỉ số bắt đầu của phần tử cuối cùng
        List<Feedback> feedbacks = feedback.subList(startIndex, totalFeedbacks);

        // lưu số lượng sản phẩm để thống kê
        model.addAttribute("countPlant", countPlant);
        model.addAttribute("countAccessories", countAccessories);
        model.addAttribute("countSeed", countSeed);
        model.addAttribute("totalProduct", totalProduct);
        model.addAttribute("countOrderToday", countOrderToday);

        // thống kê số lượng order
        model.addAttribute("countOrderPending", countOrderPending);
        model.addAttribute("countOrderDelivery", countOrderDelivery);
        model.addAttribute("countOrderSolved", countOrderSolved);

        // thống kê doanh thu
        model.addAttribute("totalRevenueToday", totalRevenueToday);
        model.addAttribute("totalRevenueThisMonth", totalRevenueThisMonth);
        model.addAttribute("totalRevenueThisYear", totalRevenueThisYear);
        model.addAttribute("totalRevenueThisQuarter", totalRevenueThisQuarter);

        // Lấy order và feedback
        model.addAttribute("orders", filteredOrders);
        model.addAttribute("feedbacks", feedbacks);
        
        return "admin/dashboard";
    }

    public double calculateRevenueByDay(List<Billing> bill, Date targetDate) {
        double totalRevenue = 0;
        LocalDate localTargetDate = targetDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        for (Billing item : bill) {
            LocalDate billingDate = item.getOrder().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if (billingDate.isEqual(localTargetDate)) {
                totalRevenue += item.getBill_amount();
            }
        }
        return totalRevenue;
    }

    public double calculateRevenueByMonth(List<Billing> bill, int targetYear, int targetMonth) {
        double totalRevenue = 0;
        for (Billing item : bill) {
            LocalDate billingDate = item.getOrder().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if (billingDate.getYear() == targetYear && billingDate.getMonthValue() == targetMonth) {
                totalRevenue += item.getBill_amount();
            }
        }
        return totalRevenue;
    }

    public double calculateRevenueByYear(List<Billing> bill, int targetYear) {
        double totalRevenue = 0;
        for (Billing item : bill) {
            LocalDate billingDate = item.getOrder().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            if (billingDate.getYear() == targetYear) {
                totalRevenue += item.getBill_amount();
            }
        }
        return totalRevenue;
    }

    public double calculateRevenueByQuarter(List<Billing> bill, int targetYear, int targetQuarter) {
        double totalRevenue = 0;
        for (Billing item : bill) {
            LocalDate billingDate = item.getOrder().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int itemQuarter = (billingDate.getMonthValue() - 1) / 3 + 1;
            if (billingDate.getYear() == targetYear && itemQuarter == targetQuarter) {
                totalRevenue += item.getBill_amount();
            }
        }
        return totalRevenue;
    }

    public static boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        int day1 = cal1.get(Calendar.DAY_OF_MONTH);
        int month1 = cal1.get(Calendar.MONTH);
        int year1 = cal1.get(Calendar.YEAR);

        int day2 = cal2.get(Calendar.DAY_OF_MONTH);
        int month2 = cal2.get(Calendar.MONTH);
        int year2 = cal2.get(Calendar.YEAR);

        return day1 == day2 && month1 == month2 && year1 == year2;
    }

}
