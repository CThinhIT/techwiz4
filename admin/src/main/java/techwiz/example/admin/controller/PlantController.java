package techwiz.example.admin.controller;

import jakarta.servlet.http.HttpSession;
import java.util.Base64;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import techwiz.example.admin.enums.CategoryRef;
import techwiz.example.admin.enums.Role;
import techwiz.example.admin.models.PlantRequest;
import techwiz.example.admin.models.Plants;
import techwiz.example.admin.models.Users;
import techwiz.example.admin.service.FirebaseStorageService;
import techwiz.example.admin.service.IPlant;

@Controller
@RequestMapping("/admin")
public class PlantController {

    @Autowired
    IPlant service;

    @Autowired
    private FirebaseStorageService storageService;

    @RequestMapping("/plants")
    public String plants(Model model, HttpSession session) {
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }
        
        List<Plants> plants = service.getAllPlants();        
        for (Plants plant : plants) {
            String imageUrl = storageService.getFirebaseImageUrl(plant.getImage());
            plant.setImage(imageUrl);
        }

        model.addAttribute("plants", plants);

        return "admin/plant/plants";
    }

    @RequestMapping("/create-plant")
    public String createPlant(Model model, HttpSession session) {
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        //model.addAttribute("categories", service.getCategories());
        model.addAttribute("specieses", service.getSpecies());
        return "admin/plant/create-plant";
    }

    @RequestMapping(value = "/create-plant", method = RequestMethod.POST)
    public String createPlant(Model model, HttpSession session, @ModelAttribute("PlantRequest") PlantRequest plant, @RequestParam("speciesId") String speciesId, @RequestParam("images") MultipartFile imageFile, RedirectAttributes redirectAttributes) {
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        // Lấy ảnh
        if (!imageFile.isEmpty()) {
            try {
                // Lấy tên file ảnh
                String imageName = imageFile.getOriginalFilename();

                // Tải ảnh lên Firebase Storage
                String imageUrl = storageService.uploadImage(imageFile, imageName);

                // Set đường dẫn ảnh cho đối tượng PlantRequest
                plant.setImage("images/" + imageName);

            } catch (Exception e) {
                // Xử lý lỗi nếu có (nếu cần)
                model.addAttribute("message", "An error occurred while processing the image.");
                // return hoặc throw exception tùy theo trường hợp
            }
        }

        // set giá trị cho plant
        String categoryId = CategoryRef.PLANT.getValue();

        // Tạo plant
        PlantRequest createdPlant = service.createPlant(plant, categoryId, speciesId);
        if (createdPlant == null) {
            redirectAttributes.addFlashAttribute("message", "Create a new plant failed");
            return "redirect:/admin/create-plant";
        }

        redirectAttributes.addFlashAttribute("message", "Create a successful plant!");
        return "redirect:/admin/plants";
    }

    @RequestMapping("/edit-plant/{plantId}")
    public String editPlant(Model model, HttpSession session, @PathVariable("plantId") String plantId) {
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        Plants plant = service.getPlantById(plantId);

        // Lấy URL của ảnh từ Firebase Storage và đặt vào thuộc tính image của đối tượng plant
        String imageUrl = storageService.getFirebaseImageUrl(plant.getImage());

        model.addAttribute("imageUrl", imageUrl);
        model.addAttribute("plant", plant);
        model.addAttribute("specieses", service.getSpecies());

        return "admin/plant/edit-plant";
    }

    @RequestMapping(value = "/edit-plant", method = RequestMethod.POST)
    public String editPlant(Model model, HttpSession session, @RequestParam("id") String plantId, @ModelAttribute("PlantRequest") PlantRequest plant, @RequestParam("speciesId") String speciesId, @RequestParam("images") MultipartFile imageFile, RedirectAttributes redirectAttributes) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        // Check trùng
        // Lấy ảnh
        if (!imageFile.isEmpty()) {
            try {
                // Lấy tên file ảnh
                String imageName = imageFile.getOriginalFilename();

                // Tải ảnh lên Firebase Storage
                String imageUrl = storageService.uploadImage(imageFile, imageName);

                // Set đường dẫn ảnh cho đối tượng PlantRequest
                plant.setImage("images/" + imageName);

            } catch (Exception e) {
                // Xử lý lỗi nếu có (nếu cần)
                model.addAttribute("message", "An error occurred while processing the image.");
                // return hoặc throw exception tùy theo trường hợp
            }
        }

        // Lấy ảnh
        if (!plant.getImage().isEmpty()) {
            plant.setImage(plant.getImage());
        }

        // set giá trị cho plant
        String categoryId = CategoryRef.PLANT.getValue();


        // Cập nhật plant
        PlantRequest updatedPlant = service.updatedPlant(plantId, plant, categoryId, speciesId);
        if (updatedPlant == null) {
            redirectAttributes.addFlashAttribute("message", "Updated plant failed");
            return "redirect:/admin/update-plant";
        }

        redirectAttributes.addFlashAttribute("message", "Updated a successful plant!");
        return "redirect:/admin/plants";
    }

    @RequestMapping("/delete-plant/{plantId}")
    public String deletePlant(Model model, HttpSession session, @PathVariable("plantId") String plantId, RedirectAttributes redirectAttributes) {

        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        boolean deletedPlant = service.deletePlant(plantId);

        if (deletedPlant == false) {
            redirectAttributes.addFlashAttribute("errorMessage", "Plant deletion failed!");
            return "redirect:/admin/plants";
        }

        redirectAttributes.addFlashAttribute("message", "Deleted a successful plant!");
        return "redirect:/admin/plants";
    }

    @RequestMapping("/detail-plant/{plantId}")
    public String detailPlant(Model model, HttpSession session, @PathVariable("plantId") String plantId) {
        Users user = (Users) session.getAttribute("user");
        if (user == null || !user.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
            return "redirect:/admin/login";
        }

        Plants plant = service.getPlantById(plantId);

        // Lấy URL của ảnh từ Firebase Storage và đặt vào thuộc tính image của đối tượng plant
        String imageUrl = storageService.getFirebaseImageUrl(plant.getImage());

        if (imageUrl != null) {
            plant.setImage(imageUrl);
        }

        model.addAttribute("plant", plant);
        model.addAttribute("specieses", service.getSpecies());

        return "admin/plant/detail-plants";
    }

    private String convertImageToBase64(byte[] imageBytes) {
        // Chuyển đổi mảng byte của hình ảnh thành mã Base64
        return Base64.getEncoder().encodeToString(imageBytes);
    }

    // Khai báo InitBinder
//    @InitBinder
//    public void initBinder(WebDataBinder binder) {
//        binder.registerCustomEditor(BigDecimal.class, new PropertyEditorSupport() {
//            @Override
//            public void setAsText(String text) {
//                if (text != null && !text.trim().isEmpty()) {
//                    try {
//                        BigDecimal price = new BigDecimal(text);
//                        setValue(price);
//                    } catch (NumberFormatException e) {
//                        // Xử lý lỗi nếu giá trị không hợp lệ
//                        setValue(null); // Set giá trị thành null nếu không thể chuyển đổi thành BigDecimal
//                    }
//                } else {
//                    setValue(null); // Set giá trị thành null nếu text rỗng hoặc null
//                }
//            }
//        });
//
//    }
}
