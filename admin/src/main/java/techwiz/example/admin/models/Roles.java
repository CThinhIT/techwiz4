package techwiz.example.admin.models;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Roles {
    private String DocumentId;
    private String name;
    private String description;
}
