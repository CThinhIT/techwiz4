package techwiz.example.admin.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Billing {
    String DocumentId;
    Long bill_amount;
//    Date billing_date;
    boolean method_payment;
    
    @JsonIgnore
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference order_id;
    Order order;
    
    int status;
}
