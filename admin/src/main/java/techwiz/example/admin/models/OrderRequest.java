package techwiz.example.admin.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrderRequest {
    Date date;
    int status;
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference user_id;
}
