package techwiz.example.admin.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessoriesRequest {
    private String about;
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    private DocumentReference category_id;
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference species_id;
    private String description;
    private String image;
    private String name;
    private Double price;
    private int stock;
}

