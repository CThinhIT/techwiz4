package techwiz.example.admin.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Users {
    
    private String DocumentId;
    private String address;
    private String email;
    private String name;
    private String password;
    private String phone;
    @JsonIgnore
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    private DocumentReference role_id;
    private Roles role;
    private String username;
    private String token;
    private Integer age;
    private Boolean gender;
    private int status;
}
