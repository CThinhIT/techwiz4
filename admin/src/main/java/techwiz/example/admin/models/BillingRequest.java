package techwiz.example.admin.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BillingRequest {
    Long bill_amount;
//    Date billing_date;
    boolean method_payment;

    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference order_id;
    
    int status;
}
