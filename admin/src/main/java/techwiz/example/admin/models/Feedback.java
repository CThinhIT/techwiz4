package techwiz.example.admin.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Feedback {
    String DocumentId;
    String title;
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference user_id;
    Users users;
    String content;
    
}
