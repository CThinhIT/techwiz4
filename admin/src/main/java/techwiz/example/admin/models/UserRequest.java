package techwiz.example.admin.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRequest {

    private String address;
    private String email;
    private Integer name;
    private String password;
    private String phone;
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    private DocumentReference role_id;
    private String username;
    private Boolean gender;
    private String token;
    private int status;
}
