package techwiz.example.admin.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Order {
    String DocumentId;
    Date date;
    int status;
    @JsonIgnore
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference user_id;
    Users user;
}
