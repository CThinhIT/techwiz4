package techwiz.example.admin.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Seed {
    String DocumentId;
    String about;
    @JsonIgnore
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference category_id;
    Category category;
    @JsonIgnore
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference species_id;
    Species species;
    String image;
    String incubate;
    String name;
    Double price;
    int stock;
    String description;
}
