package techwiz.example.admin.models;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class Category {
    private String DocumentId;
    private String name;
    private String icon;
}
