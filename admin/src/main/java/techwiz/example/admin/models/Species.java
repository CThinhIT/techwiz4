package techwiz.example.admin.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.cloud.firestore.DocumentReference;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Species {

    private String DocumentId;
    private String icon;
    private String name;
    @JsonIgnore
    @JsonSerialize(using = DocumentReferenceSerializer.class)
    @JsonDeserialize(using = DocumentReferenceDeserializer.class)
    DocumentReference category_id;
}
