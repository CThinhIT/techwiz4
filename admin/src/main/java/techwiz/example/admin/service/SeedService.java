package techwiz.example.admin.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteBatch;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.admin.enums.CategoryRef;
import techwiz.example.admin.models.Category;
import techwiz.example.admin.models.Seed;
import techwiz.example.admin.models.SeedRequest;
import techwiz.example.admin.models.Species;

@Service
public class SeedService implements ISeed {
    
    @Override
    public List<Seed> getAllSeed() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "seed"
        CollectionReference seedsCollection = firestore.collection("seed");

        // Tạo danh sách chứa seed
        List<Seed> seedList = new ArrayList<>();
        
        try {
            // Truy vấn Firestore để lấy danh sách seed
            ApiFuture<QuerySnapshot> querySnapshot = seedsCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng seed từ dữ liệu tài liệu
                Seed seed = document.toObject(Seed.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng seed
                String documentId = document.getId();
                seed.setDocumentId(documentId);

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference categoryReference = document.get("category_id", DocumentReference.class);
                DocumentReference speciesReference = document.get("species_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò category
                if (categoryReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot categorySnapshot = categoryReference.get().get();

                    // Nếu vai trò tồn tại
                    if (categorySnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Category category = categorySnapshot.toObject(Category.class);
                        category.setDocumentId(categorySnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng seed
                        seed.setCategory_id(categoryReference);
                        seed.setCategory(category);
                    }
                }

                // Tham chiếu vai trò species
                if (speciesReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot speciesSnapshot = speciesReference.get().get();

                    // Nếu vai trò tồn tại
                    if (speciesSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Species species = speciesSnapshot.toObject(Species.class);
                        species.setDocumentId(speciesSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        seed.setSpecies_id(speciesReference);
                        seed.setSpecies(species);
                    }
                }
                // End set vai trò   

                // Thêm seed vào danh sách
                seedList.add(seed);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách seed
        return seedList;
    }
    
    @Override
    public Seed getSeedById(String seedId) {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu seed với documentId tương ứng
        DocumentReference seedRef = firestore.collection("seed").document(seedId);
        
        try {
            // Lấy tài liệu seed từ Firestore
            DocumentSnapshot seedSnapshot = seedRef.get().get();

            // Nếu tài liệu seed tồn tại
            if (seedSnapshot.exists()) {
                // Tạo đối tượng seed từ dữ liệu tài liệu
                Seed seed = seedSnapshot.toObject(Seed.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng seed
                seed.setDocumentId(seedSnapshot.getId());

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference categoryReference = seedSnapshot.get("category_id", DocumentReference.class);
                DocumentReference speciesReference = seedSnapshot.get("species_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò category
                if (categoryReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot categorySnapshot = categoryReference.get().get();

                    // Nếu vai trò tồn tại
                    if (categorySnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Category category = categorySnapshot.toObject(Category.class);
                        category.setDocumentId(categorySnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng seed
                        seed.setCategory_id(categoryReference);
                        seed.setCategory(category);
                    }
                }
                // End tham chiếu vai trò
                // Tham chiếu đến vai trò species
                if (speciesReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot speciesSnapshot = speciesReference.get().get();

                    // Nếu vai trò tồn tại
                    if (speciesSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Species species = speciesSnapshot.toObject(Species.class);
                        species.setDocumentId(speciesSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng plant
                        seed.setSpecies_id(speciesReference);
                        seed.setSpecies(species);
                    }
                }
                // End tham chiếu vai trò

                // Trả về đối tượng seed
                return seed;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy seed với documentId
        return null;
    }
    
    @Override
    public SeedRequest createSeed(SeedRequest seedRequest, String categoryId, String speciesId) {
        Firestore firestore = FirestoreClient.getFirestore();
        
        try {
            // Lấy tham chiếu đến collection "Seeds"
            CollectionReference seedCollection = firestore.collection("seed");

            // Cách set giá trị cho kiểu Reference
            // Tạo một DocumentReference dựa trên document_id
            DocumentReference categoryReference = firestore.collection("category").document(categoryId);
            DocumentReference speciesReference = firestore.collection("species").document(speciesId);
            // Gán DocumentReference cho trường category_id trong đối tượng Seed
            seedRequest.setCategory_id(categoryReference);
            seedRequest.setSpecies_id(speciesReference);

            // Thêm seed mới vào Firestore
            ApiFuture<DocumentReference> documentReferenceApiFuture = seedCollection.add(seedRequest);

            // Lấy DocumentReference của seed vừa được thêm
            DocumentReference addedSeedRef = documentReferenceApiFuture.get();
            
            return seedRequest;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    public SeedRequest updatedSeed(String seedId, SeedRequest seedRequest, String categoryId, String speciesId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference seedRef = firestore.collection("seed").document(seedId);

        // Cách set giá trị cho kiểu Reference
        // Tạo một DocumentReference dựa trên document_id
        DocumentReference categoryReference = firestore.collection("category").document(categoryId);
        DocumentReference speciesReference = firestore.collection("species").document(speciesId);
        // Gán DocumentReference cho trường category_id trong đối tượng Seed
        seedRequest.setCategory_id(categoryReference);
        seedRequest.setSpecies_id(speciesReference);
        
        try {
            DocumentSnapshot seedSnapshot = seedRef.get().get();
            
            if (seedSnapshot.exists()) {
                // Cập nhật thông tin người dùng
                seedRef.set(seedRequest, SetOptions.merge()).get();
                
                return seedRequest;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    public boolean deletedSeed(String seedId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference seedRef = firestore.collection("seed").document(seedId);

        // Tạo một batch write
        WriteBatch batch = firestore.batch();

        try {
            DocumentSnapshot seedSnapshot = seedRef.get().get();

            if (seedSnapshot.exists()) {
                // Lấy giá trị plantId
                // String seedId = seedId; // Đặt giá trị plantId tương ứng với cấu trúc của bạn

                // Xóa tài liệu cây cảnh
                batch.delete(seedRef);

                // Tạo truy vấn để lấy các tài liệu trong collection order_detail có trường plant_id bằng plantId
                CollectionReference orderDetailCollection = firestore.collection("order_detail");
                Query orderDetailQuery = orderDetailCollection.whereEqualTo("seed_id", firestore.document("seed/" + seedId));

                QuerySnapshot orderDetailSnapshot = orderDetailQuery.get().get();
                for (QueryDocumentSnapshot orderDetailDoc : orderDetailSnapshot) {
                    batch.delete(orderDetailDoc.getReference());
                }

                // Thực hiện batch write
                batch.commit().get();

                return true;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }
    
    @Override
    public List<Category> getCategories() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "category"
        CollectionReference categoriesCollection = firestore.collection("category");

        // Tạo danh sách chứa plant
        List<Category> categorytList = new ArrayList<>();
        
        try {
            // Truy vấn Firestore để lấy danh sách category
            ApiFuture<QuerySnapshot> querySnapshot = categoriesCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng category từ dữ liệu tài liệu
                Category category = document.toObject(Category.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng category
                String documentId = document.getId();
                category.setDocumentId(documentId);

                // Thêm người dùng vào danh sách
                categorytList.add(category);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách categories
        return categorytList;
    }
    
    @Override
    public List<Species> getSpecies() {
        Firestore firestore = FirestoreClient.getFirestore();
        
        CollectionReference speciesCollection = firestore.collection("species");
        
        List<Species> speciesList = new ArrayList<>();
        
        try {
            Query query = speciesCollection.whereEqualTo("category_id", firestore.document("category/" + CategoryRef.SEED.getValue()));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();
            
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                Species species = document.toObject(Species.class);
                String documentId = document.getId();
                species.setDocumentId(documentId);
                
                speciesList.add(species);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        
        return speciesList;
    }
    
}
