package techwiz.example.admin.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import techwiz.example.admin.TokenUtil.TokenUtil;
import techwiz.example.admin.enums.Role;
import techwiz.example.admin.models.Roles;
import techwiz.example.admin.models.UserRequest;
import techwiz.example.admin.models.Users;

@Service
public class AuthService implements IAuth {
//    @Override
//    public Users registerUser(UserRequest user) {
//        return null;
//    }

    @Override
    public Users loginUser(UserRequest user) {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference usersCollection = firestore.collection("users");

        try {
            // Tìm người dùng dựa trên email hoặc username
            Query query = null;
            if (user.getEmail() != null) {
                query = usersCollection.whereEqualTo("email", user.getEmail());
            }
            if (user.getUsername() != null) {
                query = usersCollection.whereEqualTo("username", user.getUsername());
            }

            if (query != null) {
                ApiFuture<QuerySnapshot> querySnapshot = query.get();

                for (QueryDocumentSnapshot document : querySnapshot.get().getDocuments()) {

                    Users storedUser = document.toObject(Users.class);

                    // Lấy id cho user
                    storedUser.setDocumentId(document.getId());

                    // So sánh mật khẩu đã nhập với mật khẩu lưu trong Firestore
                    String password = user.getPassword();

                    if (checkPassword(password, storedUser.getPassword())) {

                        DocumentReference roleReference = storedUser.getRole_id();
                        if (roleReference != null) {
                            DocumentSnapshot roleSnapshot = roleReference.get().get();
                            if (roleSnapshot.exists()) {
                                Roles role = roleSnapshot.toObject(Roles.class);
                                role.setDocumentId(roleSnapshot.getId());
                                storedUser.setRole_id(roleReference);
                                storedUser.setRole(role);
                                if (!storedUser.getRole().getDocumentId().equals(Role.ADMIN.getValue())) {
                                    return null;
                                }
                            }
                        }

                        // Tạo token và lưu vào bảng người dùng
                        // Tạo token
                        String token = TokenUtil.generateToken(storedUser.getDocumentId(), storedUser.getRole_id().getId());
                        saveToken(storedUser.getDocumentId(), token);

                        return storedUser; // Trả về thông tin người dùng nếu đăng nhập thành công
                    }
                }
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null; // Trả về null nếu đăng nhập không thành công
    }

    private void saveToken(String userId, String token) {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu người dùng với documentId tương ứng
        DocumentReference userRef = firestore.collection("users").document(userId);
        try {
            DocumentSnapshot userSnapshot = userRef.get().get();

            if (userSnapshot.exists()) {
                // Tạo một map chứa thông tin token để cập nhật
                Map<String, Object> tokenData = new HashMap<>();
                tokenData.put("token", token);

                // Cập nhật thông tin token trong tài liệu người dùng
                userRef.update(tokenData).get();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private String hashPassword(String password) {
        // Thực hiện mã hóa mật khẩu ở đây (ví dụ: sử dụng BCrypt)
        String hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt());
        return hashedPassword;
    }

    private boolean checkPassword(String password, String hashedPassword) {
        try {
            // Kiểm tra xem hashedPassword có đúng định dạng BCrypt không

            // Kiểm tra mật khẩu đã được mã hóa khớp với mật khẩu gốc
            boolean passwordMatched = BCrypt.checkpw(password, hashedPassword);
            return passwordMatched;
        } catch (IllegalArgumentException e) {
            // Xử lý lỗi khi hashedPassword không đúng định dạng
            return false;
        } catch (Exception e) {
            // Xử lý các lỗi khác nếu có
            return false;
        }
    }

}
