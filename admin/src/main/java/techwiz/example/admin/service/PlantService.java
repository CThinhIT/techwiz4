package techwiz.example.admin.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteBatch;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.admin.enums.CategoryRef;
import techwiz.example.admin.models.Category;
import techwiz.example.admin.models.PlantRequest;
import techwiz.example.admin.models.Plants;
import techwiz.example.admin.models.Species;

@Service
public class PlantService implements IPlant {

    @Override
    public List<Plants> getAllPlants() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "plants"
        CollectionReference plantsCollection = firestore.collection("plants");

        // Tạo danh sách chứa plant
        List<Plants> plantList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách người dùng
            ApiFuture<QuerySnapshot> querySnapshot = plantsCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng người dùng từ dữ liệu tài liệu
                Plants plant = document.toObject(Plants.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                String documentId = document.getId();
                plant.setDocumentId(documentId);

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference categoryReference = document.get("category_id", DocumentReference.class);
                DocumentReference speciesReference = document.get("species_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò category
                if (categoryReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot categorySnapshot = categoryReference.get().get();

                    // Nếu vai trò tồn tại
                    if (categorySnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Category category = categorySnapshot.toObject(Category.class);
                        category.setDocumentId(categorySnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        plant.setCategory_id(categoryReference);
                        plant.setCategory(category);
                    }
                }

                // Tham chiếu vai trò species
                if (speciesReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot speciesSnapshot = speciesReference.get().get();

                    // Nếu vai trò tồn tại
                    if (speciesSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Species species = speciesSnapshot.toObject(Species.class);
                        species.setDocumentId(speciesSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        plant.setSpecies_id(speciesReference);
                        plant.setSpecies(species);
                    }
                }
                // End set vai trò   

                // Thêm người dùng vào danh sách
                plantList.add(plant);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách plants
        return plantList;
    }

    @Override
    public Plants getPlantById(String documentId) {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu plant với documentId tương ứng
        DocumentReference plantRef = firestore.collection("plants").document(documentId);

        try {
            // Lấy tài liệu plant từ Firestore
            DocumentSnapshot plantSnapshot = plantRef.get().get();

            // Nếu tài liệu plant tồn tại
            if (plantSnapshot.exists()) {
                // Tạo đối tượng plant từ dữ liệu tài liệu
                Plants plant = plantSnapshot.toObject(Plants.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng Plants
                plant.setDocumentId(plantSnapshot.getId());

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference categoryReference = plantSnapshot.get("category_id", DocumentReference.class);
                DocumentReference speciesReference = plantSnapshot.get("species_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò category
                if (categoryReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot categorySnapshot = categoryReference.get().get();

                    // Nếu vai trò tồn tại
                    if (categorySnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Category category = categorySnapshot.toObject(Category.class);
                        category.setDocumentId(categorySnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng plant
                        plant.setCategory_id(categoryReference);
                        plant.setCategory(category);
                    }
                }

                // Tham chiếu đến vai trò species
                if (speciesReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot speciesSnapshot = speciesReference.get().get();

                    // Nếu vai trò tồn tại
                    if (speciesSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Species species = speciesSnapshot.toObject(Species.class);
                        species.setDocumentId(speciesSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng plant
                        plant.setSpecies_id(speciesReference);
                        plant.setSpecies(species);
                    }
                }
                // End tham chiếu vai trò

                // Trả về đối tượng plant
                return plant;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy plant với documentId
        return null;
    }

    @Override
    public PlantRequest createPlant(PlantRequest plant, String categoryId, String speciesId) {
        Firestore firestore = FirestoreClient.getFirestore();

        try {
            // Lấy tham chiếu đến collection "Plant"
            CollectionReference plantsCollection = firestore.collection("plants");

            // Cách set giá trị cho kiểu Reference
            // Tạo một DocumentReference dựa trên document_id
            DocumentReference categoryReference = firestore.collection("category").document(categoryId);
            DocumentReference speciesReference = firestore.collection("species").document(speciesId);
            // Gán DocumentReference cho trường category_id and species_id trong đối tượng Plants
            plant.setCategory_id(categoryReference);
            plant.setSpecies_id(speciesReference);

            // Thêm người dùng mới vào Firestore
            ApiFuture<DocumentReference> documentReferenceApiFuture = plantsCollection.add(plant);

            // Lấy DocumentReference của người dùng vừa được thêm
            DocumentReference addedPlantRef = documentReferenceApiFuture.get();

            return plant;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public PlantRequest updatedPlant(String documentId, PlantRequest plantRequest, String categoryId, String speciesId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference plantRef = firestore.collection("plants").document(documentId);

        // Cách set giá trị cho kiểu Reference
        // Tạo một DocumentReference dựa trên document_id
        DocumentReference categoryReference = firestore.collection("category").document(categoryId);
        DocumentReference speciesReference = firestore.collection("species").document(speciesId);
        // Gán DocumentReference cho trường category_id and species_id trong đối tượng Plants
        plantRequest.setCategory_id(categoryReference);
        plantRequest.setSpecies_id(speciesReference);

        try {
            DocumentSnapshot plantSnapshot = plantRef.get().get();

            if (plantSnapshot.exists()) {
                // Cập nhật thông tin người dùng
                plantRef.set(plantRequest, SetOptions.merge()).get();

                return plantRequest;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean deletePlant(String documentId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference plantRef = firestore.collection("plants").document(documentId);

        // Tạo một batch write
        WriteBatch batch = firestore.batch();

        try {
            DocumentSnapshot plantSnapshot = plantRef.get().get();

            if (plantSnapshot.exists()) {
                // Lấy giá trị plantId
                String plantId = documentId; // Đặt giá trị plantId tương ứng với cấu trúc của bạn

                // Xóa tài liệu cây cảnh
                batch.delete(plantRef);

                // Tạo truy vấn để lấy các tài liệu trong collection order_detail có trường plant_id bằng plantId
                CollectionReference orderDetailCollection = firestore.collection("order_detail");
                Query orderDetailQuery = orderDetailCollection.whereEqualTo("plants_id", firestore.document("plants/" + plantId));

                QuerySnapshot orderDetailSnapshot = orderDetailQuery.get().get();
                for (QueryDocumentSnapshot orderDetailDoc : orderDetailSnapshot) {
                    batch.delete(orderDetailDoc.getReference());
                }

                // Thực hiện batch write
                batch.commit().get();

                return true;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public List<Category> getCategories() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "category"
        CollectionReference categoriesCollection = firestore.collection("category");

        // Tạo danh sách chứa plant
        List<Category> categorytList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách category
            ApiFuture<QuerySnapshot> querySnapshot = categoriesCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng category từ dữ liệu tài liệu
                Category category = document.toObject(Category.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng category
                String documentId = document.getId();
                category.setDocumentId(documentId);

                // Thêm người dùng vào danh sách
                categorytList.add(category);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách categories
        return categorytList;
    }

    @Override
    public List<Species> getSpecies() {
        Firestore firestore = FirestoreClient.getFirestore();

        CollectionReference speciesCollection = firestore.collection("species");

        List<Species> speciesList = new ArrayList<>();

        try {
            Query query = speciesCollection.whereEqualTo("category_id", firestore.document("category/" + CategoryRef.PLANT.getValue()));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                Species species = document.toObject(Species.class);
                String documentId = document.getId();
                species.setDocumentId(documentId);

                speciesList.add(species);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return speciesList;
    }

}
