package techwiz.example.admin.service;

import java.util.List;
import techwiz.example.admin.models.Feedback;

public interface IFeedback {
    List<Feedback> feedbacks();
    Feedback getFeedbackById(String feedbackId);
}
