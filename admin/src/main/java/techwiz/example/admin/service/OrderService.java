package techwiz.example.admin.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.admin.models.Accessories;
import techwiz.example.admin.models.Billing;
import techwiz.example.admin.models.BillingRequest;
import techwiz.example.admin.models.Order;
import techwiz.example.admin.models.OrderDetail;
import techwiz.example.admin.models.OrderRequest;
import techwiz.example.admin.models.Plants;
import techwiz.example.admin.models.Seed;
import techwiz.example.admin.models.Users;

@Service
public class OrderService implements IOrder {

    @Override
    public List<Order> getAllOrder() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "Order"
        CollectionReference ordersCollection = firestore.collection("order");

        // Tạo danh sách chứa order
        List<Order> orderList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách người dùng
            ApiFuture<QuerySnapshot> querySnapshot = ordersCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng người dùng từ dữ liệu tài liệu
                Order order = document.toObject(Order.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                String documentId = document.getId();
                order.setDocumentId(documentId);

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference usersReference = document.get("user_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò category
                if (usersReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot userSnapshot = usersReference.get().get();

                    // Nếu vai trò tồn tại
                    if (userSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Users user = userSnapshot.toObject(Users.class);
                        user.setDocumentId(userSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        order.setUser_id(usersReference);
                        order.setUser(user);
                    }
                }

                // End set vai trò   
                // Thêm người dùng vào danh sách
                orderList.add(order);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách plants
        return orderList;
    }

    @Override
    public List<OrderDetail> getOrderDetailByOrderId(String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference orderDetailsCollection = firestore.collection("order_detail");

        List<OrderDetail> orderDetailList = new ArrayList<>();

        try {
            Query query = orderDetailsCollection.whereEqualTo("order_id", firestore.document("order/" + orderId));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                OrderDetail orderDetail = document.toObject(OrderDetail.class);
                String documentId = document.getId();
                orderDetail.setDocumentId(documentId);

                // Lấy thông tin accessories từ tham chiếu
                DocumentReference accessoriesRef = orderDetail.getProduct_id();
                DocumentSnapshot documentSnapshotProduct = accessoriesRef.get().get();
                // lấy thông tin category
                DocumentReference categoryReference = documentSnapshotProduct.get("category_id", DocumentReference.class);
                DocumentSnapshot documentSnapshotCategory = categoryReference.get().get();
                String category = documentSnapshotCategory.getString("name");

                if(!category.equalsIgnoreCase("accessories")){
                    accessoriesRef = null;
                }
                if (accessoriesRef != null) {
                    DocumentSnapshot accessoriesSnapshot = accessoriesRef.get().get();
                    Accessories accessories = accessoriesSnapshot.toObject(Accessories.class);
                    accessories.setDocumentId(accessoriesSnapshot.getId());
                    orderDetail.setAccessories(accessories);
                }

                // Lấy thông tin order từ tham chiếu
                DocumentReference orderRef = orderDetail.getOrder_id();
                if (orderRef != null) {
                    DocumentSnapshot orderSnapshot = orderRef.get().get();
                    Order order = orderSnapshot.toObject(Order.class);
                    order.setDocumentId(orderSnapshot.getId());
                    orderDetail.setOrder(order);
                }

                // Lấy thông tin plant từ tham chiếu
                DocumentReference plantRef = orderDetail.getProduct_id();
                if(!category.equalsIgnoreCase("plant")){
                    plantRef = null;
                }
                if (plantRef != null) {
                    DocumentSnapshot plantSnapshot = plantRef.get().get();
                    Plants plant = plantSnapshot.toObject(Plants.class);
                    plant.setDocumentId(plantSnapshot.getId());
                    orderDetail.setPlant(plant);
                }

                // Lấy thông tin seed từ tham chiếu
                DocumentReference seedRef = orderDetail.getProduct_id();
                if(!category.equalsIgnoreCase("seed")){
                    seedRef = null;
                }
                if (seedRef != null) {
                    DocumentSnapshot seedSnapshot = seedRef.get().get();
                    Seed seed = seedSnapshot.toObject(Seed.class);
                    seed.setDocumentId(seedSnapshot.getId());
                    orderDetail.setSeed(seed);
                }

                // Thêm orderDetail vào danh sách
                orderDetailList.add(orderDetail);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return orderDetailList;
    }

    @Override
    public boolean deleteOrderDetailsByOrderId(String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference orderDetailsCollection = firestore.collection("order_detail");

        try {
            Query query = orderDetailsCollection.whereEqualTo("order_id", firestore.document("order/" + orderId));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                String orderDetailDocumentId = document.getId();
                firestore.collection("order_detail").document(orderDetailDocumentId).delete();
            }

            return true; // Successfully deleted all order details associated with the order ID
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false; // Failed to delete order details
    }

    @Override
    public Order getOrderById(String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference orderRef = firestore.collection("order").document(orderId);

        try {
            DocumentSnapshot orderSnapshot = orderRef.get().get();

            if (orderSnapshot.exists()) {
                Order order = orderSnapshot.toObject(Order.class);
                order.setDocumentId(orderSnapshot.getId());

                // Lấy thông tin người dùng từ tham chiếu
                DocumentReference userRef = order.getUser_id();
                if (userRef != null) {
                    DocumentSnapshot userSnapshot = userRef.get().get();
                    Users user = userSnapshot.toObject(Users.class);
                    user.setDocumentId(userSnapshot.getId());
                    order.setUser(user);
                }

                return order;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean deleteOrderByOrderId(String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference orderRef = firestore.collection("order").document(orderId);

        try {
            DocumentSnapshot orderSnapshot = orderRef.get().get();

            if (orderSnapshot.exists()) {
                // Delete the order document
                orderRef.delete();
                return true; // Successfully deleted the order
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false; // Failed to delete the order
    }

    @Override
    public List<Billing> getAllBilling() {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference billingCollection = firestore.collection("billing");

        List<Billing> billingList = new ArrayList<>();

        try {
            ApiFuture<QuerySnapshot> querySnapshot = billingCollection.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                Billing billing = document.toObject(Billing.class);
                String documentId = document.getId();
                billing.setDocumentId(documentId);

                // Thêm billing vào danh sách
                billingList.add(billing);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return billingList;
    }

    @Override
    public Billing getBillingByOrderId(String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference billingCollection = firestore.collection("billing");

        try {
            Query query = billingCollection.whereEqualTo("order_id", firestore.document("order/" + orderId));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                Billing billing = document.toObject(Billing.class);
                String documentId = document.getId();
                billing.setDocumentId(documentId);

                return billing; // Chỉ lấy thông tin billing đầu tiên tìm thấy
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null; // Trả về null nếu không tìm thấy thông tin billing
    }

    @Override
    public boolean deleteBillingByOrderId(String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference billingCollection = firestore.collection("billing");

        try {
            Query query = billingCollection.whereEqualTo("order_id", firestore.document("order/" + orderId));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                String billingDocumentId = document.getId();
                firestore.collection("billing").document(billingDocumentId).delete();
                return true; // Successfully deleted the billing entry
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false; // Failed to delete the billing entry
    }

    @Override
    public BillingRequest updateBilling(BillingRequest billingRequest, String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();

        CollectionReference billingCollection = firestore.collection("billing");

        try {
            Query query = billingCollection.whereEqualTo("order_id", firestore.document("order/" + orderId));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                DocumentReference billingRef = billingCollection.document(document.getId());

                // Cập nhật thông tin billing
                billingRef.set(billingRequest, SetOptions.merge());

                return billingRequest;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null; // Trả về null nếu không tìm thấy thông tin billing
    }

    @Override
    public OrderRequest updateOrder(OrderRequest orderRequest, String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference orderRef = firestore.collection("order").document(orderId);

        try {
            DocumentSnapshot orderSnapshot = orderRef.get().get();

            if (orderSnapshot.exists()) {
                // Cập nhật thông tin đơn hàng
                orderRef.set(orderRequest, SetOptions.merge());

                return orderRequest;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null; // Trả về null nếu không tìm thấy thông tin đơn hàng
    }

    @Override
    public BillingRequest getBillingRequestByOrderId(String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference billingCollection = firestore.collection("billing");

        try {
            Query query = billingCollection.whereEqualTo("order_id", firestore.document("order/" + orderId));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                BillingRequest billingRequest = document.toObject(BillingRequest.class);
                String documentId = document.getId();

                return billingRequest; // Chỉ lấy thông tin billing request đầu tiên tìm thấy
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null; // Trả về null nếu không tìm thấy thông tin billing request
    }

    @Override
    public OrderRequest getOrderRequestById(String orderId) {
        Firestore firestore = FirestoreClient.getFirestore();
        CollectionReference orderRequestCollection = firestore.collection("order");

        try {
            DocumentReference orderRequestRef = orderRequestCollection.document(orderId);
            DocumentSnapshot orderRequestSnapshot = orderRequestRef.get().get();

            if (orderRequestSnapshot.exists()) {
                OrderRequest orderRequest = orderRequestSnapshot.toObject(OrderRequest.class);
                String documentId = orderRequestSnapshot.getId();

                return orderRequest;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null; // Trả về null nếu không tìm thấy thông tin order request
    }

}
