package techwiz.example.admin.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FirebaseStorageService {

    private Storage storage;
    private String bucketName;

    @Autowired
    public FirebaseStorageService(
            ResourceLoader resourceLoader,
            @Value("${firebase.storage.bucketName}") String bucketName) {
        try {
            this.bucketName = bucketName;
            Resource resource = resourceLoader.getResource("classpath:serviceAccountKey.json");
            FileInputStream serviceAccount = new FileInputStream(resource.getFile());
            GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
            storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFirebaseImageUrl(String imageName) {
        try {
            if (bucketName != null && imageName != null) {
                BlobId blobId = BlobId.of(bucketName, imageName);
                Blob blob = storage.get(blobId);

                if (blob != null) {
                    return blob.signUrl(365, TimeUnit.DAYS).toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String uploadImage(MultipartFile imageFile, String imageName) {
        List<String> contentTypes = Arrays.asList("image/jpeg", "image/png");
        try {
            String imagePath = "images/" + imageName;
            BlobId blobId = BlobId.of(bucketName, imagePath);

            String detectedContentType = "application/octet-stream"; // Mặc định là application/octet-stream

            // Kiểm tra loại tệp tin và xác định contentType tương ứng
            // Ví dụ: kiểm tra xem tệp có đuôi .jpg hay .jpeg hay không để đặt contentType là "image/jpeg"
            // Bạn có thể thêm các quy tắc kiểm tra khác tùy theo nhu cầu
            String filename = imageFile.getOriginalFilename();
            if (filename != null) {
                if (filename.toLowerCase().endsWith(".jpg") || filename.toLowerCase().endsWith(".jpeg")) {
                    detectedContentType = "image/jpeg";
                } else if (filename.toLowerCase().endsWith(".png")) {
                    detectedContentType = "image/png";
                }
            }

            if (contentTypes.contains(detectedContentType)) {
                BlobInfo blobInfo = BlobInfo.newBuilder(blobId)
                        .setContentType(detectedContentType)
                        .build();
                byte[] bytes = imageFile.getBytes();
                Blob blob = storage.create(blobInfo, bytes);

                return getFirebaseImageUrl(imagePath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

        // Nếu chỉ cần thêm ảnh kiểu application/octet-stream(có thể upload mọi loại ảnh)
        // try {
        // String imagePath = "images/" + imageName; // Đường dẫn trong thư mục images
        // BlobId blobId = BlobId.of(bucketName, imagePath);
        // BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
        // byte[] bytes = imageFile.getBytes();
        // Blob blob = storage.create(blobInfo, bytes);
        //
        // return getFirebaseImageUrl(imagePath); // Trả về URL của ảnh đã tải lên
        // } catch (Exception e) {
        //    e.printStackTrace();
        // }
        //
        // return null;
}
