package techwiz.example.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

@Configuration
public class FirebaseConfig {

    @Autowired
    private ResourceLoader resourceLoader;

    @Value("${firebase.storage.bucketName}")
    private String bucketName;

    @Bean
    public FirebaseStorageService firebaseStorageService() {
        return new FirebaseStorageService(resourceLoader, bucketName);
    }
}


