/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/Service.java to edit this template
 */
package techwiz.example.admin.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.admin.models.Roles;
import techwiz.example.admin.models.Users;

/**
 *
 * @author admin
 */
@Service
public class AccountService implements IAccount{

    @Override
    public List<Users> getListUser() {
// Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "users"
        CollectionReference usersCollection = firestore.collection("users");

        // Tạo danh sách chứa người dùng
        List<Users> userList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách người dùng
            ApiFuture<QuerySnapshot> querySnapshot = usersCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng người dùng từ dữ liệu tài liệu
                Users user = document.toObject(Users.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                String documentId = document.getId();
                user.setDocumentId(documentId);

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference roleReference = document.get("role_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò
                if (roleReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot roleSnapshot = roleReference.get().get();

                    // Nếu vai trò tồn tại
                    if (roleSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Roles role = roleSnapshot.toObject(Roles.class);
                        role.setDocumentId(roleSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        user.setRole_id(roleReference);
                        user.setRole(role);
                    }
                }

                // Thêm người dùng vào danh sách
                userList.add(user);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách người dùng
        return userList;    
    }

    @Override
    public Users getDetailUser(String documentId) {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu người dùng với documentId tương ứng
        DocumentReference userRef = firestore.collection("users").document(documentId);

        try {
            // Lấy tài liệu người dùng từ Firestore
            DocumentSnapshot userSnapshot = userRef.get().get();

            // Nếu tài liệu người dùng tồn tại
            if (userSnapshot.exists()) {
                // Tạo đối tượng người dùng từ dữ liệu tài liệu
                Users user = userSnapshot.toObject(Users.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                user.setDocumentId(userSnapshot.getId());

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference roleReference = userSnapshot.get("role_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò
                if (roleReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot roleSnapshot = roleReference.get().get();

                    // Nếu vai trò tồn tại
                    if (roleSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Roles role = roleSnapshot.toObject(Roles.class);
                        role.setDocumentId(roleSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        user.setRole_id(roleReference);
                        user.setRole(role);
                    }
                }

                // Trả về đối tượng người dùng
                return user;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy người dùng với documentId
        return null;    
    }
    
}
