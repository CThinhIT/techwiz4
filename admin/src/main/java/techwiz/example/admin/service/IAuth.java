package techwiz.example.admin.service;

import techwiz.example.admin.models.UserRequest;
import techwiz.example.admin.models.Users;


public interface IAuth {
    // Users registerUser(UserRequest user);
    Users loginUser(UserRequest user);
}
