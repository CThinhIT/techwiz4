package techwiz.example.admin.service;

import java.util.List;
import techwiz.example.admin.models.Category;
import techwiz.example.admin.models.PlantRequest;
import techwiz.example.admin.models.Plants;
import techwiz.example.admin.models.Species;

public interface IPlant {
    List<Plants> getAllPlants();
    Plants getPlantById(String documentId);
    PlantRequest createPlant(PlantRequest plant, String categoryId, String speciesId);
    PlantRequest updatedPlant(String documentId, PlantRequest plantRequest, String categoryId, String speciesId);
    boolean deletePlant(String documentId);
    
    List<Category> getCategories();
    List<Species> getSpecies();
}
