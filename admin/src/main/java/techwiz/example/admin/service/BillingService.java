package techwiz.example.admin.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.admin.models.Billing;
import techwiz.example.admin.models.Order;

@Service
public class BillingService {

    public List<Billing> getAllBilling() {
    Firestore firestore = FirestoreClient.getFirestore();
    CollectionReference billingCollection = firestore.collection("billing");
    
    List<Billing> billingList = new ArrayList<>();
    
    try {
        ApiFuture<QuerySnapshot> querySnapshot = billingCollection.get();
        
        for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
            Billing billing = document.toObject(Billing.class);
            String documentId = document.getId();
            billing.setDocumentId(documentId);
            
            // Lấy thông tin order_id từ Billing
            DocumentReference orderRef = billing.getOrder_id();
            DocumentSnapshot orderSnapshot = orderRef.get().get();
            
            if (orderSnapshot.exists()) {
                Order order = orderSnapshot.toObject(Order.class);
                order.setDocumentId(orderSnapshot.getId());
                billing.setOrder(order);
            }
            
            billingList.add(billing);
        }
    } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
    }
    
    return billingList;
}


}
