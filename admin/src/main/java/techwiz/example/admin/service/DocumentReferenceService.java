/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/Service.java to edit this template
 */
package techwiz.example.admin.service;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import org.springframework.stereotype.Service;
import techwiz.example.admin.models.Accessories;

/**
 *
 * @author admin
 */
@Service
public class DocumentReferenceService {
     private Firestore firestore;

    public DocumentReferenceService() {
        // Khởi tạo Firestore
        FirestoreOptions firestoreOptions = FirestoreOptions.newBuilder().setProjectId("techwiz4-79ba4").build();
        this.firestore = firestoreOptions.getService();
    }

    public DocumentReference getDocumentReferenceFromString(String documentPath) {
        // Chuyển đổi chuỗi thành DocumentReference
        DocumentReference documentReference = firestore.collection("accessories").document(documentPath);
        return documentReference;
    }
}
