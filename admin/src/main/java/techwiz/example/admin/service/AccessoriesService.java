package techwiz.example.admin.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.Query;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.SetOptions;
import com.google.cloud.firestore.WriteBatch;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.admin.enums.CategoryRef;
import techwiz.example.admin.models.Accessories;
import techwiz.example.admin.models.AccessoriesRequest;
import techwiz.example.admin.models.Category;
import techwiz.example.admin.models.Species;

@Service
public class AccessoriesService implements IAccessories{
    @Override
    public List<Accessories> getAccessories() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "users"
        CollectionReference accessoriesCollection = firestore.collection("accessories");

        // Tạo danh sách chứa người dùng
        List<Accessories> accessoriesList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách người dùng
            ApiFuture<QuerySnapshot> querySnapshot = accessoriesCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng người dùng từ dữ liệu tài liệu
                Accessories accessories = document.toObject(Accessories.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                String documentId = document.getId();
                accessories.setDocumentId(documentId);

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference categoryReference = document.get("category_id", DocumentReference.class);
                DocumentReference speciesReference = document.get("species_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò
                if (categoryReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot categorySnapshot = categoryReference.get().get();

                    // Nếu vai trò tồn tại
                    if (categorySnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Category cate = categorySnapshot.toObject(Category.class);
                        cate.setDocumentId(categorySnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        accessories.setCategory_id(categoryReference);
                        
                    }
                }
                
                // Tham chiếu vai trò species
                if (speciesReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot speciesSnapshot = speciesReference.get().get();

                    // Nếu vai trò tồn tại
                    if (speciesSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Species species = speciesSnapshot.toObject(Species.class);
                        species.setDocumentId(speciesSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        accessories.setSpecies_id(speciesReference);
                        accessories.setSpecies(species);
                    }
                }
                // End set vai trò   

                // Thêm người dùng vào danh sách
                accessoriesList.add(accessories);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách người dùng
        return accessoriesList;

    }

    @Override
    public Accessories getAccessories(String documentId) {
// Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu người dùng với documentId tương ứng
        DocumentReference accessoriesRef = firestore.collection("accessories").document(documentId);

        try {
            // Lấy tài liệu người dùng từ Firestore
            DocumentSnapshot accessoriesSnapshot = accessoriesRef.get().get();

            // Nếu tài liệu người dùng tồn tại
            if (accessoriesSnapshot.exists()) {
                // Tạo đối tượng người dùng từ dữ liệu tài liệu
                Accessories accessories = accessoriesSnapshot.toObject(Accessories.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                accessories.setDocumentId(accessoriesSnapshot.getId());

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference categoryReference = accessoriesSnapshot.get("category_id", DocumentReference.class);
                DocumentReference speciesReference = accessoriesSnapshot.get("species_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò
                if (categoryReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot categorySnapshot = categoryReference.get().get();

                    // Nếu vai trò tồn tại
                    if (categorySnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Category cate = categorySnapshot.toObject(Category.class);
                        cate.setDocumentId(categorySnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        accessories.setCategory_id(categoryReference);
                        
                    }
                }
                
                // Tham chiếu đến vai trò species
                if (speciesReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot speciesSnapshot = speciesReference.get().get();

                    // Nếu vai trò tồn tại
                    if (speciesSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Species species = speciesSnapshot.toObject(Species.class);
                        species.setDocumentId(speciesSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng plant
                        accessories.setSpecies_id(speciesReference);
                        accessories.setSpecies(species);
                    }
                }
                // End tham chiếu vai trò

                // Trả về đối tượng người dùng
                return accessories;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy người dùng với documentId
        return null;
    }

    @Override
    public AccessoriesRequest createAccessory(AccessoriesRequest accessories, String categoryId, String speciesId) {
        Firestore firestore = FirestoreClient.getFirestore();

        try {
            // Lấy tham chiếu đến collection "users"
            CollectionReference usersCollection = firestore.collection("accessories");
            
            DocumentReference categoryReference = firestore.collection("category").document(categoryId);
            DocumentReference speciesReference = firestore.collection("species").document(speciesId);
            // Gán DocumentReference cho trường category_id and species_id trong đối tượng Plants
            accessories.setCategory_id(categoryReference);
            accessories.setSpecies_id(speciesReference);
            
            ApiFuture<DocumentReference> documentReferenceApiFuture = usersCollection.add(accessories);

            // Lấy DocumentReference của người dùng vừa được thêm
            DocumentReference addedAccessoriesRef = documentReferenceApiFuture.get();

            return accessories;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public AccessoriesRequest updateAccessory(String documentId, AccessoriesRequest updatedAccessories, String categoryId, String speciesId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference accessoriesRef = firestore.collection("accessories").document(documentId);
        
        // Cách set giá trị cho kiểu Reference
        // Tạo một DocumentReference dựa trên document_id
        DocumentReference categoryReference = firestore.collection("category").document(categoryId);
        DocumentReference speciesReference = firestore.collection("species").document(speciesId);
        // Gán DocumentReference cho trường category_id and species_id trong đối tượng Plants
        updatedAccessories.setCategory_id(categoryReference);
        updatedAccessories.setSpecies_id(speciesReference);

        try {
            DocumentSnapshot accessoriesSnapshot = accessoriesRef.get().get();

            if (accessoriesSnapshot.exists()) {
                // Cập nhật thông tin người dùng
                accessoriesRef.set(updatedAccessories, SetOptions.merge()).get();

                return updatedAccessories;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean deleteAccessory(String documentId) {
        Firestore firestore = FirestoreClient.getFirestore();
        DocumentReference accessoriesRef = firestore.collection("accessories").document(documentId);

        // Tạo một batch write
        WriteBatch batch = firestore.batch();

        try {
            DocumentSnapshot accessoriesSnapshot = accessoriesRef.get().get();

            if (accessoriesSnapshot.exists()) {
                // Lấy giá trị plantId
                String accessoriesId = documentId; // Đặt giá trị plantId tương ứng với cấu trúc của bạn

                // Xóa tài liệu cây cảnh
                batch.delete(accessoriesRef);

                // Tạo truy vấn để lấy các tài liệu trong collection order_detail có trường plant_id bằng plantId
                CollectionReference orderDetailCollection = firestore.collection("order_detail");
                Query orderDetailQuery = orderDetailCollection.whereEqualTo("accessories_id", firestore.document("accessories/" + accessoriesId));

                QuerySnapshot orderDetailSnapshot = orderDetailQuery.get().get();
                for (QueryDocumentSnapshot orderDetailDoc : orderDetailSnapshot) {
                    batch.delete(orderDetailDoc.getReference());
                }

                // Thực hiện batch write
                batch.commit().get();

                return true;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Category getCategory(String documentId) {
// Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu vai trò với documentId tương ứng
        DocumentReference categoryRef = firestore.collection("accessories").document(documentId);

        try {
            // Lấy tài liệu vai trò từ Firestore
            DocumentSnapshot categorySnapshot = categoryRef.get().get();

            // Nếu tài liệu vai trò tồn tại
            if (categorySnapshot.exists()) {
                // Tạo đối tượng vai trò từ dữ liệu tài liệu
                Category cate = categorySnapshot.toObject(Category.class);

                // Gán giá trị documentId cho đối tượng vai trò
                cate.setDocumentId(categorySnapshot.getId());

                // Trả về đối tượng vai trò
                return cate;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy vai trò với documentId
        return null;
    }

    @Override
    public List<Category> getListCategory() {
// Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "users"
        CollectionReference categoryCollection = firestore.collection("category");

        // Tạo danh sách chứa người dùng
        List<Category> categoryList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách người dùng
            ApiFuture<QuerySnapshot> querySnapshot = categoryCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng người dùng từ dữ liệu tài liệu
                Category category = document.toObject(Category.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                String documentId = document.getId();
                category.setDocumentId(documentId);   

                // Thêm người dùng vào danh sách
                categoryList.add(category);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách người dùng
        return categoryList;
    }
    
    @Override
    public List<Species> getSpecies() {
        Firestore firestore = FirestoreClient.getFirestore();

        CollectionReference speciesCollection = firestore.collection("species");

        List<Species> speciesList = new ArrayList<>();

        try {
            Query query = speciesCollection.whereEqualTo("category_id", firestore.document("category/" + CategoryRef.ACCESSORIES.getValue()));
            ApiFuture<QuerySnapshot> querySnapshot = query.get();

            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                Species species = document.toObject(Species.class);
                String documentId = document.getId();
                species.setDocumentId(documentId);

                speciesList.add(species);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return speciesList;
    }
}
