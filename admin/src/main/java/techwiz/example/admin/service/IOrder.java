package techwiz.example.admin.service;

import java.util.List;
import techwiz.example.admin.models.Billing;
import techwiz.example.admin.models.BillingRequest;
import techwiz.example.admin.models.Order;
import techwiz.example.admin.models.OrderDetail;
import techwiz.example.admin.models.OrderRequest;

public interface IOrder {
    List<Order> getAllOrder();
    Order getOrderById(String orderId);
    boolean deleteOrderByOrderId(String orderId);
    
    List<Billing> getAllBilling();
    Billing getBillingByOrderId(String orderId);
    boolean deleteBillingByOrderId(String orderId);
    
    BillingRequest getBillingRequestByOrderId(String orderId);
    OrderRequest getOrderRequestById(String orderId);
    
    BillingRequest updateBilling(BillingRequest billingRequest, String orderId);
    OrderRequest updateOrder(OrderRequest orderRequest, String orderId);
    
    List<OrderDetail> getOrderDetailByOrderId(String orderId);
    boolean deleteOrderDetailsByOrderId(String orderId);
}
