package techwiz.example.admin.service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.cloud.FirestoreClient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import org.springframework.stereotype.Service;
import techwiz.example.admin.models.Category;
import techwiz.example.admin.models.Feedback;
import techwiz.example.admin.models.Plants;
import techwiz.example.admin.models.Species;
import techwiz.example.admin.models.Users;

@Service
public class FeedbackService implements IFeedback {

    @Override
    public List<Feedback> feedbacks() {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến collection "plants"
        CollectionReference feedbackCollection = firestore.collection("feedback");

        // Tạo danh sách chứa plant
        List<Feedback> feedbackList = new ArrayList<>();

        try {
            // Truy vấn Firestore để lấy danh sách người dùng
            ApiFuture<QuerySnapshot> querySnapshot = feedbackCollection.get();

            // Lặp qua từng tài liệu trong kết quả truy vấn
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                // Tạo một đối tượng người dùng từ dữ liệu tài liệu
                Feedback feedback = document.toObject(Feedback.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng người dùng
                String documentId = document.getId();
                feedback.setDocumentId(documentId);

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference userReference = document.get("user_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò category
                if (userReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot userSnapshot = userReference.get().get();

                    // Nếu vai trò tồn tại
                    if (userSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Users user = userSnapshot.toObject(Users.class);
                        user.setDocumentId(userSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng người dùng
                        feedback.setUser_id(userReference);
                        feedback.setUsers(user);
                    }
                }
                // End set vai trò   

                // Thêm người dùng vào danh sách
                feedbackList.add(feedback);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về danh sách plants
        return feedbackList;
    }

    @Override
    public Feedback getFeedbackById(String feedbackId) {
        // Lấy thể hiện của Firestore
        Firestore firestore = FirestoreClient.getFirestore();

        // Lấy tham chiếu đến tài liệu plant với documentId tương ứng
        DocumentReference feedbackRef = firestore.collection("feedback").document(feedbackId);

        try {
            // Lấy tài liệu plant từ Firestore
            DocumentSnapshot feedbackSnapshot = feedbackRef.get().get();

            // Nếu tài liệu plant tồn tại
            if (feedbackSnapshot.exists()) {
                // Tạo đối tượng plant từ dữ liệu tài liệu
                Feedback feedback = feedbackSnapshot.toObject(Feedback.class);

                // Lấy giá trị documentId từ DocumentSnapshot và gán cho đối tượng Plants
                feedback.setDocumentId(feedbackSnapshot.getId());

                // Lấy tham chiếu của vai trò từ tài liệu
                DocumentReference userReference = feedbackSnapshot.get("user_id", DocumentReference.class);

                // Nếu có tham chiếu đến vai trò category
                if (userReference != null) {
                    // Lấy thông tin vai trò từ Firestore
                    DocumentSnapshot userSnapshot = userReference.get().get();

                    // Nếu vai trò tồn tại
                    if (userSnapshot.exists()) {
                        // Tạo đối tượng vai trò từ dữ liệu tài liệu
                        Users user = userSnapshot.toObject(Users.class);
                        user.setDocumentId(userSnapshot.getId());

                        // Gán thông tin vai trò vào đối tượng plant
                        feedback.setUser_id(userReference);
                        feedback.setUsers(user);
                    }
                }
                // End tham chiếu vai trò

                // Trả về đối tượng plant
                return feedback;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // Trả về null nếu không tìm thấy plant với documentId
        return null;
    }

}
