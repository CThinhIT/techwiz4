/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package techwiz.example.admin.service;

import java.util.List;
import techwiz.example.admin.models.Users;

/**
 *
 * @author admin
 */
public interface IAccount {
    List<Users> getListUser();
    Users getDetailUser(String documentId);
}
