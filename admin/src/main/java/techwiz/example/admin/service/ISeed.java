package techwiz.example.admin.service;

import java.util.List;
import techwiz.example.admin.models.Category;
import techwiz.example.admin.models.Seed;
import techwiz.example.admin.models.SeedRequest;
import techwiz.example.admin.models.Species;

public interface ISeed {
    List<Seed> getAllSeed();
    Seed getSeedById(String seedId);
    
    SeedRequest createSeed(SeedRequest seedRequest, String categoryId, String speciesId);
    SeedRequest updatedSeed(String seedId, SeedRequest seedRequest, String categoryId, String speciesId);
    boolean deletedSeed(String seedId); 
            
    List<Category> getCategories();
    public List<Species> getSpecies();
}
