package techwiz.example.admin.service;

import java.util.List;
import techwiz.example.admin.models.Accessories;
import techwiz.example.admin.models.AccessoriesRequest;
import techwiz.example.admin.models.Category;
import techwiz.example.admin.models.Species;

public interface IAccessories {
    List<Accessories> getAccessories();
    Accessories getAccessories(String documentId);
    AccessoriesRequest createAccessory(AccessoriesRequest accessories, String categoryId, String speciesId);
    AccessoriesRequest updateAccessory(String documentId, AccessoriesRequest updatedAccessories, String categoryId, String speciesId);
    boolean deleteAccessory(String documentId);
    Category getCategory(String documentId);
    List<Category> getListCategory();
    public List<Species> getSpecies();
}
