package techwiz.example.admin.enums;

public enum SpeciesRef {
    HOUSE_PLANTS("XuoQePaIUIAjWRcE79T6"),
    FRUIT_TREES("sWCDgt9AMNqu8eeLqlXx"),
    FLOWER_TREE("itM0mfBUNpH6C1KKVt9f"),
    FIG_WORT("10NvzOVE6s8V1OugJje5");

    private final String value;

    SpeciesRef(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
