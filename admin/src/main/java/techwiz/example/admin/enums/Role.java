package techwiz.example.admin.enums;

public enum Role {
    ADMIN("hFn4LdpNfyk79m4zTHDa"),
    CUSTOMER("0OX6T54myQw2k7DadF3U");

    private final String value;

    Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
