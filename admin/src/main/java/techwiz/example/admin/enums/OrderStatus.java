package techwiz.example.admin.enums;

public enum OrderStatus {
    PENDING(0),
    DELIVERY(1),
    SOLVE(2);

    private final int value;

    OrderStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
