package techwiz.example.admin.enums;

public enum BillStatus {
    WAIT(0),
    UNPAID(1),
    PAID(2);

    private final int value;

    BillStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
