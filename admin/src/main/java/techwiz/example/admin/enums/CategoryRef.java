package techwiz.example.admin.enums;

public enum CategoryRef {
    ACCESSORIES("GlCztAR2nJKDw9ri3UtJ"),
    SEED("JofUMP9rvPiqV7Ghdmn2"),
    PLANT("MgFkDbhrE312rV9ASF8D");

    private final String value;

    CategoryRef(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
