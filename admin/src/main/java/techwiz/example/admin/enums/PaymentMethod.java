package techwiz.example.admin.enums;

public enum PaymentMethod {
    COD(false),
    PAYPAL(true);

    private final boolean value;

    PaymentMethod(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }
}
