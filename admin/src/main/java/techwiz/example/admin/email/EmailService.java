package techwiz.example.admin.email;

import jakarta.mail.internet.MimeMessage;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import techwiz.example.admin.models.Billing;
import techwiz.example.admin.models.OrderDetail;

@Service
public class EmailService {
    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;

    @Autowired
    public EmailService(JavaMailSender javaMailSender, TemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
    }
    
     public void sendConfirmationEmail(String recipientEmail, String userFullName, String orderID, Billing bill, List<OrderDetail> orderDetails){
         try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

            helper.setTo(recipientEmail);
            helper.setSubject("Confirmation of order code: " + orderID);

            Context context = new Context();
            context.setVariable("userFullName", userFullName);
            context.setVariable("orderID", orderID);
            context.setVariable("billing", bill); // Thêm payment vào context
            context.setVariable("orderDetails", orderDetails); // Thêm booking vào context
            
            String emailContent = templateEngine.process("admin/email/confirmation_email", context);
            //String emailContent = templateEngine.process("confirmation_email", context);

            helper.setText(emailContent, true);
            javaMailSender.send(mimeMessage);
        } catch (Exception e) {
            // Handle exceptions
        }
     }
}
